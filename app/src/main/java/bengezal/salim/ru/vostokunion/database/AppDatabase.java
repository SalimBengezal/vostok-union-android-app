package bengezal.salim.ru.vostokunion.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;

import bengezal.salim.ru.vostokunion.database.dao.ExtractionDAO;
import bengezal.salim.ru.vostokunion.database.dao.ReceiptDAO;
import bengezal.salim.ru.vostokunion.database.entities.Extraction;
import bengezal.salim.ru.vostokunion.database.entities.Receipt;

@Database(entities = {Receipt.class, Extraction.class}, version = 2, exportSchema = false)
@TypeConverters({DateConverter.class})
public abstract class AppDatabase extends RoomDatabase {

    public abstract ReceiptDAO receiptDAO();

    public abstract ExtractionDAO extractionDAO();

}
