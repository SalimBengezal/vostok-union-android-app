package bengezal.salim.ru.vostokunion.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;
import android.widget.TextView;

import java.util.List;

import bengezal.salim.ru.vostokunion.R;

public class DocumentsAdapter extends RecyclerView.Adapter<DocumentsAdapter.ViewHolder> {

    private final ItemClickListener listener;
    private final Context context;
    private final List<Document> documents;

    public DocumentsAdapter(List<Document> documents, ItemClickListener listener, Context context) {
        this.documents = documents;
        this.listener = listener;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_documents, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Document document = documents.get(position);
        holder.tvName.setText(document.getName());
        holder.tvDescription.setText(document.getDescription());
        holder.layout.setOnClickListener(e -> listener.onItemClicked(document.getId(), document.getType()));
        holder.btnMenu.setOnClickListener(e -> {
            PopupMenu popupMenu = new PopupMenu(context, holder.btnMenu);
            popupMenu.inflate(R.menu.item_documents);
            popupMenu.setOnMenuItemClickListener(menuItem -> {
                switch (menuItem.getItemId()) {
                    case R.id.menu_item_documents_delete:
                        listener.onItemDelete(document.getId(), document.getType());
                        popupMenu.dismiss();
                        break;
                }
                return false;
            });
            popupMenu.show();
        });
    }

    @Override
    public int getItemCount() {
        return documents.size();
    }

    public interface ItemClickListener {
        void onItemClicked(long id, Document.TYPE type);

        void onItemDelete(long id, Document.TYPE type);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView tvName;
        private final TextView tvDescription;
        private final ConstraintLayout layout;
        private final TextView btnMenu;

        ViewHolder(View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.item_document_name);
            tvDescription = itemView.findViewById(R.id.item_document_description);
            layout = itemView.findViewById(R.id.item_document);
            btnMenu = itemView.findViewById(R.id.item_document_menu);
        }
    }

    public static class Document {

        private final String name;
        private final String description;
        private final long id;
        private final TYPE type;

        public Document(String name, String description, long id, TYPE type) {
            this.name = name;
            this.description = description;
            this.id = id;
            this.type = type;
        }

        String getName() {
            return name;
        }

        String getDescription() {
            return description;
        }

        long getId() {
            return id;
        }

        TYPE getType() {
            return type;
        }

        public enum TYPE {
            RECEIPT, EXTRACTION
        }
    }
}
