package bengezal.salim.ru.vostokunion.fragments.documents;

import java.util.Date;

public interface DocumentsPresenter {

    void getMonthsInReceiptList();

    void downloadReceipt(Date date);

    void downloadExtraction(Date startPeriod, Date endPeriod);

    void loadCachedReceipts();

    void loadCachedExtractions();

    void openStoredReceipt(long id);

    void openStoredExtraction(long id);

    void onDestroy();

    void deleteReceipt(long id);

    void deleteExtraction(long id);
}
