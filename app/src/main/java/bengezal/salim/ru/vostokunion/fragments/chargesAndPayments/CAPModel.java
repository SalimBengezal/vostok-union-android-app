package bengezal.salim.ru.vostokunion.fragments.chargesAndPayments;


import java.util.Date;

import io.reactivex.Observable;
import io.reactivex.Single;
import ru.bengezal.salim.vostok_union_api.dto.ChargesAndPaymentsDTO;
import ru.bengezal.salim.vostok_union_api.dto.MonthDetailsDTO;

public interface CAPModel {

    Single<ChargesAndPaymentsDTO> getChargesAndPayments(Date from, Date to);

    Observable<MonthDetailsDTO> getMonthDetails(String monthId);

}
