package bengezal.salim.ru.vostokunion.activities.login;

import java.util.Map;

import io.reactivex.Observable;
import ru.bengezal.salim.vostok_union_api.dto.AccountStatus;

public interface LoginModel {

    Observable<Boolean> authenticate(String username, String password);

    Observable<Boolean> authenticate(Map<String, String> cookies);

    Observable<AccountStatus> checkLS(String accountNumber);

}
