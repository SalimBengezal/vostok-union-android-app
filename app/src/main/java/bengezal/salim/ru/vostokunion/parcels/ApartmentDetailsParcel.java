package bengezal.salim.ru.vostokunion.parcels;

import android.os.Parcel;
import android.os.Parcelable;

public class ApartmentDetailsParcel implements Parcelable {

    public static final Creator<ApartmentDetailsParcel> CREATOR = new Creator<ApartmentDetailsParcel>() {
        @Override
        public ApartmentDetailsParcel createFromParcel(Parcel in) {
            return new ApartmentDetailsParcel(in);
        }

        @Override
        public ApartmentDetailsParcel[] newArray(int size) {
            return new ApartmentDetailsParcel[size];
        }
    };

    private final String accountName;
    private final String accountNumber;
    private final String address;
    private final String phoneNumber;
    private final int registeredCount;
    private final double livingArea;
    private final double totalArea;
    private final String documentType;

    public ApartmentDetailsParcel(String accountName, String accountNumber, String address, String phoneNumber, int registeredCount, double livingArea, double totalArea, String documentType) {
        this.accountName = accountName;
        this.accountNumber = accountNumber;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.registeredCount = registeredCount;
        this.livingArea = livingArea;
        this.totalArea = totalArea;
        this.documentType = documentType;
    }

    private ApartmentDetailsParcel(Parcel parcel) {
        accountName = parcel.readString();
        accountNumber = parcel.readString();
        address = parcel.readString();
        phoneNumber = parcel.readString();
        registeredCount = parcel.readInt();
        livingArea = parcel.readDouble();
        totalArea = parcel.readDouble();
        documentType = parcel.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(accountName);
        parcel.writeString(accountNumber);
        parcel.writeString(address);
        parcel.writeString(phoneNumber);
        parcel.writeInt(registeredCount);
        parcel.writeDouble(livingArea);
        parcel.writeDouble(totalArea);
        parcel.writeString(documentType);
    }

    public String getAccountName() {
        return accountName;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public String getAddress() {
        return address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public int getRegisteredCount() {
        return registeredCount;
    }

    public double getLivingArea() {
        return livingArea;
    }

    public double getTotalArea() {
        return totalArea;
    }

    public String getDocumentType() {
        return documentType;
    }
}
