package bengezal.salim.ru.vostokunion.injection.modules;

import android.support.annotation.NonNull;

import javax.inject.Singleton;

import bengezal.salim.ru.vostokunion.managers.CookieManager;
import bengezal.salim.ru.vostokunion.managers.SharedPreferencesManager;
import dagger.Module;
import dagger.Provides;
import ru.bengezal.salim.vostok_union_api.VostokUnion;
import ru.bengezal.salim.vostok_union_api.VostokUnionAPI;

@Module
public class NetworkReceiverModule {

    @Provides
    @NonNull
    @Singleton
    VostokUnionAPI provideNetworkReceiverModule(CookieManager cookieManager, SharedPreferencesManager sp) {
        VostokUnionAPI vostokUnion = new VostokUnion();
        vostokUnion.setCookies(cookieManager.getCookie());
        vostokUnion.setAccountNumber(sp.getAccountNumber());
        vostokUnion.setPassword(sp.getPassword());
        return vostokUnion;
    }

}
