package bengezal.salim.ru.vostokunion.activities.login.impl;

import android.app.AlertDialog;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import bengezal.salim.ru.vostokunion.R;
import bengezal.salim.ru.vostokunion.activities.login.ErrorStatus;
import bengezal.salim.ru.vostokunion.activities.login.LoginPresenter;
import bengezal.salim.ru.vostokunion.activities.login.LoginView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;

public class LoginActivity extends AppCompatActivity implements LoginView {

    private static final String TAG = LoginActivity.class.getSimpleName();
    @BindView(R.id.login_etAccountNumber)
    EditText etAccountNumber;
    @BindView(R.id.login_etPassword)
    EditText etPassword;
    @BindView(R.id.login_tilAccountNumber)
    TextInputLayout tilAccountNumber;
    @BindView(R.id.login_tilPassword)
    TextInputLayout tilPassword;
    @BindView(R.id.login_progressBar)
    ProgressBar progressBar;
    @BindView(R.id.login_container)
    LinearLayout loginContainer;
    @BindView(R.id.login_retryBlock_tvInfo)
    TextView retryBlockInfo;
    @BindView(R.id.login_retryBlock_btnRetry)
    Button retryBlockBtnRetry;
    @BindView(R.id.login_retryBlock)
    RelativeLayout retryBlock;
    private AlertDialog ad;
    private LoginPresenter presenter;

    @OnTextChanged(value = {R.id.login_etPassword, R.id.login_etAccountNumber}, callback = OnTextChanged.Callback.TEXT_CHANGED)
    void onFieldsTextChanged() {
        if (!etPassword.getText().toString().isEmpty())
            tilPassword.setError(null);
        if (!etAccountNumber.getText().toString().isEmpty())
            tilAccountNumber.setError(null);
    }

    @OnClick(R.id.login_btnEnter)
    public void onViewClicked() {
        String accountNumber = etAccountNumber.getText().toString();
        String password = etPassword.getText().toString();
        if (!validateFields(accountNumber, password)) {
            Log.i(TAG, "One of the fields is empty");
        } else {
            presenter.authenticate(accountNumber, password);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        ad = new AlertDialog.Builder(this)
                .setIcon(R.drawable.ic_error_outline_black)
                .setTitle(getString(R.string.error))
                .setNegativeButton(getString(android.R.string.ok), (dialogInterface, i) -> dialogInterface.cancel())
                .create();

        presenter = new LoginPresenterImpl(this);
        presenter.authenticateFromPreferences();
        retryBlockBtnRetry.setOnClickListener(listener -> presenter.authenticateFromPreferences());
    }

    private boolean validateFields(String accountNumber, String password) {
        boolean valid = true;
        if (!accountNumber.isEmpty()) {
            tilAccountNumber.setError(null);
        } else {
            tilAccountNumber.setError(getString(R.string.error_account_number_empty_message));
            valid = false;
        }
        if (!password.isEmpty()) {
            tilPassword.setError(null);
        } else {
            tilPassword.setError(getString(R.string.error_password_empty_message));
            valid = false;
        }
        return valid;
    }

    @Override
    public void showError(ErrorStatus status) {
        switch (status) {
            case NOT_EXIST:
                loginContainer.setVisibility(ConstraintLayout.VISIBLE);
                ad.setMessage(getString(R.string.incorrect_account_number));
                ad.show();
                break;
            case INCORRECT_PASSWORD:
                loginContainer.setVisibility(ConstraintLayout.VISIBLE);
                ad.setMessage(getString(R.string.incorrect_password));
                ad.show();
                break;
            case SERVICE_UNAVAILABLE:
                retryBlockInfo.setText(getString(R.string.service_unavailable));
                retryBlock.setVisibility(View.VISIBLE);
                break;
            case TIMEOUT:
                retryBlockInfo.setText(getString(R.string.timeout_authentication));
                retryBlock.setVisibility(View.VISIBLE);
                break;
        }

    }

    @Override
    public void authorizationPassed() {
        ad.dismiss();
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void startLoading() {
        retryBlock.setVisibility(View.INVISIBLE);
        loginContainer.setVisibility(View.INVISIBLE);
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void stopLoading() {
        progressBar.setVisibility(View.INVISIBLE);
    }

    @Override
    protected void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }
}
