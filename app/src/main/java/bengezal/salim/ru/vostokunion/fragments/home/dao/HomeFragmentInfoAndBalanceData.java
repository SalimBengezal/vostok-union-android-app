package bengezal.salim.ru.vostokunion.fragments.home.dao;

import java.util.Date;

public class HomeFragmentInfoAndBalanceData {

    private final Date receiptMonth;
    private final Double receiptCurrentBalance;
    private final Double receiptDebt;
    private final Double receiptAccrued;
    private final Double receiptPaid;
    private final Double receiptPenalties;
    private final Double receiptTotalPayment;
    private final Double receiptTotalPaymentWithPenalties;
    private final Double receiptPaidCurrentMonth;
    private final String accountNumber;
    private final String accountName;
    private final String accountAddress;

    public HomeFragmentInfoAndBalanceData(String accountNumber, String accountName, String address, Date receiptMonth, Double receiptCurrentBalance, Double receiptDebt, Double receiptAccrued, Double receiptPaid, Double receiptPenalties, Double receiptTotalPayment, Double receiptTotalPaymentWithPenalties, Double receiptPaidCurrentMonth) {
        this.accountNumber = accountNumber;
        this.accountName = accountName;
        this.accountAddress = address;
        this.receiptMonth = receiptMonth;
        this.receiptCurrentBalance = receiptCurrentBalance;
        this.receiptDebt = receiptDebt;
        this.receiptAccrued = receiptAccrued;
        this.receiptPaid = receiptPaid;
        this.receiptPenalties = receiptPenalties;
        this.receiptTotalPayment = receiptTotalPayment;
        this.receiptTotalPaymentWithPenalties = receiptTotalPaymentWithPenalties;
        this.receiptPaidCurrentMonth = receiptPaidCurrentMonth;
    }

    public Date getReceiptMonth() {
        return receiptMonth;
    }

    public Double getReceiptCurrentBalance() {
        return receiptCurrentBalance;
    }

    public Double getReceiptDebt() {
        return receiptDebt;
    }

    public Double getReceiptAccrued() {
        return receiptAccrued;
    }

    public Double getReceiptPaid() {
        return receiptPaid;
    }

    public Double getReceiptPenalties() {
        return receiptPenalties;
    }

    public Double getReceiptTotalPayment() {
        return receiptTotalPayment;
    }

    public Double getReceiptTotalPaymentWithPenalties() {
        return receiptTotalPaymentWithPenalties;
    }

    public Double getReceiptPaidCurrentMonth() {
        return receiptPaidCurrentMonth;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public String getAccountName() {
        return accountName;
    }

    public String getAccountAddress() {
        return accountAddress;
    }
}
