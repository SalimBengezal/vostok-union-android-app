package bengezal.salim.ru.vostokunion.activities.login;

public interface LoginView {

    void showError(ErrorStatus status);

    void authorizationPassed();

    void startLoading();

    void stopLoading();

}
