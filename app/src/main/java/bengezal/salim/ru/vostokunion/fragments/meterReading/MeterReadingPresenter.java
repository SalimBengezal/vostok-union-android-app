package bengezal.salim.ru.vostokunion.fragments.meterReading;

import bengezal.salim.ru.vostokunion.fragments.meterReading.dao.DeleteMeterReading;
import bengezal.salim.ru.vostokunion.fragments.meterReading.dao.NewMeterReadings;

public interface MeterReadingPresenter {

    void showMeterReadings();

    void enterNewMeterReadings(NewMeterReadings newMeterReadings);

    void deleteMeterReadingsValue(DeleteMeterReading meterReadingsDeleteValue);

    void onDestroy();

}
