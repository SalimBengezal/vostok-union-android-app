package bengezal.salim.ru.vostokunion.fragments.home.impl;

import android.util.Log;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import bengezal.salim.ru.vostokunion.fragments.home.HomeFragmentModel;
import bengezal.salim.ru.vostokunion.fragments.home.HomeFragmentPresenter;
import bengezal.salim.ru.vostokunion.fragments.home.HomeFragmentView;
import bengezal.salim.ru.vostokunion.fragments.home.dao.ChargesData;
import bengezal.salim.ru.vostokunion.fragments.home.dao.CounterData;
import bengezal.salim.ru.vostokunion.fragments.home.dao.HomeFragmentInfoAndBalanceData;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import ru.bengezal.salim.vostok_union_api.dto.ChargesAndPaymentsDTO;
import ru.bengezal.salim.vostok_union_api.dto.MainInfoDTO;
import ru.bengezal.salim.vostok_union_api.dto.MeterReadingsDTO;

public class HomeFragmentPresenterImpl implements HomeFragmentPresenter {

    private static final String TAG = HomeFragmentPresenterImpl.class.getSimpleName();
    private final HomeFragmentModel model;
    private final HomeFragmentView view;

    private final CompositeDisposable disposables = new CompositeDisposable();

    HomeFragmentPresenterImpl(HomeFragmentView view) {
        this.view = view;
        model = new HomeFragmentModelImpl();
    }

    @Override
    public void loadInfoAndBalance() {
        view.startLoading();
        Disposable disposable = model.getMainInfo()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(this::convertInfoAndBalance)
                .subscribe(e -> {
                    view.showInfoAndBalanceData(e);
                    view.stopLoading();
                    Log.i(TAG, "data loaded");
                });
        disposables.add(disposable);
    }

    @Override
    public void loadCounters() {
        //TODO:visualize loading
        Disposable disposable = model.getCountersInfo()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(this::convertCountersInfo)
                .subscribe(e -> {
                    view.showCountersData(e);
                    Log.i(TAG, "Counters data loaded");
                });
        disposables.add(disposable);
    }

    @Override
    public void loadCharges(Date from, Date to) {
        Disposable disposable = model.getCharges(from, to)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(this::convertChargesInfo)
                .subscribe(e -> {
                    view.showChargesMonthly(e);
                    Log.i(TAG, "Charges data loaded");
                });
        disposables.add(disposable);
    }

    private List<ChargesData> convertChargesInfo(ChargesAndPaymentsDTO chargesAndPaymentsDTO) {
        List<ChargesData> list = new ArrayList<>();
        for (ChargesAndPaymentsDTO.Record rec : chargesAndPaymentsDTO.getRecords()) {
            if (rec.getCharged() != null) {
                String monthName = rec.getMonthName().replaceAll("[^\\D.]", "").trim();
                list.add(new ChargesData(monthName, rec.getCharged()));
            }
        }
        return list;
    }

    private List<CounterData> convertCountersInfo(MeterReadingsDTO meterReadingsDTO) {
        List<CounterData> counterDataList = new ArrayList<>();
        for (MeterReadingsDTO.Record record : meterReadingsDTO.getReadings()) {
            counterDataList.add(new CounterData(
                    record.getName(),
                    record.getTariff(),
                    record.getCounterNumber(),
                    record.getLastReadingDate(),
                    record.getLastReading()
            ));
        }
        return counterDataList;
    }

    private HomeFragmentInfoAndBalanceData convertInfoAndBalance(MainInfoDTO mainInfoDTO) {
        return new HomeFragmentInfoAndBalanceData(
                mainInfoDTO.accountNumber,
                mainInfoDTO.accountName,
                mainInfoDTO.address,
                mainInfoDTO.receiptMonth,
                mainInfoDTO.receiptCurrentBalance,
                mainInfoDTO.receiptDebt,
                mainInfoDTO.receiptAccrued,
                mainInfoDTO.receiptPaid,
                mainInfoDTO.receiptPenalties,
                mainInfoDTO.receiptTotalPayment,
                mainInfoDTO.receiptTotalPaymentWithPenalties,
                mainInfoDTO.receiptPaidCurrentMonth
        );
    }

    @Override
    public void onDestroy() {
        disposables.dispose();
    }
}
