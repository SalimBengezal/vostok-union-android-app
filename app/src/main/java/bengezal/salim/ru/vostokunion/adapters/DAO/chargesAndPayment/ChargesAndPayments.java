package bengezal.salim.ru.vostokunion.adapters.DAO.chargesAndPayment;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ChargesAndPayments {

    private final Date startDate;
    private final Date endDate;
    private final List<Record> records;

    public ChargesAndPayments(Date startDate, Date endDate) {
        this.startDate = startDate;
        this.endDate = endDate;
        records = new ArrayList<>();
    }

    public List<Record> getRecords() {
        return records;
    }

    public void add(String monthId, String monthName, Double incomingBalance, Double charged, Double outgoingBalance, List<Payment> payments) {
        records.add(new Record(monthId, monthName, incomingBalance, charged, outgoingBalance, payments));
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public class Record {
        private final String monthId;
        private final String monthName;
        private final Double incomingBalance;
        private final Double charged;
        private final Double outgoingBalance;
        private final List<Payment> payments;


        Record(String monthId, String monthName, Double incomingBalance, Double charged, Double outgoingBalance, List<Payment> payments) {
            this.monthId = monthId;
            this.monthName = monthName;
            this.incomingBalance = incomingBalance;
            this.charged = charged;
            this.outgoingBalance = outgoingBalance;
            this.payments = payments;

        }

        public String getMonthName() {
            return monthName;
        }

        public Double getIncomingBalance() {
            return incomingBalance;
        }

        public Double getCharged() {
            return charged;
        }

        public Double getOutgoingBalance() {
            return outgoingBalance;
        }

        public List<Payment> getPayments() {
            return payments;
        }

        public String getMonthId() {
            return monthId;
        }
    }

}
