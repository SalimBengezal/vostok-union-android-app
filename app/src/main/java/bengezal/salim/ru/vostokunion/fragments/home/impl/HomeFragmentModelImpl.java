package bengezal.salim.ru.vostokunion.fragments.home.impl;

import android.util.Log;

import java.util.Date;

import javax.inject.Inject;

import bengezal.salim.ru.vostokunion.injection.App;
import io.reactivex.Observable;
import ru.bengezal.salim.vostok_union_api.VostokUnionAPI;
import ru.bengezal.salim.vostok_union_api.dto.ChargesAndPaymentsDTO;
import ru.bengezal.salim.vostok_union_api.dto.MainInfoDTO;
import ru.bengezal.salim.vostok_union_api.dto.MeterReadingsDTO;

public class HomeFragmentModelImpl implements bengezal.salim.ru.vostokunion.fragments.home.HomeFragmentModel {

    private static final String TAG = HomeFragmentModelImpl.class.getName();

    @Inject
    VostokUnionAPI vostokUnion;

    HomeFragmentModelImpl() {
        App.getComponent().inject(this);
    }

    @Override
    public Observable<MainInfoDTO> getMainInfo() {
        return Observable.create(e -> {
            Log.i(TAG, "getting main info");
            e.onNext(vostokUnion.getMainInfo());
            e.onComplete();
        });
    }

    @Override
    public Observable<MeterReadingsDTO> getCountersInfo() {
        return Observable.create(e -> {
            Log.i(TAG, "getting counters info");
            e.onNext(vostokUnion.getMeterReadingsInfo());
            e.onComplete();
        });
    }

    @Override
    public Observable<ChargesAndPaymentsDTO> getCharges(Date from, Date to) {
        return Observable.create(e -> {
            Log.i(TAG, "getting charges info");
            e.onNext(vostokUnion.getChargesAndPaymentsInfo(from, to));
            e.onComplete();
        });
    }


}
