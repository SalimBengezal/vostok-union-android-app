package bengezal.salim.ru.vostokunion.fragments.meterReading.impl;

import android.util.Log;

import javax.inject.Inject;

import bengezal.salim.ru.vostokunion.fragments.meterReading.MeterReadingModel;
import bengezal.salim.ru.vostokunion.injection.App;
import io.reactivex.Observable;
import ru.bengezal.salim.vostok_union_api.VostokUnionAPI;
import ru.bengezal.salim.vostok_union_api.dto.DeleteMeterReadingDTO;
import ru.bengezal.salim.vostok_union_api.dto.MeterReadingsDTO;
import ru.bengezal.salim.vostok_union_api.dto.NewMeterReadingsDTO;

public class MeterReadingModelImpl implements MeterReadingModel {

    private static final String TAG = MeterReadingModelImpl.class.getName();

    @Inject
    VostokUnionAPI vostokUnion;

    MeterReadingModelImpl() {
        App.getComponent().inject(this);
    }

    @Override
    public Observable<MeterReadingsDTO> getMeterReadingsInfo() {
        return Observable.create(e -> {
            Log.i(TAG, "getting meter readings");
            e.onNext(vostokUnion.getMeterReadingsInfo());
            e.onComplete();
        });
    }

    @Override
    public Observable<String> deleteMeterReadingsValue(DeleteMeterReadingDTO meterReadingsDeleteValueDAO) {
        return Observable.create(e -> {
            Log.i(TAG, "Deleting meter readings: " + meterReadingsDeleteValueDAO.getRowIdSch());
            e.onNext(vostokUnion.deleteMeterReading(meterReadingsDeleteValueDAO));
            e.onComplete();
        });
    }

    public Observable<String> saveMeterReadings(NewMeterReadingsDTO meterReadings) {
        return Observable.create(e -> {
            Log.i(TAG, "Sending new meter readings");
            e.onNext(vostokUnion.saveNewReadings(meterReadings));
            e.onComplete();
        });
    }

}
