package bengezal.salim.ru.vostokunion.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import bengezal.salim.ru.vostokunion.R;
import bengezal.salim.ru.vostokunion.adapters.DAO.Counters.DateRecord;
import bengezal.salim.ru.vostokunion.components.ClickableRecyclerView;

public class CountersAdapter extends RecyclerView.Adapter<CountersAdapter.ViewHolder> {

    private final List<DateRecord> dateRecords;
    private final Context context;

    public CountersAdapter(List<DateRecord> records, Context context) {
        this.dateRecords = records;
        Collections.sort(dateRecords, DateRecord.DESCENDING_DATE);
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_counter_history, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        DateRecord dateRecord = dateRecords.get(position);
        String dateString = new SimpleDateFormat("dd MMMM yyyy", Locale.getDefault()).format(dateRecord.date);
        holder.tvDate.setText(dateString);

        holder.rvCounters.setOnClickListener(v -> {
            AlertDialog ad = new AlertDialog.Builder(context)
                    .setMessage(getDialogText(dateRecord.getCountersNumbers()))
                    .setTitle(R.string.counters)
                    .setNegativeButton(android.R.string.ok, (dialogInterface, i) -> dialogInterface.cancel())
                    .setIcon(R.drawable.ic_info)
                    .create();
            ad.show();
        });
        holder.rvCounters.setHasFixedSize(true);

        CountersRecordAdapter recordAdapter = new CountersRecordAdapter(dateRecord.records, context);
        holder.rvCounters.setAdapter(recordAdapter);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(context);
        holder.rvCounters.setLayoutManager(mLayoutManager);
        DividerItemDecorator dividerItemDecorator = new DividerItemDecorator(context.getResources().getDrawable(R.drawable.divider_vertical));
        holder.rvCounters.addItemDecoration(dividerItemDecorator);
    }

    private String getDialogText(Set<DateRecord.Counter> countersNumbers) {
        StringBuilder s = new StringBuilder();
        for (DateRecord.Counter counter : countersNumbers) {
            s.append(counter.name)
                    .append(": №").append(counter.number)
                    .append("\n");
        }
        return s.toString();
    }

    @Override
    public int getItemCount() {
        return dateRecords.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView tvDate;
        private final ClickableRecyclerView rvCounters;

        ViewHolder(View view) {
            super(view);
            tvDate = view.findViewById(R.id.item_counter_history_date);
            rvCounters = view.findViewById(R.id.item_counter_history_list);
        }

    }

    class DividerItemDecorator extends RecyclerView.ItemDecoration {
        private final Drawable mDivider;

        DividerItemDecorator(Drawable divider) {
            mDivider = divider;
        }

        @Override
        public void onDraw(@NonNull Canvas canvas, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
            int dividerLeft = parent.getPaddingLeft();
            int dividerRight = parent.getWidth() - parent.getPaddingRight();

            int childCount = parent.getChildCount();
            for (int i = 0; i <= childCount - 2; i++) {
                View child = parent.getChildAt(i);

                RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();

                int dividerTop = child.getBottom() + params.bottomMargin;
                int dividerBottom = dividerTop + mDivider.getIntrinsicHeight();

                mDivider.setBounds(dividerLeft, dividerTop, dividerRight, dividerBottom);
                mDivider.draw(canvas);
            }
        }
    }

}
