package bengezal.salim.ru.vostokunion.fragments.countersHistory;

import bengezal.salim.ru.vostokunion.adapters.DAO.Counters.Counters;

public interface CountersHistoryView {

    void startLoading();

    void stopLoading();

    void showCounters(Counters counters);

}
