package bengezal.salim.ru.vostokunion.activities.main.impl;

import android.util.Log;

import javax.inject.Inject;

import bengezal.salim.ru.vostokunion.activities.main.MainModel;
import bengezal.salim.ru.vostokunion.injection.App;
import io.reactivex.Observable;
import ru.bengezal.salim.vostok_union_api.VostokUnionAPI;
import ru.bengezal.salim.vostok_union_api.dto.MainInfoDTO;

public class MainModelImpl implements MainModel {

    private static final String TAG = MainModelImpl.class.getName();

    @Inject
    VostokUnionAPI vostokUnion;

    MainModelImpl() {
        App.getComponent().inject(this);
    }

    @Override
    public Observable<MainInfoDTO> getMainInfo() {
        return Observable.create(e -> {
            Log.i(TAG, "getting main info");
            e.onNext(vostokUnion.getMainInfo());
            e.onComplete();
        });
    }

    @Override
    public Observable<Void> logout() {
        return Observable.create(e -> {
            Log.i(TAG, "Logging out...");
            vostokUnion.disconnect();
            e.onComplete();
        });
    }

}
