package bengezal.salim.ru.vostokunion.parcels;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class MonthDetailsParcel implements Parcelable {

    public static final Creator<MonthDetailsParcel> CREATOR = new Creator<MonthDetailsParcel>() {
        @Override
        public MonthDetailsParcel createFromParcel(Parcel in) {
            return new MonthDetailsParcel(in);
        }

        @Override
        public MonthDetailsParcel[] newArray(int size) {
            return new MonthDetailsParcel[size];
        }
    };
    private final List<MonthDetailsRecordParcel> records;
    private String monthName;
    private Double incomingBalance;
    private Double charged;
    private Double recalculation;
    private Double takenForQuality;
    private Double payments;
    private Double outgoingBalance;

    private MonthDetailsParcel(Parcel parcel) {
        String monthName1 = parcel.readString();
        if (monthName1 != null) {
            monthName = monthName1;
        } else {
            monthName = null;
        }
        records = new ArrayList<>();
        parcel.readList(records, getClass().getClassLoader());
        Double incomingBalance1 = parcel.readDouble();
        if (incomingBalance1 == 0) {
            incomingBalance = null;
        } else {
            incomingBalance = incomingBalance1;
        }

        Double charged1 = parcel.readDouble();
        if (charged1 == 0) {
            charged = null;
        } else {
            charged = charged1;
        }

        Double recalculation1 = parcel.readDouble();
        if (recalculation1 == 0) {
            recalculation = null;
        } else {
            recalculation = recalculation1;
        }

        Double takenForQuality1 = parcel.readDouble();
        if (takenForQuality1 == 0) {
            takenForQuality = null;
        } else {
            takenForQuality = takenForQuality1;
        }

        Double payments1 = parcel.readDouble();
        if (payments1 == 0) {
            payments = null;
        } else {
            payments = payments1;
        }

        Double outgoingBalance1 = parcel.readDouble();
        if (outgoingBalance1 == 0) {
            outgoingBalance = null;
        } else {
            outgoingBalance = outgoingBalance1;
        }

    }

    public MonthDetailsParcel() {
        records = new ArrayList<>();
    }

    public void addRecord(String name, Double tariff, Double incomingBalance, Double charged, Double recalculation, Double takenForQuality, Double payments, Double outgoingBalance, Double volume) {
        records.add(new MonthDetailsRecordParcel(name, tariff, incomingBalance, charged, recalculation, takenForQuality, payments, outgoingBalance, volume));
    }

    public List<MonthDetailsRecordParcel> getRecords() {
        return records;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(monthName);
        parcel.writeList(records);
        if (incomingBalance == null) {
            parcel.writeDouble(0);
        } else {
            parcel.writeDouble(incomingBalance);
        }
        if (charged == null) {
            parcel.writeDouble(0);
        } else {
            parcel.writeDouble(charged);
        }
        if (recalculation == null) {
            parcel.writeDouble(0);
        } else {
            parcel.writeDouble(recalculation);
        }
        if (takenForQuality == null) {
            parcel.writeDouble(0);
        } else {
            parcel.writeDouble(takenForQuality);
        }
        if (payments == null) {
            parcel.writeDouble(0);
        } else {
            parcel.writeDouble(payments);
        }
        if (outgoingBalance == null) {
            parcel.writeDouble(0);
        } else {
            parcel.writeDouble(outgoingBalance);
        }
    }

    public Double getIncomingBalance() {
        return incomingBalance;
    }

    public void setIncomingBalance(Double incomingBalance) {
        this.incomingBalance = incomingBalance;
    }

    public Double getCharged() {
        return charged;
    }

    public void setCharged(Double charged) {
        this.charged = charged;
    }

    public Double getRecalculation() {
        return recalculation;
    }

    public void setRecalculation(Double recalculation) {
        this.recalculation = recalculation;
    }

    public Double getTakenForQuality() {
        return takenForQuality;
    }

    public void setTakenForQuality(Double takenForQuality) {
        this.takenForQuality = takenForQuality;
    }

    public Double getPayments() {
        return payments;
    }

    public void setPayments(Double payments) {
        this.payments = payments;
    }

    public Double getOutgoingBalance() {
        return outgoingBalance;
    }

    public void setOutgoingBalance(Double outgoingBalance) {
        this.outgoingBalance = outgoingBalance;
    }

    public String getMonthName() {
        return monthName;
    }

    public void setMonthName(String monthName) {
        this.monthName = monthName;
    }
}
