package bengezal.salim.ru.vostokunion.activities;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import java.util.ArrayList;

import bengezal.salim.ru.vostokunion.R;
import bengezal.salim.ru.vostokunion.adapters.DAO.DetailsContentValues;
import bengezal.salim.ru.vostokunion.adapters.DetailsAdapter;
import bengezal.salim.ru.vostokunion.parcels.ApartmentDetailsParcel;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ApartmentDetailsActivity extends AppCompatActivity {

    @BindView(R.id.details_rvInfo)
    RecyclerView detailsRecycleView;
    @BindView(R.id.details_toolbar)
    Toolbar detailsToolbar;

    private ArrayList<DetailsContentValues> contentValues;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apartment_details);
        ButterKnife.bind(this);
        ApartmentDetailsParcel detailsParcel = getIntent().getParcelableExtra(ApartmentDetailsParcel.class.getCanonicalName());
        contentValues = getData(detailsParcel);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        detailsRecycleView.setLayoutManager(mLayoutManager);
        RecyclerView.Adapter adapter = new DetailsAdapter(contentValues);
        detailsRecycleView.setAdapter(adapter);
        detailsToolbar.setTitle(getString(R.string.my_apartment));
        detailsToolbar.setNavigationIcon(ContextCompat.getDrawable(this, R.drawable.ic_arrow_back_white));
        detailsToolbar.setNavigationOnClickListener(view -> onBackPressed());
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    private ArrayList<DetailsContentValues> getData(ApartmentDetailsParcel parcel) {
        String accountName = parcel.getAccountName();
        String accountNumber = parcel.getAccountNumber();
        String address = parcel.getAddress();
        String phoneNumber = parcel.getPhoneNumber();
        int registeredCount = parcel.getRegisteredCount();
        double livingArea = parcel.getLivingArea();
        double totalArea = parcel.getTotalArea();
        String documentType = parcel.getDocumentType();

        contentValues = new ArrayList<>();
        contentValues.add(new DetailsContentValues(getString(R.string.subscriber), accountName, R.drawable.ic_account_person));
        contentValues.add(new DetailsContentValues(getString(R.string.account_number), accountNumber, R.drawable.ic_account_number));
        contentValues.add(new DetailsContentValues(getString(R.string.address), address, R.drawable.ic_location));
        contentValues.add(new DetailsContentValues(getString(R.string.phone_number), phoneNumber, R.drawable.ic_phone));
        contentValues.add(new DetailsContentValues(getString(R.string.registered_count), registeredCount + " " + getString(R.string.short_person), R.drawable.ic_people));
        contentValues.add(new DetailsContentValues(getString(R.string.living_area_with_measure), String.valueOf(livingArea), R.drawable.ic_living_area));
        contentValues.add(new DetailsContentValues(getString(R.string.total_area_with_measure), String.valueOf(totalArea), R.drawable.ic_total_area));
        contentValues.add(new DetailsContentValues(getString(R.string.document_type), documentType, R.drawable.ic_document));

        return contentValues;
    }
}
