package bengezal.salim.ru.vostokunion.adapters;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TextInputEditText;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.Switch;
import android.widget.TextView;

import com.mikepenz.materialize.color.Material;

import java.text.DateFormat;
import java.util.Collections;
import java.util.Objects;

import bengezal.salim.ru.vostokunion.R;
import bengezal.salim.ru.vostokunion.fragments.meterReading.dao.MeterReadings;
import bengezal.salim.ru.vostokunion.fragments.meterReading.impl.MeterReadingFragment;

public class MeterReadingsAdapter extends RecyclerView.Adapter<MeterReadingsAdapter.ViewHolder> {

    private static final String TAG = MeterReadingsAdapter.class.getName();
    private final MeterReadings meterReadings;
    private final Context context;
    private final MeterReadingFragment.DeleteValueListener listener;

    public MeterReadingsAdapter(MeterReadings readings, Context context, MeterReadingFragment.DeleteValueListener listener) {
        this.meterReadings = readings;
        Collections.sort(meterReadings.getReadings(), MeterReadings.DESCENDING_ACCEPTED_VALUE);
        this.context = context;
        this.listener = listener;
    }

    @NonNull
    @Override
    public MeterReadingsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_meter_readings, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MeterReadingsAdapter.ViewHolder holder, int position) {
        MeterReadings.Record record = meterReadings.getReadings().get(position);
        holder.tvName.setText(record.getName());
        if (record.getTariff() != null) {
            String tariff = context.getString(R.string.tariff) + ": " + record.getTariff();
            holder.tvTariff.setText(tariff);
        } else {
            holder.tvTariff.setVisibility(View.GONE);
        }
        String lastReading = context.getString(R.string.meter_reading) + ": " + record.getLastReading();
        holder.tvLastReading.setText(lastReading);
        DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT);
        String lastReadingDate = context.getString(R.string.meter_reading_date) + ": " + df.format(record.getLastReadingDate());
        holder.tvLastReadingDate.setText(lastReadingDate);
        String counterNumber = "№ " + record.getCounterNumber();
        holder.tvCounterNumber.setText(counterNumber);

        if (record.getEnabledEditState()) {
            if (record.getDeleteLink() == null) {
                holder.blockNewReading.setVisibility(View.VISIBLE);
                holder.tvLastReading.setVisibility(View.VISIBLE);
                holder.blockAcceptedValue.setVisibility(View.GONE);
            } else {
                holder.blockNewReading.setVisibility(View.GONE);
                holder.tvLastReading.setVisibility(View.GONE);
                holder.blockAcceptedValue.setVisibility(View.VISIBLE);
                String newReadingStr = context.getText(R.string.new_meter_reading) + ": " + record.getLastReading();
                holder.tvAcceptedValue.setText(newReadingStr);
                holder.btnDeleteAcceptedValue.setOnClickListener(e -> listener.onDeleteValue(record));
                holder.blockLayout.getBackground().setColorFilter(Material.LightGreen._50.getAsColor(), PorterDuff.Mode.SRC_OVER);
                Drawable checkDrawable = ContextCompat.getDrawable(context, R.drawable.ic_check);
                if (checkDrawable != null) {
                    checkDrawable.setColorFilter(new PorterDuffColorFilter(Material.LightGreen._300.getAsColor(), PorterDuff.Mode.SRC_IN));
                    holder.tvName.setCompoundDrawablesWithIntrinsicBounds(checkDrawable, null, null, null);
                }
            }
        } else {
            //TODO:check value after 25 day
            holder.blockNewReading.setVisibility(View.GONE);
            holder.blockAcceptedValue.setVisibility(View.GONE);
        }
        holder.etNewReading.setOnFocusChangeListener((view, hasFocus) -> {
            if (!hasFocus) {
                Double newValue = null;
                Double oldValue = record.getLastReading();
                try {
                    newValue = Double.parseDouble(Objects.requireNonNull(holder.etNewReading.getText()).toString().trim());
                } catch (NumberFormatException e) {
                    record.setNewReading(null);
                }
                if (newValue != null && oldValue != null) {
                    if (newValue < oldValue) {
                        holder.tvError.setText(R.string.value_must_be_not_less_than_latest);
                        holder.tvError.setVisibility(View.VISIBLE);
                        record.setNewReading(null);
                    } else {
                        holder.tvError.setVisibility(View.GONE);
                        record.setNewReading(newValue);
                    }
                }
                holder.etNewReading.clearFocus();
            }
        });

        holder.etNewReading.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String valStr = s.toString().trim();
                Double val = null;
                if (s.length() > 0) {
                    try {
                        val = Double.parseDouble(valStr);
                    } catch (NumberFormatException e) {
                        Log.i(TAG, "Can't parse: " + valStr);
                    } catch (NullPointerException e) {
                        Log.i(TAG, "nullPointerException: " + valStr);
                    }
                }
                if (val != null) {
                    record.setNewReading(val);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        if (record.getNewReading() != null) {
            holder.etNewReading.setText(String.format("%s", record.getNewReading().toString()));
        }
        holder.swNotChanged.setOnCheckedChangeListener((compoundButton, b) -> {
            if (compoundButton.isChecked()) {
                holder.etNewReading.setText(String.format("%s", record.getLastReading().toString()));
                holder.tvError.setVisibility(View.GONE);
                record.setNewReading(record.getLastReading());
                holder.etNewReading.clearFocus();
            }
            holder.etNewReading.setEnabled(!compoundButton.isChecked());
        });
    }

    @Override
    public int getItemCount() {
        return meterReadings.getReadings().size();
    }

    public MeterReadings getMeterReadings() {
        return meterReadings;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView tvTariff;
        private final TextView tvLastReading;
        private final TextView tvName;
        private final TextView tvLastReadingDate;
        private final TextView tvCounterNumber;
        private final ConstraintLayout blockNewReading;
        private final TextInputEditText etNewReading;
        private final Switch swNotChanged;
        private final TextView tvError;
        private final ConstraintLayout blockAcceptedValue;
        private final TextView tvAcceptedValue;
        private final ImageButton btnDeleteAcceptedValue;
        private final ConstraintLayout blockLayout;

        ViewHolder(View view) {
            super(view);
            tvName = view.findViewById(R.id.item_meter_readings_name);
            tvTariff = view.findViewById(R.id.item_meter_readings_tariff);
            tvLastReading = view.findViewById(R.id.item_meter_readings_lastReading);
            tvLastReadingDate = view.findViewById(R.id.item_meter_readings_lastReadingDate);
            tvCounterNumber = view.findViewById(R.id.item_meter_readings_counterNumber);
            etNewReading = view.findViewById(R.id.item_meter_readings_blockInputNewReading_newReading);
            swNotChanged = view.findViewById(R.id.item_meter_readings_blockInputNewReading_switcher);
            tvError = view.findViewById(R.id.item_meter_readings_blockInputNewReading_tvErrorMessage);
            blockAcceptedValue = view.findViewById(R.id.item_meter_readings_blockAcceptedValue);
            tvAcceptedValue = view.findViewById(R.id.item_meter_readings_blockAcceptedValue_newValue);
            btnDeleteAcceptedValue = view.findViewById(R.id.item_meter_readings_blockAcceptedValue_btnDelete);
            blockNewReading = view.findViewById(R.id.item_meter_readings_blockInputNewReading);
            blockLayout = view.findViewById(R.id.item_meter_readings_layout);
        }
    }
}
