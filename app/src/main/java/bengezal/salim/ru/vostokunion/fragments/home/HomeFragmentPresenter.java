package bengezal.salim.ru.vostokunion.fragments.home;

import java.util.Date;

public interface HomeFragmentPresenter {

    void loadInfoAndBalance();

    void loadCounters();

    void loadCharges(Date from, Date to);

    void onDestroy();

}
