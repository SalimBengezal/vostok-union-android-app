package bengezal.salim.ru.vostokunion.fragments.paymentHistory;

import java.util.Date;

public interface PaymentHistoryPresenter {

    void showPaymentHistory(Date from, Date to);

    void onDestroy();

}
