package bengezal.salim.ru.vostokunion.fragments.countersHistory.impl;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

import bengezal.salim.ru.vostokunion.R;
import bengezal.salim.ru.vostokunion.adapters.CountersAdapter;
import bengezal.salim.ru.vostokunion.adapters.DAO.Counters.Counters;
import bengezal.salim.ru.vostokunion.components.BlockInformationView;
import bengezal.salim.ru.vostokunion.dialogs.DateFilterDialogFragment;
import bengezal.salim.ru.vostokunion.fragments.countersHistory.CountersHistoryPresenter;
import bengezal.salim.ru.vostokunion.fragments.countersHistory.CountersHistoryView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class CountersHistoryFragment extends Fragment implements CountersHistoryView, DateFilterDialogFragment.DateFilterListener {

    private static final String FILTER_DIALOG_TAG = "FILTER_DIALOG_TAG";
    @BindView(R.id.counters_history_rvRecords)
    RecyclerView countersView;
    @BindView(R.id.counters_history_progressBar)
    ProgressBar progressBar;
    @BindView(R.id.counters_history_content)
    LinearLayout content;
    @BindView(R.id.counters_history_info_block)
    BlockInformationView countersHistoryBlockInformation;
    private CountersHistoryPresenter presenter;
    private Unbinder unbinder;
    private CountersAdapter adapter;
    private Date from;
    private Date to;

    public static CountersHistoryFragment getInstance() {
        return new CountersHistoryFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_counters_history, container, false);
        unbinder = ButterKnife.bind(this, view);
        presenter = new CountersHistoryFragmentPresenterImpl(this);

        Calendar calendar = Calendar.getInstance();
        to = calendar.getTime();
        calendar.add(Calendar.MONTH, -2);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        onDateChosen(from = calendar.getTime(), to);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        countersView.setLayoutManager(mLayoutManager);
        adapter = new CountersAdapter(new ArrayList<>(), getContext());
        countersView.setAdapter(adapter);
        countersView.setHasFixedSize(true);
        countersView.setNestedScrollingEnabled(false);

        return view;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void startLoading() {
        if (getActivity() != null) {
            progressBar.setVisibility(ProgressBar.VISIBLE);
            content.setVisibility(NestedScrollView.INVISIBLE);
        }
    }

    @Override
    public void stopLoading() {
        if (getActivity() != null) {
            progressBar.setVisibility(ProgressBar.INVISIBLE);
            content.setVisibility(NestedScrollView.VISIBLE);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_counters_history, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_filter:
                showDialog();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showCounters(Counters counters) {
        if (getActivity() != null) {
            adapter = new CountersAdapter(counters.getDates(), getContext());
            countersView.setAdapter(adapter);
            showInfoBlock(counters.getStartDate(), counters.getEndDate());
        }
    }

    private void showInfoBlock(Date startDate, Date endDate) {
        DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT);
        String dateString;
        if (startDate != null && endDate != null) {
            dateString = String.format("%s %s %s %s", getString(R.string.showing_results_from), df.format(startDate), getString(R.string.until), df.format(endDate));
        } else {
            dateString = getString(R.string.no_info_in_this_date_range);
        }
        countersHistoryBlockInformation.setMessage(dateString);
        countersHistoryBlockInformation.setIcon(ContextCompat.getDrawable(Objects.requireNonNull(getContext()), R.drawable.ic_info));
    }

    @SuppressLint("CommitTransaction")
    private void showDialog() {
        if (getActivity() != null) {
            FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
            Fragment prev = getActivity().getSupportFragmentManager().findFragmentByTag(FILTER_DIALOG_TAG);
            if (prev != null) {
                ft.remove(prev);
            }
            ft.addToBackStack(null);

            DateFilterDialogFragment dialogFragment = DateFilterDialogFragment.newInstance(from, to);
            dialogFragment.setTargetFragment(this, 0);
            assert getFragmentManager() != null;
            dialogFragment.show(getFragmentManager().beginTransaction(), FILTER_DIALOG_TAG);
        }
    }


    @Override
    public void onDateChosen(Date from, Date to) {
        if (getActivity() != null) {
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            if (fragmentManager.findFragmentById(R.id.counters_history_info_block) != null) {
                fragmentManager.beginTransaction()
                        .remove(Objects.requireNonNull(fragmentManager.findFragmentById(R.id.counters_history_info_block)))
                        .commit();
            }
        }
        this.from = from;
        this.to = to;
        presenter.showCountersHistory(from, to);
    }

    @Override
    public void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }
}
