package bengezal.salim.ru.vostokunion.adapters.DAO.Counters;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Counters {

    private final List<DateRecord> dates;
    private final Date startDate;
    private final Date endDate;

    public Counters(Date startDate, Date endDate) {
        this.startDate = startDate;
        this.endDate = endDate;
        dates = new ArrayList<>();
    }

    public List<DateRecord> getDates() {
        return dates;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public enum TYPE {ELECTRICITY_T1, ELECTRICITY_T2, ELECTRICITY_T3, HOT_WATER, COLD_WATER}

}
