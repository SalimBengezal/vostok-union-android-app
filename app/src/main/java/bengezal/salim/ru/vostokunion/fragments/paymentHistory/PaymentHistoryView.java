package bengezal.salim.ru.vostokunion.fragments.paymentHistory;

import bengezal.salim.ru.vostokunion.adapters.DAO.PaymentHistory.PaymentHistory;

public interface PaymentHistoryView {

    void startLoading();

    void stopLoading();

    void showPaymentHistory(PaymentHistory paymentHistory);

}
