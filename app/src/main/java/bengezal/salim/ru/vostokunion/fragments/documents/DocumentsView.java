package bengezal.salim.ru.vostokunion.fragments.documents;

import java.util.Date;
import java.util.List;

import bengezal.salim.ru.vostokunion.adapters.DocumentsAdapter;

public interface DocumentsView {

    void startLoading();

    void stopLoading();

    void showMonthsInReceiptList(List<Date> dateList);

    void showCachedDocumentsList(List<DocumentsAdapter.Document> documents, String header);

    void openReceiptFile(String path);

    void fileNotFound(String fileName);

}
