package bengezal.salim.ru.vostokunion.adapters.DAO.Counters;

import android.support.annotation.NonNull;

import java.util.Comparator;

import bengezal.salim.ru.vostokunion.R;

public class CounterRecord implements Comparable<CounterRecord> {

    public static final Comparator<? super CounterRecord> ALPHABETICAL_ORDER = (Comparator<CounterRecord>) (rec1, rec2) -> {
        String r1 = getCounterName(rec1.counterType);
        String r2 = getCounterName(rec2.counterType);
        if (r1 != null && r2 != null) {
            return r1.compareToIgnoreCase(r2);
        }
        return 0;
    };

    public final Counters.TYPE counterType;
    public final String counterNumber;
    public final Double meterReading;
    public final Double consumption;

    public CounterRecord(Counters.TYPE counterType, String counterNumber, Double meterReading, Double consumption) {
        this.counterType = counterType;
        this.counterNumber = counterNumber;
        this.meterReading = meterReading;
        this.consumption = consumption;
    }

    public static String getCounterName(Counters.TYPE counterType) {
        switch (counterType) {
            case HOT_WATER:
                return "Горячая вода";
            case COLD_WATER:
                return "Холодная вода";
            case ELECTRICITY_T2:
                return "Электричество (T2)";
            case ELECTRICITY_T3:
                return "Электричество (T3)";
            case ELECTRICITY_T1:
                return "Электричество (T1)";
        }
        return null;
    }

    public static int getImageDrawable(Counters.TYPE counterType) {
        switch (counterType) {
            case ELECTRICITY_T1:
                return R.drawable.ic_electricity;
            case ELECTRICITY_T2:
                return R.drawable.ic_electricity;
            case ELECTRICITY_T3:
                return R.drawable.ic_electricity;
            case COLD_WATER:
                return R.drawable.ic_cold_water;
            case HOT_WATER:
                return R.drawable.ic_hot_water;
        }
        return android.R.color.transparent;
    }

    @Override
    public int compareTo(@NonNull CounterRecord record) {
        String r1 = getCounterName(this.counterType);
        String r2 = getCounterName(record.counterType);
        if (r1 != null && r2 != null) {
            return r1.compareToIgnoreCase(r2);
        }
        return 0;
    }
}
