package bengezal.salim.ru.vostokunion.fragments.chargesAndPayments;

import java.util.Date;

public interface CAPPresenter {

    void showChargesAndPayments(Date from, Date to);

    void showMonthDetails(String monthId);

    void onDestroy();

}
