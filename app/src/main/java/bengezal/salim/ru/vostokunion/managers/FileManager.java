package bengezal.salim.ru.vostokunion.managers;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class FileManager {

    private static final String TAG = FileManager.class.getName();
    private static final String RECEIPTS_DIRECTORY = "receipts";
    private static final String EXTRACTIONS_DIRECTORY = "extractions";
    private static final String EXTERNAL_TEMPORARY_DIRECTORY = "vostokUnion";
    private static final SimpleDateFormat TIME_FORMAT = new SimpleDateFormat("HHmmssSSS", new Locale("ru"));
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyyMMdd", new Locale("ru"));

    private final Context context;

    public FileManager(Context context) {
        this.context = context;
    }

    private static boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state);
    }

    private static void copy(File src, File dst) throws IOException {
        try (InputStream in = new FileInputStream(src)) {
            try (OutputStream out = new FileOutputStream(dst)) {
                byte[] buf = new byte[1024];
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
            }
        }
    }

    private File getPathForNewFile(String fileName, String directory) throws Exception {
        File dir = new File(context.getFilesDir(), directory);
        if (!dir.exists() && !dir.mkdirs()) {
            Log.e(TAG, "Directory was not created:" + dir.getPath());
        }
        File f = new File(dir, fileName);
        if (!f.exists()) {
            try {
                if (f.createNewFile()) {
                    return f;
                } else {
                    Log.e(TAG, "File was not created: " + f.getPath());
                }
            } catch (IOException e) {
                Log.e(TAG, "IOException in creating of file: " + f.getPath());
            }
        }
        throw new Exception("This file exists:" + f.getPath());
    }

    private File getPathForNewReceiptFile(Date period) throws Exception {
        Date curDate = new Date();
        String fileName = DATE_FORMAT.format(period) + "_" + DATE_FORMAT.format(curDate) + TIME_FORMAT.format(curDate) + ".pdf";
        return getPathForNewFile(fileName, RECEIPTS_DIRECTORY);
    }

    private File getPathForNewExtractionFile(Date startPeriod, Date endPeriod) throws Exception {
        Date curDate = new Date();
        String fileName = DATE_FORMAT.format(startPeriod) + "-" + DATE_FORMAT.format(endPeriod) + "_" + DATE_FORMAT.format(curDate) + TIME_FORMAT.format(curDate) + ".pdf";
        return getPathForNewFile(fileName, EXTRACTIONS_DIRECTORY);
    }

    private void writeToFile(File f, byte[] data) throws IOException {
        FileOutputStream fos;
        fos = new FileOutputStream(f, false);
        fos.write(data);
        fos.close();
    }

    public String saveReceiptFile(Date period, byte[] data) {
        File receiptFile = null;
        try {
            receiptFile = getPathForNewReceiptFile(period);
            writeToFile(receiptFile, data);
        } catch (IOException e) {
            String message = receiptFile == null ? "IOException: receiptFile is null" : "Can't write data to file" + receiptFile.getPath();
            Log.e(TAG, message);
        } catch (Exception e) {
            String message = receiptFile == null ? "Exception: receiptFile is null" : "This file exists:" + receiptFile.getPath();
            Log.e(TAG, message);
        }
        if (receiptFile != null) {
            return receiptFile.getPath();
        } else {
            return null;
        }
    }

    public String saveExtractionFile(Date startPeriod, Date endPeriod, byte[] data) {
        File f = null;
        try {
            f = getPathForNewExtractionFile(startPeriod, endPeriod);
            writeToFile(f, data);
        } catch (IOException e) {
            String message = f == null ? "IOException: receiptFile is null" : "Can't write data to file" + f.getPath();
            Log.e(TAG, message);
        } catch (Exception e) {
            String message = f == null ? "Exception: receiptFile is null" : "This file exists:" + f.getPath();
            Log.e(TAG, message);
        }
        if (f != null) {
            return f.getPath();
        } else {
            return null;
        }
    }

    private String getFileInTempDir(String pathName, String temporaryName) throws IOException {
        File f = new File(pathName);
        if (!f.exists()) {
            return null;
        }
        String extension = getFileExtension(f.getName());
        if (extension == null) {
            return null;
        }
        File tempDir = new File(Environment.getExternalStorageDirectory(), EXTERNAL_TEMPORARY_DIRECTORY);
        if (!tempDir.exists() && !tempDir.mkdirs()) {
            Log.i(TAG, "Can't create temporary directory:" + tempDir.getPath());
        }
        File tempFile = new File(tempDir, temporaryName.concat(".").concat(extension));

        if (tempFile.exists() && !tempFile.delete()) {
            Log.e(TAG, "Can't delete existing file:" + tempFile.getPath());
        } else {
            if (!tempFile.createNewFile()) {
                Log.e(TAG, "Can't create temporary file: " + tempFile.getPath());
            } else {
                tempFile.deleteOnExit();
                copy(f, tempFile);
                return tempFile.getPath();
            }
        }
        return null;
    }

    private String getFileExtension(String fileName) {
        String[] strings = fileName.split("[.]");
        if (strings.length != 0) {
            return strings[strings.length - 1];
        }
        return null;
    }

    public String getFile(String pathName, String temporaryName) throws Exception {
        if (!isExternalStorageWritable()) {
            throw new Exception("External Storage is not Writable");
        } else {
            return getFileInTempDir(pathName, temporaryName);
        }
    }

    public boolean deleteFile(String path) {
        File f = new File(path);
        if (f.exists()) {
            return f.delete();
        }
        return false;
    }

    public void clearTemporaryDir() {
        File tempDir = new File(Environment.getExternalStorageDirectory(), EXTERNAL_TEMPORARY_DIRECTORY);
        if (tempDir.exists()) {
            for (File f : tempDir.listFiles()) {
                if (!f.delete()) {
                    Log.e(TAG, "Can't remove file:" + f.getPath());
                }
            }
        }
    }
}
