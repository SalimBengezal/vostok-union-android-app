package bengezal.salim.ru.vostokunion.adapters.DAO.PaymentHistory;

import android.support.annotation.NonNull;

import java.util.Comparator;
import java.util.Date;

public class PaymentHistoryRecord implements Comparable<PaymentHistoryRecord> {

    public static final Comparator<? super PaymentHistoryRecord> DATE_DESC = (Comparator<PaymentHistoryRecord>) (rec1, rec2) -> {
        Date r1 = rec1.date;
        Date r2 = rec2.date;
        if (r1 != null && r2 != null) {
            return r2.compareTo(r1);
        }
        return 0;
    };

    private final Date date;
    private final Double sum;
    private final String source;

    PaymentHistoryRecord(Date date, Double sum, String source) {
        this.date = date;
        this.sum = sum;
        this.source = source;
    }


    public Date getDate() {
        return date;
    }

    public Double getSum() {
        return sum;
    }

    public String getSource() {
        return source;
    }

    @Override
    public int compareTo(@NonNull PaymentHistoryRecord paymentHistoryRecord) {
        return paymentHistoryRecord.date.compareTo(this.date);
    }
}
