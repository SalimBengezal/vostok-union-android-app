package bengezal.salim.ru.vostokunion.fragments.meterReading.impl;

import java.util.Collections;

import bengezal.salim.ru.vostokunion.fragments.meterReading.MeterReadingModel;
import bengezal.salim.ru.vostokunion.fragments.meterReading.MeterReadingPresenter;
import bengezal.salim.ru.vostokunion.fragments.meterReading.MeterReadingView;
import bengezal.salim.ru.vostokunion.fragments.meterReading.dao.DeleteMeterReading;
import bengezal.salim.ru.vostokunion.fragments.meterReading.dao.MeterReadings;
import bengezal.salim.ru.vostokunion.fragments.meterReading.dao.NewMeterReadings;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import ru.bengezal.salim.vostok_union_api.dto.DeleteMeterReadingDTO;
import ru.bengezal.salim.vostok_union_api.dto.MeterReadingsDTO;
import ru.bengezal.salim.vostok_union_api.dto.NewMeterReadingsDTO;

public class MeterReadingPresenterImpl implements MeterReadingPresenter {

    private final MeterReadingView view;
    private final MeterReadingModel model;

    private final CompositeDisposable disposables = new CompositeDisposable();

    MeterReadingPresenterImpl(MeterReadingView view) {
        this.view = view;
        model = new MeterReadingModelImpl();
    }

    @Override
    public void showMeterReadings() {
        view.startLoading();
        Disposable disposable = model
                .getMeterReadingsInfo()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(this::convertMeterReadings)
                .subscribe(e -> {
                    view.showMeterReadings(e);
                    view.stopLoading();
                });
        disposables.add(disposable);
    }

    private MeterReadings convertMeterReadings(MeterReadingsDTO meterReadingsDTO) {
        MeterReadings meterReadings = new MeterReadings(
                meterReadingsDTO.getAction(),
                meterReadingsDTO.getLs(),
                meterReadingsDTO.getStartPossibleEnteringDate(),
                meterReadingsDTO.getEndPossibleEnteringDate()
        );
        for (MeterReadingsDTO.Record record : meterReadingsDTO.getReadings()) {
            MeterReadingsDTO.Record.DeleteLink deleteLink = record.getDeleteLink();
            if (deleteLink != null) {
                meterReadings.addReading(
                        record.getName(),
                        record.getTariff(),
                        record.getCounterNumber(),
                        record.getLastReadingDate(),
                        record.getLastReading(),
                        record.getNewReading(),
                        record.getNewReadingInputName(),
                        record.getNewReadingInputHiddenName(),
                        record.getNewReadingInputHiddenVal(),
                        record.getEditingStatus(),
                        deleteLink.getRowIdSch(),
                        deleteLink.getTariff(),
                        deleteLink.getDate(),
                        deleteLink.getPokazLs(),
                        deleteLink.getAction()
                );
            } else {
                meterReadings.addReading(
                        record.getName(),
                        record.getTariff(),
                        record.getCounterNumber(),
                        record.getLastReadingDate(),
                        record.getLastReading(),
                        record.getNewReading(),
                        record.getNewReadingInputName(),
                        record.getNewReadingInputHiddenName(),
                        record.getNewReadingInputHiddenVal(),
                        record.getEditingStatus()
                );
            }
        }
        Collections.sort(meterReadings.getReadings());

        return meterReadings;
    }

    @Override
    public void enterNewMeterReadings(NewMeterReadings newMeterReadings) {
        view.startLoading();
        NewMeterReadingsDTO newMeterReadingsDTO = new NewMeterReadingsDTO(newMeterReadings.getAction(), newMeterReadings.getPokazLS());
        for (NewMeterReadings.Record rec : newMeterReadings.getMeterReadings()) {
            newMeterReadingsDTO.addRecord(rec.getInputName(), rec.getNewValue(), rec.getHiddenName(), rec.getHiddenVal());
        }
        Disposable disposable = model.saveMeterReadings(newMeterReadingsDTO)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .doOnComplete(this::showMeterReadings)
                .subscribe(e -> {
                    view.resultOfEnteringReadings(e);
                    view.stopLoading();
                });
        disposables.add(disposable);
    }

    @Override
    public void deleteMeterReadingsValue(DeleteMeterReading deleteMeterReading) {
        view.startLoading();
        DeleteMeterReadingDTO deleteMeterReadingDTO = new DeleteMeterReadingDTO(deleteMeterReading.getRowIdSch(), deleteMeterReading.getTariff(), deleteMeterReading.getDate(), deleteMeterReading.getPokazLS(), deleteMeterReading.getAction());
        Disposable disposable = model.deleteMeterReadingsValue(deleteMeterReadingDTO)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .doOnComplete(this::showMeterReadings)
                .subscribe(e -> {
                    view.resultOfEnteringReadings(e);
                    view.stopLoading();
                });
        disposables.add(disposable);
    }

    @Override
    public void onDestroy() {
        disposables.dispose();
    }
}
