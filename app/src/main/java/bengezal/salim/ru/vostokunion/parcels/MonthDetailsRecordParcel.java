package bengezal.salim.ru.vostokunion.parcels;

import android.os.Parcel;
import android.os.Parcelable;

public class MonthDetailsRecordParcel implements Parcelable {
    public static final Creator<MonthDetailsRecordParcel> CREATOR = new Creator<MonthDetailsRecordParcel>() {
        @Override
        public MonthDetailsRecordParcel createFromParcel(Parcel in) {
            return new MonthDetailsRecordParcel(in);
        }

        @Override
        public MonthDetailsRecordParcel[] newArray(int size) {
            return new MonthDetailsRecordParcel[size];
        }
    };
    private final String name;
    private final Double tariff;
    private final Double incomingBalance;
    private final Double charged;
    private final Double recalculation;
    private final Double takenForQuality;
    private final Double payments;
    private final Double outgoingBalance;
    private final Double volume;

    MonthDetailsRecordParcel(String name, Double tariff, Double incomingBalance, Double charged, Double recalculation, Double takenForQuality, Double payments, Double outgoingBalance, Double volume) {
        this.name = name;
        this.tariff = tariff;
        this.incomingBalance = incomingBalance;
        this.charged = charged;
        this.recalculation = recalculation;
        this.takenForQuality = takenForQuality;
        this.payments = payments;
        this.outgoingBalance = outgoingBalance;
        this.volume = volume;
    }

    private MonthDetailsRecordParcel(Parcel parcel) {
        name = parcel.readString();

        Double tariff1 = parcel.readDouble();
        if (tariff1 == 0) {
            tariff = null;
        } else {
            tariff = tariff1;
        }

        Double incomingBalance1 = parcel.readDouble();
        if (incomingBalance1 == 0) {
            incomingBalance = null;
        } else {
            incomingBalance = incomingBalance1;
        }

        Double charged1 = parcel.readDouble();
        if (charged1 == 0) {
            charged = null;
        } else {
            charged = charged1;
        }

        Double recalculation1 = parcel.readDouble();
        if (recalculation1 == 0) {
            recalculation = null;
        } else {
            recalculation = recalculation1;
        }

        Double takenForQuality1 = parcel.readDouble();
        if (takenForQuality1 == 0) {
            takenForQuality = null;
        } else {
            takenForQuality = takenForQuality1;
        }

        Double payments1 = parcel.readDouble();
        if (payments1 == 0) {
            payments = null;
        } else {
            payments = payments1;
        }

        Double outgoingBalance1 = parcel.readDouble();
        if (outgoingBalance1 == 0) {
            outgoingBalance = null;
        } else {
            outgoingBalance = outgoingBalance1;
        }

        Double volume1 = parcel.readDouble();
        if (volume1 == 0) {
            volume = null;
        } else {
            volume = volume1;
        }

    }

    public String getName() {
        return name;
    }

    public Double getTariff() {
        return tariff;
    }

    public Double getIncomingBalance() {
        return incomingBalance;
    }

    public Double getCharged() {
        return charged;
    }

    public Double getRecalculation() {
        return recalculation;
    }

    public Double getTakenForQuality() {
        return takenForQuality;
    }

    public Double getPayments() {
        return payments;
    }

    public Double getOutgoingBalance() {
        return outgoingBalance;
    }

    public Double getVolume() {
        return volume;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        if (name != null) {
            parcel.writeString(name);
        } else {
            parcel.writeString(null);
        }
        if (tariff == null) {
            parcel.writeDouble(0);
        } else {
            parcel.writeDouble(tariff);
        }
        if (incomingBalance == null) {
            parcel.writeDouble(0);
        } else {
            parcel.writeDouble(incomingBalance);
        }
        if (charged == null) {
            parcel.writeDouble(0);
        } else {
            parcel.writeDouble(charged);
        }
        if (recalculation == null) {
            parcel.writeDouble(0);
        } else {
            parcel.writeDouble(recalculation);
        }
        if (takenForQuality == null) {
            parcel.writeDouble(0);
        } else {
            parcel.writeDouble(takenForQuality);
        }
        if (payments == null) {
            parcel.writeDouble(0);
        } else {
            parcel.writeDouble(payments);
        }
        if (outgoingBalance == null) {
            parcel.writeDouble(0);
        } else {
            parcel.writeDouble(outgoingBalance);
        }
        if (volume == null) {
            parcel.writeDouble(0);
        } else {
            parcel.writeDouble(volume);
        }
    }
}
