package bengezal.salim.ru.vostokunion.fragments.chargesAndPayments.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import bengezal.salim.ru.vostokunion.adapters.DAO.chargesAndPayment.ChargesAndPayments;
import bengezal.salim.ru.vostokunion.adapters.DAO.chargesAndPayment.Payment;
import bengezal.salim.ru.vostokunion.fragments.chargesAndPayments.CAPModel;
import bengezal.salim.ru.vostokunion.fragments.chargesAndPayments.CAPPresenter;
import bengezal.salim.ru.vostokunion.fragments.chargesAndPayments.CAPView;
import bengezal.salim.ru.vostokunion.parcels.MonthDetailsParcel;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import ru.bengezal.salim.vostok_union_api.dto.ChargesAndPaymentsDTO;
import ru.bengezal.salim.vostok_union_api.dto.MonthDetailsDTO;

public class CAPPresenterImpl implements CAPPresenter {

    private final CAPModel model;
    private final CAPView view;
    private final CompositeDisposable disposables = new CompositeDisposable();

    CAPPresenterImpl(CAPView view) {
        this.model = new CAPModelImpl();
        this.view = view;

    }

    @Override
    public void showChargesAndPayments(Date from, Date to) {
        view.startLoading();
        Disposable disposable = model.getChargesAndPayments(from, to)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(this::convertChargesAndPayments)
                .subscribe(list -> {
                    view.showChargesAndPayments(list);
                    view.stopLoading();
                });
        disposables.add(disposable);
    }

    @Override
    public void showMonthDetails(String monthId) {
        view.startLoading();
        Disposable disposable = model.getMonthDetails(monthId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(this::convertMonthDetails)
                .subscribe(monthDetails -> {
                    if (!monthDetails.getRecords().isEmpty()) {
                        DateFormat df = new SimpleDateFormat("MM-yyyy", Locale.getDefault());
                        Date monthDate = df.parse(monthId);
                        String monthName = new SimpleDateFormat("LLLL yyyy", Locale.getDefault()).format(monthDate);
                        monthDetails.setMonthName(monthName);
                        view.showMonthDetails(monthDetails);
                    } else {
                        view.notifyNoDataNow();
                    }
                    view.stopLoading();
                });
        disposables.add(disposable);
    }

    private MonthDetailsParcel convertMonthDetails(MonthDetailsDTO monthDetailsDTO) {
        MonthDetailsParcel monthDetails = new MonthDetailsParcel();
        MonthDetailsDTO.Record s = monthDetailsDTO.getSummary();
        if (s != null && monthDetailsDTO.getRecords() != null) {
            monthDetails.setCharged(s.getCharged());
            monthDetails.setIncomingBalance(s.getIncomingBalance());
            monthDetails.setOutgoingBalance(s.getOutgoingBalance());
            monthDetails.setPayments(s.getPayments());
            monthDetails.setRecalculation(s.getRecalculation());
            monthDetails.setTakenForQuality(s.getTakenForQuality());

            for (MonthDetailsDTO.Record rec : monthDetailsDTO.getRecords()) {
                monthDetails.addRecord(
                        rec.getName(),
                        rec.getTariff(),
                        rec.getIncomingBalance(),
                        rec.getCharged(),
                        rec.getRecalculation(),
                        rec.getTakenForQuality(),
                        rec.getPayments(),
                        rec.getOutgoingBalance(),
                        rec.getVolume()
                );
            }
        }
        return monthDetails;
    }

    private ChargesAndPayments convertChargesAndPayments(ChargesAndPaymentsDTO chargesAndPaymentsDTO) {

        ChargesAndPayments chargesAndPayments = new ChargesAndPayments(chargesAndPaymentsDTO.getFrom(), chargesAndPaymentsDTO.getTo());

        for (ChargesAndPaymentsDTO.Record record : chargesAndPaymentsDTO.getRecords()) {

            String monthName = null;
            Double incomingBalance = null;
            Double charged = null;
            Double outgoingBalance = null;
            Date paymentDate = null;
            String paymentSource = null;
            Double paymentAmount = null;
            String monthId = null;

            if (record.getMonthName() != null) {
                monthName = record.getMonthName();
            }
            if (record.getIncomingBalance() != null) {
                incomingBalance = record.getIncomingBalance();
            }
            if (record.getCharged() != null) {
                charged = record.getCharged();
            }
            if (record.getOutgoingBalance() != null) {
                outgoingBalance = record.getOutgoingBalance();
            }
            if (record.getPaymentDate() != null) {
                paymentDate = record.getPaymentDate();
            }
            if (record.getPaymentSource() != null) {
                paymentSource = record.getPaymentSource();
            }
            if (record.getPaymentAmount() != null) {
                paymentAmount = record.getPaymentAmount();
            }
            if (record.getRecordId() != null) {
                monthId = record.getRecordId();
            }

            if (monthName == null && incomingBalance == null && charged == null) {
                List<Payment> payments = chargesAndPayments.getRecords().get(chargesAndPayments.getRecords().size() - 1).getPayments();
                payments.add(new Payment(paymentDate, paymentAmount, paymentSource));
            } else {
                List<Payment> payments = new ArrayList<>();
                if (paymentDate != null && paymentAmount != null && paymentSource != null) {
                    payments.add(new Payment(paymentDate, paymentAmount, paymentSource));
                }
                chargesAndPayments.add(monthId, monthName, incomingBalance, charged, outgoingBalance, payments);
            }
        }

        Collections.reverse(chargesAndPayments.getRecords());

        return chargesAndPayments;
    }

    public void onDestroy() {
        disposables.dispose();
    }

}
