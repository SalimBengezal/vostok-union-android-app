package bengezal.salim.ru.vostokunion.fragments.countersHistory;

import java.util.Date;

import io.reactivex.Observable;
import ru.bengezal.salim.vostok_union_api.dto.CountersHistoryDTO;

public interface CountersHistoryModel {

    Observable<CountersHistoryDTO> getHistoryCounters(Date from, Date to);

}
