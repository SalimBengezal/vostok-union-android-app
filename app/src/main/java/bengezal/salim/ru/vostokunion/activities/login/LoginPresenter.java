package bengezal.salim.ru.vostokunion.activities.login;

public interface LoginPresenter {

    void authenticate(String username, String password);

    void authenticateFromPreferences();

    void onDestroy();

}
