package bengezal.salim.ru.vostokunion.fragments.countersHistory.impl;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import bengezal.salim.ru.vostokunion.adapters.DAO.Counters.CounterRecord;
import bengezal.salim.ru.vostokunion.adapters.DAO.Counters.Counters;
import bengezal.salim.ru.vostokunion.adapters.DAO.Counters.DateRecord;
import bengezal.salim.ru.vostokunion.fragments.countersHistory.CountersHistoryModel;
import bengezal.salim.ru.vostokunion.fragments.countersHistory.CountersHistoryPresenter;
import bengezal.salim.ru.vostokunion.fragments.countersHistory.CountersHistoryView;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import ru.bengezal.salim.vostok_union_api.dto.CountersHistoryDTO;

public class CountersHistoryFragmentPresenterImpl implements CountersHistoryPresenter {

    private final CountersHistoryView view;
    private final CountersHistoryModel model;

    private final CompositeDisposable disposables = new CompositeDisposable();

    CountersHistoryFragmentPresenterImpl(CountersHistoryView view) {
        this.view = view;
        model = new CountersHistoryModelImpl();
    }

    @Override
    public void showCountersHistory(Date from, Date to) {
        view.startLoading();
        Disposable disposable = model.getHistoryCounters(from, to)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .map(this::convert)
                .subscribe(e -> {
                    view.showCounters(e);
                    view.stopLoading();
                });
        disposables.add(disposable);
    }

    private Counters convert(CountersHistoryDTO historyCounterInfo) {
        Counters counters = new Counters(historyCounterInfo.getFromDate(), historyCounterInfo.getToDate());
        Set<Date> dateSet = new HashSet<>();
        for (CountersHistoryDTO.Record record : historyCounterInfo.getHistoryList()) {
            dateSet.add(record.getDate());
        }
        for (Date date : dateSet) {
            DateRecord dateRecord = new DateRecord();
            dateRecord.date = date;
            for (CountersHistoryDTO.Record record : historyCounterInfo.getHistoryList()) {
                if (record.getDate().equals(date)) {
                    dateRecord.records.add(
                            new CounterRecord(
                                    Counters.TYPE.valueOf(record.getCounterType().toString()),
                                    record.getCounterNumber(),
                                    record.getMeterReading(),
                                    record.getConsumption()
                            )
                    );
                }

            }
            counters.getDates().add(dateRecord);
        }
        leaveOnlyNewData(counters);
        return counters;
    }

    private void leaveOnlyNewData(Counters counters) {
        for (DateRecord d : counters.getDates()) {
            Set<CounterRecord> removeList = new HashSet<>();
            for (CounterRecord cr : d.records) {
                for (CounterRecord counterRecord : d.records) {
                    if (cr != counterRecord
                            && cr.counterType.equals(counterRecord.counterType)
                            && cr.counterNumber.equals(counterRecord.counterNumber)
                            && cr.meterReading > counterRecord.meterReading) {
                        removeList.add(counterRecord);
                    }
                }
            }
            d.records.removeAll(removeList);
        }
    }

    @Override
    public void onDestroy() {
        disposables.dispose();
    }
}
