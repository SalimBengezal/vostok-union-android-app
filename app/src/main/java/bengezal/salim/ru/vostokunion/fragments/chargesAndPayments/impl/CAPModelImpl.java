package bengezal.salim.ru.vostokunion.fragments.chargesAndPayments.impl;

import android.util.Log;

import java.util.Date;

import javax.inject.Inject;

import bengezal.salim.ru.vostokunion.fragments.chargesAndPayments.CAPModel;
import bengezal.salim.ru.vostokunion.injection.App;
import io.reactivex.Observable;
import io.reactivex.Single;
import ru.bengezal.salim.vostok_union_api.VostokUnionAPI;
import ru.bengezal.salim.vostok_union_api.dto.ChargesAndPaymentsDTO;
import ru.bengezal.salim.vostok_union_api.dto.MonthDetailsDTO;

public class CAPModelImpl implements CAPModel {

    private static final String TAG = CAPModelImpl.class.getName();

    @Inject
    VostokUnionAPI vostokUnion;

    CAPModelImpl() {
        App.getComponent().inject(this);
    }

    @Override
    public Single<ChargesAndPaymentsDTO> getChargesAndPayments(Date from, Date to) {
        return Single.fromCallable(() -> {
            Log.i(TAG, "getting charges and payments");
            return vostokUnion.getChargesAndPaymentsInfo(from, to);
        });
    }

    @Override
    public Observable<MonthDetailsDTO> getMonthDetails(String monthId) {
        return Observable.create(e -> {
            Log.i(TAG, "getting month details: " + monthId);
            e.onNext(vostokUnion.getMonthDetailsInfo(monthId));
            e.onComplete();
        });
    }
}
