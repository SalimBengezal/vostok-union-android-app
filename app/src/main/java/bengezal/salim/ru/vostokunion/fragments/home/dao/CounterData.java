package bengezal.salim.ru.vostokunion.fragments.home.dao;

import android.support.annotation.NonNull;

import java.util.Date;

public class CounterData implements Comparable<CounterData> {

    private final String counterNumber;
    private final Date lastReadingDate;
    private final Double lastReading;
    private TYPE type;
    private TARIFF tariff;
    public CounterData(String name, String tariff, String counterNumber, Date lastReadingDate, Double lastReading) {
        String _name = name.toLowerCase();
        if (_name.contains("электро")) {
            String _tariff = tariff.toLowerCase();
            this.type = TYPE.ELECTRICITY;
            if (_tariff.contains("т1")) {
                this.tariff = TARIFF.T1;
            } else if (_tariff.contains("т2")) {
                this.tariff = TARIFF.T2;
            } else if (_tariff.contains("т3")) {
                this.tariff = TARIFF.T3;
            }
        } else if (_name.contains("холодное")) {
            this.type = TYPE.COLD_WATER;
            this.tariff = null;
        } else if (_name.contains("горячее")) {
            this.type = TYPE.HOT_WATER;
            this.tariff = null;
        } else {
            this.type = null;
            this.tariff = null;
        }

        this.counterNumber = counterNumber;
        this.lastReadingDate = lastReadingDate;
        this.lastReading = lastReading;
    }

    private static String getCounterName(TYPE counterType, TARIFF tariff) {
        switch (counterType) {
            case HOT_WATER:
                return "Горячая вода";
            case COLD_WATER:
                return "Холодная вода";
            case ELECTRICITY:
                switch (tariff) {
                    case T1:
                        return "Электричество (T1)";
                    case T2:
                        return "Электричество (T2)";
                    case T3:
                        return "Электричество (T3)";
                }
        }
        return null;
    }

    @Override
    public int compareTo(@NonNull CounterData record) {
        String r1 = getCounterName(this.type, this.tariff);
        String r2 = getCounterName(record.type, record.tariff);
        if (r1 != null && r2 != null) {
            return r1.compareToIgnoreCase(r2);
        }
        return 0;
    }

    public TYPE getType() {
        return type;
    }

    public TARIFF getTariff() {
        return tariff;
    }

    public String getCounterNumber() {
        return counterNumber;
    }

    public Date getLastReadingDate() {
        return lastReadingDate;
    }

    public Double getLastReading() {
        return lastReading;
    }

    public enum TYPE {ELECTRICITY, COLD_WATER, HOT_WATER}

    public enum TARIFF {T1, T2, T3}
}
