package bengezal.salim.ru.vostokunion.fragments.paymentHistory.impl;

import android.annotation.SuppressLint;
import android.graphics.drawable.TransitionDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSmoothScroller;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.mikepenz.materialize.color.Material;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import bengezal.salim.ru.vostokunion.R;
import bengezal.salim.ru.vostokunion.adapters.DAO.PaymentHistory.PaymentHistory;
import bengezal.salim.ru.vostokunion.adapters.DAO.PaymentHistory.PaymentHistoryRecord;
import bengezal.salim.ru.vostokunion.adapters.PaymentHistoryAdapter;
import bengezal.salim.ru.vostokunion.components.BlockInformationView;
import bengezal.salim.ru.vostokunion.components.SmoothScrollableRecyclerView;
import bengezal.salim.ru.vostokunion.dialogs.DateFilterDialogFragment;
import bengezal.salim.ru.vostokunion.fragments.paymentHistory.PaymentHistoryPresenter;
import bengezal.salim.ru.vostokunion.fragments.paymentHistory.PaymentHistoryView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class PaymentHistoryFragment extends Fragment implements PaymentHistoryView, DateFilterDialogFragment.DateFilterListener {

    private static final String FILTER_DIALOG_TAG = "dialog";
    @BindView(R.id.payment_history_progressBar)
    ProgressBar progressBar;
    @BindView(R.id.payment_history_rvRecords)
    SmoothScrollableRecyclerView paymentsView;
    @BindView(R.id.payment_history_content)
    RelativeLayout content;
    @BindView(R.id.payment_history_chart)
    BarChart chart;
    @BindView(R.id.payment_history_info_block)
    BlockInformationView paymentHistoryBlockInformation;
    private int highlightPosition = -1;
    private PaymentHistoryAdapter adapter;
    private Unbinder unbinder;
    private PaymentHistoryPresenter presenter;
    private Date to;
    private Date from;
    private LinearSmoothScroller mSmoothScroller;
    private LinearLayoutManager mLayoutManager;
    private HighlighterListener highlighterListener;

    public static PaymentHistoryFragment getInstance() {
        return new PaymentHistoryFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_payment_history, container, false);
        unbinder = ButterKnife.bind(this, view);
        presenter = new PaymentHistoryFragmentPresenterImpl(this);

        Calendar calendar = Calendar.getInstance();
        to = calendar.getTime();
        calendar.add(Calendar.YEAR, -1);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        onDateChosen(from = calendar.getTime(), to);

        mLayoutManager = new LinearLayoutManager(getContext());
        mSmoothScroller = new LinearSmoothScroller(Objects.requireNonNull(getContext())) {
            @Override
            protected int getVerticalSnapPreference() {
                return LinearSmoothScroller.SNAP_TO_START;
            }

            @Override
            protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                return 300f / displayMetrics.densityDpi;
            }

            @Override
            protected void onStop() {
                super.onStop();
                if (highlightPosition != -1) {
                    highlighterListener.highlight(highlightPosition);
                    highlightPosition = -1;
                }
            }

        };
        paymentsView.setLayoutManager(mLayoutManager);
        paymentsView.setClickable(false);
        paymentsView.setOnTouchListener((v, event) -> {
            chart.highlightValue(null);
            return false;
        });
        paymentsView.setNestedScrollingEnabled(false);
        adapter = new PaymentHistoryAdapter(new ArrayList<>(), getContext());
        paymentsView.setAdapter(adapter);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(paymentsView.getContext(), mLayoutManager.getOrientation());
        dividerItemDecoration.setDrawable(getResources().getDrawable(R.drawable.divider_vertical_transparent_extended));
        paymentsView.addItemDecoration(dividerItemDecoration);

        highlighterListener = position -> {
            LinearLayoutManager linManager = (LinearLayoutManager) paymentsView.getLayoutManager();
            assert linManager != null;
            View v = linManager.findViewByPosition(position);
            if (v != null) {
                TransitionDrawable transition = (TransitionDrawable) v.getBackground();
                transition.startTransition(3000);
                transition.reverseTransition(3000);

            }
        };

        return view;
    }

    @Override
    public void onDateChosen(Date from, Date to) {
        if (getActivity() != null) {
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            if (fragmentManager.findFragmentById(R.id.payment_history_info_block) != null) {
                fragmentManager.beginTransaction()
                        .remove(Objects.requireNonNull(fragmentManager.findFragmentById(R.id.payment_history_info_block)))
                        .commit();
            }
        }
        this.from = from;
        this.to = to;
        presenter.showPaymentHistory(from, to);
    }

    @Override
    public void startLoading() {
        if (getActivity() != null) {
            progressBar.setVisibility(ProgressBar.VISIBLE);
            content.setVisibility(NestedScrollView.INVISIBLE);
        }

    }

    @Override
    public void stopLoading() {
        if (getActivity() != null) {
            progressBar.setVisibility(ProgressBar.INVISIBLE);
            content.setVisibility(NestedScrollView.VISIBLE);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_filter:
                showDialog();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_payments_history, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @SuppressLint("CommitTransaction")
    private void showDialog() {
        if (getActivity() != null) {
            FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
            Fragment prev = getActivity().getSupportFragmentManager().findFragmentByTag(FILTER_DIALOG_TAG);
            if (prev != null) {
                ft.remove(prev);
            }
            ft.addToBackStack(null);

            DateFilterDialogFragment dialogFragment = DateFilterDialogFragment.newInstance(from, to);
            dialogFragment.setTargetFragment(this, 0);
            if (getFragmentManager() != null) {
                dialogFragment.show(getFragmentManager().beginTransaction(), FILTER_DIALOG_TAG);
            }
        }

    }

    @Override
    public void showPaymentHistory(PaymentHistory paymentHistory) {
        if (getActivity() != null) {
            showInfoInChart(paymentHistory.getRecords());
            adapter = new PaymentHistoryAdapter(paymentHistory.getRecords(), getContext());
            paymentsView.setAdapter(adapter);
            showInfoBlock(paymentHistory.getFrom(), paymentHistory.getTo());
        }
    }

    private void showInfoBlock(Date from, Date to) {
        DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT);
        String dateString;
        if (from != null && to != null) {
            dateString = String.format("%s %s %s %s", getString(R.string.showing_results_from), df.format(from), getString(R.string.until), df.format(to));
        } else
            dateString = getString(R.string.no_info_in_this_date_range);

        paymentHistoryBlockInformation.setMessage(dateString);
        paymentHistoryBlockInformation.setIcon(ContextCompat.getDrawable(Objects.requireNonNull(getContext()), R.drawable.ic_info));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void showInfoInChart(List<PaymentHistoryRecord> list) {
        List<BarEntry> barEntries = new ArrayList<>();
        List<String> months = new ArrayList<>();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM", Locale.getDefault());
        for (int i = 0; i < list.size(); i++) {
            barEntries.add(new BarEntry(i, list.get(i).getSum().floatValue()));
            months.add(sdf.format(list.get(i).getDate()));
        }

        BarDataSet dataSet = new BarDataSet(barEntries, "платежи");
        dataSet.setColors(
                Material.Teal._200.getAsColor(),
                Material.Blue._200.getAsColor(),
                Material.Yellow._200.getAsColor(),
                Material.Pink._200.getAsColor(),
                Material.LightGreen._200.getAsColor(),
                Material.DeepOrange._200.getAsColor(),
                Material.Amber._200.getAsColor(),
                Material.LightGreen._200.getAsColor());
        BarData barData = new BarData(dataSet);
        barData.setDrawValues(true);
        barData.setValueTextColor(Material.BlueGrey._400.getAsColor());
        barData.setValueTextSize(11f);
        chart.setData(barData);
        chart.setDrawGridBackground(false);
        chart.getDescription().setEnabled(false);
        chart.getLegend().setEnabled(false);
        chart.setScaleYEnabled(false);
        chart.setVisibleXRangeMinimum(4);
        chart.setVisibleXRangeMaximum(6);
        chart.setDoubleTapToZoomEnabled(false);
        chart.setExtraBottomOffset(10);
        chart.setAutoScaleMinMaxEnabled(true);
        chart.moveViewTo(dataSet.getXMax(), 1f, YAxis.AxisDependency.RIGHT);
        chart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {
                int position = Objects.requireNonNull(paymentsView.getAdapter()).getItemCount() - 1 - ((int) e.getX());
                mSmoothScroller.setTargetPosition(position);
                mLayoutManager.startSmoothScroll(mSmoothScroller);
                highlightPosition = position;
            }

            @Override
            public void onNothingSelected() {
            }
        });
        chart.getAxisRight().setEnabled(false);
        YAxis leftAxis = chart.getAxisLeft();
        leftAxis.setEnabled(true);
        leftAxis.setDrawLabels(false);
        leftAxis.setDrawAxisLine(false);
        leftAxis.setDrawGridLines(false);
        XAxis xAxis = chart.getXAxis();
        xAxis.setDrawGridLines(false);
        xAxis.setGranularity(1f);
        xAxis.setDrawAxisLine(true);
        xAxis.setValueFormatter(new MonthValueFormatter(months));
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        chart.invalidate();
    }

    interface HighlighterListener {
        void highlight(int position);
    }

    private class MonthValueFormatter implements IAxisValueFormatter {

        private final List<String> xAxisLabels;

        MonthValueFormatter(List<String> xAxisLabels) {
            this.xAxisLabels = xAxisLabels;
        }

        @Override
        public String getFormattedValue(float value, AxisBase axis) {
            return xAxisLabels.get((int) value);
        }
    }

    @Override
    public void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }
}
