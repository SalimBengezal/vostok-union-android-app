package bengezal.salim.ru.vostokunion.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import bengezal.salim.ru.vostokunion.R;
import bengezal.salim.ru.vostokunion.adapters.DAO.chargesAndPayment.Payment;

public class PaymentsAdapter extends RecyclerView.Adapter<PaymentsAdapter.ViewHolder> {

    private final List<Payment> capPayments = new ArrayList<>();

    PaymentsAdapter(List<Payment> records) {
        capPayments.addAll(records);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_charges_and_payment_payment_details, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Payment payment = capPayments.get(position);
        DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT);
        if (payment.getPaymentDate() != null) {
            holder.tvPaymentDate.setText(df.format(payment.getPaymentDate()));
        }
        if (payment.getPaymentSource() != null) {
            holder.tvPaymentSource.setText(payment.getPaymentSource());
        }
        if (payment.getPaymentAmount() != null) {
            holder.tvPaymentAmount.setText(String.format(Locale.getDefault(), "%.2f руб.", payment.getPaymentAmount()));
        }
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
            ((LinearLayoutManager) recyclerView.getLayoutManager()).setRecycleChildrenOnDetach(true);
        }
    }

    @Override
    public int getItemCount() {
        return capPayments.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView tvPaymentDate;
        private final TextView tvPaymentAmount;
        private final TextView tvPaymentSource;

        ViewHolder(View view) {
            super(view);
            tvPaymentDate = view.findViewById(R.id.item_charges_and_payment_payment_details_paymentDate);
            tvPaymentAmount = view.findViewById(R.id.item_charges_and_payment_payment_details_paymentAmount);
            tvPaymentSource = view.findViewById(R.id.item_charges_and_payment_payment_details_paymentSource);
        }
    }

}
