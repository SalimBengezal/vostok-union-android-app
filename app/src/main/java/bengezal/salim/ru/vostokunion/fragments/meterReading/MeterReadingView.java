package bengezal.salim.ru.vostokunion.fragments.meterReading;

import bengezal.salim.ru.vostokunion.fragments.meterReading.dao.MeterReadings;

public interface MeterReadingView {

    void startLoading();

    void stopLoading();

    void showMeterReadings(MeterReadings meterReadings);

    void resultOfEnteringReadings(String message);

}
