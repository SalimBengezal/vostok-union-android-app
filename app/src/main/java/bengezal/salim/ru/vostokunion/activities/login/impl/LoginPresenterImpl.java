package bengezal.salim.ru.vostokunion.activities.login.impl;


import android.util.Log;

import java.util.Map;

import javax.inject.Inject;

import bengezal.salim.ru.vostokunion.activities.login.ErrorStatus;
import bengezal.salim.ru.vostokunion.activities.login.LoginModel;
import bengezal.salim.ru.vostokunion.activities.login.LoginPresenter;
import bengezal.salim.ru.vostokunion.activities.login.LoginView;
import bengezal.salim.ru.vostokunion.injection.App;
import bengezal.salim.ru.vostokunion.managers.CookieManager;
import bengezal.salim.ru.vostokunion.managers.SharedPreferencesManager;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import ru.bengezal.salim.vostok_union_api.VostokUnionAPI;

public class LoginPresenterImpl implements LoginPresenter {

    private static final String TAG = LoginPresenterImpl.class.getSimpleName();
    private final LoginModel model;
    private final LoginView view;
    @Inject
    CookieManager cookieManager;
    @Inject
    SharedPreferencesManager sharedPreferencesManager;
    @Inject
    VostokUnionAPI vostokUnion;

    private final CompositeDisposable disposables = new CompositeDisposable();

    LoginPresenterImpl(LoginView view) {
        App.getComponent().inject(this);
        model = new LoginModelImpl();
        this.view = view;
    }


    public void authenticate(String username, String password) {
        Log.i(TAG, "Attempt to auth with username and password");
        view.startLoading();
        Disposable disposable = model.checkLS(username)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                    switch (result) {
                        case SERVICE_UNAVAILABLE:
                            sharedPreferencesManager.setAccountNumber(username);
                            sharedPreferencesManager.setPassword(password);
                            view.stopLoading();
                            view.showError(ErrorStatus.SERVICE_UNAVAILABLE);
                            break;
                        case NOT_EXIST:
                            sharedPreferencesManager.clearAccountNumberAndPassword();
                            view.stopLoading();
                            view.showError(ErrorStatus.NOT_EXIST);
                            break;
                        case EXIST:
                            login(username, password);
                            break;
                    }
                });
        disposables.add(disposable);
    }

    private void login(String username, String password) {
        Disposable disposable = model.authenticate(username, password)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnComplete(view::stopLoading)
                .subscribe(passed -> {
                    if (passed) {
                        Log.i(TAG, "authentication username with password passed");
                        sharedPreferencesManager.setAccountNumber(username);
                        sharedPreferencesManager.setPassword(password);
                        cookieManager.saveCookie(vostokUnion.getCookies());
                        view.authorizationPassed();
                    } else {
                        Log.e(TAG, "authentication username with password not passed");
                        sharedPreferencesManager.clearAccountNumberAndPassword();
                        view.showError(ErrorStatus.INCORRECT_PASSWORD);
                    }
                }, error -> {
                    Log.e(TAG, "authentication username with password error");
                    view.showError(ErrorStatus.TIMEOUT);
                });
        disposables.add(disposable);
    }

    private void authenticate(Map<String, String> cookies) {
        Log.i(TAG, "Attempt to auth with cookies");
        view.startLoading();
        Disposable disposable = model.authenticate(cookies)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnComplete(view::stopLoading)
                .subscribe(passed -> {
                    if (passed) {
                        Log.i(TAG, "authentication cookies passed");
                        cookieManager.saveCookie(vostokUnion.getCookies());
                        view.authorizationPassed();
                    } else {
                        Log.e(TAG, "authentication cookies not passed");
                        cookieManager.clearCookie();
                        authenticateFromPreferences();
                    }
                }, error -> {
                    Log.e(TAG, "authentication cookies error");
                    cookieManager.clearCookie();
                    authenticateFromPreferences();
                });
        disposables.add(disposable);
    }

    @Override
    public void authenticateFromPreferences() {
        Map<String, String> cookies = cookieManager.getCookie();
        if (!cookies.isEmpty()) {
            authenticate(cookies);
        } else {
            String accountNumber = sharedPreferencesManager.getAccountNumber();
            String password = sharedPreferencesManager.getPassword();
            if (accountNumber != null && password != null) {
                authenticate(accountNumber, password);
            }
        }
    }

    @Override
    public void onDestroy() {
        disposables.dispose();
    }

}
