package bengezal.salim.ru.vostokunion.fragments.meterReading.dao;

import java.util.ArrayList;
import java.util.List;

public class NewMeterReadings {

    private final String action;
    private final String pokazLS;
    private final List<Record> meterReadings;

    public NewMeterReadings(String action, String pokazLS) {
        this.action = action;
        this.pokazLS = pokazLS;
        meterReadings = new ArrayList<>();
    }

    public String getAction() {
        return action;
    }

    public String getPokazLS() {
        return pokazLS;
    }

    public List<Record> getMeterReadings() {
        return meterReadings;
    }

    public void addRecord(String inputName, Double newValue, String hiddenName, String hiddenVal) {
        meterReadings.add(new Record(inputName, hiddenName, hiddenVal, newValue));
    }

    public class Record {
        private final String inputName;
        private final String hiddenName;
        private final String hiddenVal;
        private final Double newValue;

        Record(String inputName, String hiddenName, String hiddenVal, Double newValue) {
            this.inputName = inputName;
            this.hiddenName = hiddenName;
            this.hiddenVal = hiddenVal;
            this.newValue = newValue;
        }

        public String getInputName() {
            return inputName;
        }

        public String getHiddenName() {
            return hiddenName;
        }

        public String getHiddenVal() {
            return hiddenVal;
        }

        public Double getNewValue() {
            return newValue;
        }
    }

}
