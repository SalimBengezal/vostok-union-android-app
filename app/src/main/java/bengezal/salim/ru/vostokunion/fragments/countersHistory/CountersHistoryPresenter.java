package bengezal.salim.ru.vostokunion.fragments.countersHistory;

import java.util.Date;

public interface CountersHistoryPresenter {

    void showCountersHistory(Date from, Date to);

    void onDestroy();

}
