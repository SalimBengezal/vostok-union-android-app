package bengezal.salim.ru.vostokunion.injection.modules;

import android.arch.persistence.room.Room;
import android.content.Context;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import javax.inject.Singleton;

import bengezal.salim.ru.vostokunion.database.AppDatabase;
import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    private final Context context;
    private final AppDatabase db;

    public AppModule(Context context) {
        this.context = context;
        //TODO:Replace
        this.db = Room.databaseBuilder(context, AppDatabase.class, "database").fallbackToDestructiveMigration().build();
    }

    @Provides
    @Singleton
    Context provideContext() {
        return context;
    }

    @Provides
    @Singleton
    AppDatabase provideDatabase() {
        return db;
    }

    @Provides
    @Singleton
    Executor provideExecutor() {
        return Executors.newFixedThreadPool(2);
    }
}
