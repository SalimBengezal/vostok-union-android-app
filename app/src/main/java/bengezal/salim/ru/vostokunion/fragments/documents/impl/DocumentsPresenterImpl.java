package bengezal.salim.ru.vostokunion.fragments.documents.impl;

import android.util.Log;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import bengezal.salim.ru.vostokunion.adapters.DocumentsAdapter;
import bengezal.salim.ru.vostokunion.database.entities.Extraction;
import bengezal.salim.ru.vostokunion.database.entities.Receipt;
import bengezal.salim.ru.vostokunion.fragments.documents.DocumentsModel;
import bengezal.salim.ru.vostokunion.fragments.documents.DocumentsPresenter;
import bengezal.salim.ru.vostokunion.fragments.documents.DocumentsView;
import bengezal.salim.ru.vostokunion.injection.App;
import bengezal.salim.ru.vostokunion.managers.FileManager;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class DocumentsPresenterImpl implements DocumentsPresenter {

    private static final DateFormat DF_MONTH_AND_YEAR = new SimpleDateFormat("LLLL yyyy", new Locale("ru"));
    private static final DateFormat DF_DAY_MONTH_YEAR = new SimpleDateFormat("dd MMMM yyyy", new Locale("ru"));
    private static final DateFormat DF_SHORT = new SimpleDateFormat("dd.MM.yyyy", new Locale("ru"));
    private static final DateFormat DF_TIME = new SimpleDateFormat("HH:mm", new Locale("ru"));
    private static final String TAG = DocumentsPresenterImpl.class.getName();

    private final DocumentsModel model;
    private final DocumentsView view;
    private final CompositeDisposable disposables = new CompositeDisposable();
    @Inject
    FileManager fileManager;

    DocumentsPresenterImpl(DocumentsView view) {
        this.model = new DocumentsModelImpl();
        this.view = view;
        App.getComponent().inject(this);
    }

    @Override
    public void getMonthsInReceiptList() {
        view.startLoading();
        Disposable disposable = model.getReceiptList()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(e -> {
                    view.showMonthsInReceiptList(e);
                    view.stopLoading();
                });
        disposables.add(disposable);
    }

    @Override
    public void downloadReceipt(Date date) {
        view.startLoading();
        Disposable disposable = model.downloadReceipt(date)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(bytes -> {
                    Log.i(TAG, "Saving receipt file to card");
                    return fileManager.saveReceiptFile(date, bytes);
                })
                .map(path -> {
                    Receipt receipt = new Receipt();
                    receipt.setPathToFile(path);
                    receipt.setMonth(date);
                    receipt.setCreationDateTime(new Date().getTime());
                    model.saveReceiptToDB(receipt);
                    //TODO:what to return?
                    return path;
                })
                .doOnComplete(() -> {
                    view.stopLoading();
                    loadCachedReceipts();
                })
                .subscribe();
        disposables.add(disposable);
    }

    @Override
    public void downloadExtraction(Date startPeriod, Date endPeriod) {
        view.startLoading();
        Disposable disposable = model.downloadExtraction(startPeriod, endPeriod)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(bytes -> {
                    Log.i(TAG, "Saving extraction file to card");
                    return fileManager.saveExtractionFile(startPeriod, endPeriod, bytes);
                })
                .map(path -> {
                    Extraction extraction = new Extraction();
                    extraction.setStartPeriod(startPeriod);
                    extraction.setEndPeriod(endPeriod);
                    extraction.setPathToFile(path);
                    extraction.setCreationDateTime(new Date().getTime());
                    model.saveExtractionToDB(extraction);
                    //TODO:What to return?
                    return path;
                })
                .doOnComplete(() -> {
                    view.stopLoading();
                    loadCachedExtractions();
                })
                .subscribe();
        disposables.add(disposable);
    }

    @Override
    public void openStoredReceipt(long id) {
        Disposable disposable = model.getReceiptById(id)
                .subscribeOn(Schedulers.io())
                .subscribe(receipt -> {
                    String tempName = DF_MONTH_AND_YEAR.format(receipt.getMonth());
                    openFile(receipt.getPathToFile(), tempName);
                });
        disposables.add(disposable);
    }

    @Override
    public void openStoredExtraction(long id) {
        Disposable disposable = model.getExtractionById(id)
                .subscribeOn(Schedulers.io())
                .subscribe(extraction -> {
                    String tempName = DF_MONTH_AND_YEAR.format(extraction.getStartPeriod()) + "-" + DF_MONTH_AND_YEAR.format(extraction.getEndPeriod());
                    openFile(extraction.getPathToFile(), tempName);
                });
        disposables.add(disposable);
    }

    private void openFile(String privatePath, String tempName) {
        try {
            String pathToPublicFile = fileManager.getFile(privatePath, tempName);
            if (pathToPublicFile != null) {
                view.openReceiptFile(pathToPublicFile);
            } else {
                view.fileNotFound(tempName);
            }
        } catch (Exception e) {
            Log.e(TAG, "External directory is not writable");
        }
    }

    @Override
    public void loadCachedReceipts() {
        Disposable disposable = model.getCachedReceipts()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .map(receipts -> {
                    List<DocumentsAdapter.Document> documents = new ArrayList<>();
                    for (Receipt record : receipts) {
                        String name = DF_MONTH_AND_YEAR.format(record.getMonth());
                        String description = String.format("Загружен %s в %s", DF_DAY_MONTH_YEAR.format(record.getCreationDateTime()), DF_TIME.format(record.getCreationDateTime()));
                        documents.add(new DocumentsAdapter.Document(name, description, record.getUid(), DocumentsAdapter.Document.TYPE.RECEIPT));
                    }
                    Collections.reverse(documents);
                    return documents;
                })
                .subscribe(receipts -> view.showCachedDocumentsList(receipts, "Загруженные квитанции"));
        disposables.add(disposable);
    }

    @Override
    public void loadCachedExtractions() {
        Disposable disposable = model.getCachedExtractions()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .map(extractions -> {
                    List<DocumentsAdapter.Document> documents = new ArrayList<>();
                    for (Extraction record : extractions) {
                        String name = "С " + DF_SHORT.format(record.getStartPeriod()) + " по " + DF_SHORT.format(record.getEndPeriod());
                        String description = String.format("Загружен %s в %s", DF_DAY_MONTH_YEAR.format(record.getCreationDateTime()), DF_TIME.format(record.getCreationDateTime()));
                        documents.add(new DocumentsAdapter.Document(name, description, record.getUid(), DocumentsAdapter.Document.TYPE.EXTRACTION));
                    }
                    Collections.reverse(documents);
                    return documents;
                }).subscribe(extractions -> view.showCachedDocumentsList(extractions, "Загруженные выписки"));
        disposables.add(disposable);
    }

    @Override
    public void deleteReceipt(long id) {
        Disposable disposable = model.getReceiptById(id)
                .subscribeOn(Schedulers.io())
                .subscribe(receipt -> {
                    if (!fileManager.deleteFile(receipt.getPathToFile())) {
                        view.fileNotFound(receipt.getPathToFile());
                    }
                    model.deleteReceiptFromDB(receipt);
                    loadCachedReceipts();
                });
        disposables.add(disposable);
    }

    @Override
    public void deleteExtraction(long id) {
        Disposable disposable = model.getExtractionById(id)
                .subscribeOn(Schedulers.io())
                .subscribe(extraction -> {
                    if (!fileManager.deleteFile(extraction.getPathToFile())) {
                        view.fileNotFound(extraction.getPathToFile());
                    }
                    model.deleteExtractionFromDB(extraction);
                    loadCachedExtractions();
                });
        disposables.add(disposable);
    }

    public void onDestroy() {
        disposables.dispose();
    }
}
