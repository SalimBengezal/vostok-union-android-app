package bengezal.salim.ru.vostokunion.adapters;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mikepenz.materialize.color.Material;

import java.text.DateFormat;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import bengezal.salim.ru.vostokunion.R;
import bengezal.salim.ru.vostokunion.fragments.home.dao.CounterData;

public class HomeFragmentCountersAdapter extends RecyclerView.Adapter<HomeFragmentCountersAdapter.ViewHolder> {


    private final List<CounterData> counters;
    private final Context context;

    public HomeFragmentCountersAdapter(List<CounterData> counters, Context context) {
        this.counters = counters;
        this.context = context;
        Collections.sort(counters);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_home_fragment_counter, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        CounterData counter = counters.get(position);
        holder.block.getBackground().setColorFilter(getBackgroundColorForBlock(counter), PorterDuff.Mode.SRC_OVER);
        holder.icon.setImageDrawable(getIcon(counter));
        holder.icon.setColorFilter(Material.White._1000.getAsColor());
        holder.name.setText(getName(counter));
        holder.name.setTextColor(getHeaderColor(counter));
        holder.value.setTextColor(Material.White._1000.getAsColor());
        holder.value.setBackgroundColor(getBackgroundColorForValue(counter));
        holder.value.setText(String.format(Locale.getDefault(), "%.1f", counter.getLastReading()));

        holder.block.setOnClickListener(view -> {
            DateFormat sdf = DateFormat.getDateInstance();
            String message = String.format("%s: №%s\n%s: %s %s\n%s: %s",
                    context.getString(R.string.serial_number), counter.getCounterNumber(),
                    context.getString(R.string.value), counter.getLastReading(), getUnit(counter),
                    context.getString(R.string.reading_date), sdf.format(counter.getLastReadingDate()));
            AlertDialog ad = new AlertDialog.Builder(context)
                    .setMessage(message)
                    .setTitle(getFullName(counter))
                    .setNegativeButton(android.R.string.ok, (dialogInterface, i) -> dialogInterface.cancel())
                    .setIcon(R.drawable.ic_counter)
                    .create();
            ad.show();
        });
    }

    private String getFullName(CounterData counter) {
        switch (counter.getType()) {
            case HOT_WATER:
                return context.getString(R.string.hot_water);
            case COLD_WATER:
                return context.getString(R.string.cold_water);
            case ELECTRICITY:
                switch (counter.getTariff()) {
                    case T1:
                        return String.format("%s (%s)", context.getString(R.string.electricity), context.getString(R.string.T1));
                    case T2:
                        return String.format("%s (%s)", context.getString(R.string.electricity), context.getString(R.string.T2));
                    case T3:
                        return String.format("%s (%s)", context.getString(R.string.electricity), context.getString(R.string.T3));
                }
            default:
                return "";
        }
    }

    private String getUnit(CounterData counter) {
        switch (counter.getType()) {
            case ELECTRICITY:
                return context.getString(R.string.kWh);
            case COLD_WATER:
            case HOT_WATER:
                return context.getString(R.string.cubic_meter);
            default:
                return "";
        }
    }

    private int getBackgroundColorForValue(CounterData counter) {
        switch (counter.getType()) {
            case HOT_WATER:
                return Material.Red._700.getAsColor();
            case COLD_WATER:
                return Material.Blue._700.getAsColor();
            case ELECTRICITY:
                switch (counter.getTariff()) {
                    case T1:
                        return Material.Teal._700.getAsColor();
                    case T2:
                        return Material.Green._700.getAsColor();
                    case T3:
                        return Material.BlueGrey._700.getAsColor();
                }
            default:
                return Material.Grey._700.getAsColor();
        }
    }

    private int getHeaderColor(CounterData counter) {
        switch (counter.getType()) {
            case HOT_WATER:
                return Material.Red._200.getAsColor();
            case COLD_WATER:
                return Material.Blue._900.getAsColor();
            case ELECTRICITY:
                switch (counter.getTariff()) {
                    case T1:
                        return Material.Teal._900.getAsColor();
                    case T2:
                        return Material.Green._900.getAsColor();
                    case T3:
                        return Material.BlueGrey._900.getAsColor();
                }
            default:
                return Material.Grey._900.getAsColor();
        }
    }

    private String getName(CounterData counter) {
        switch (counter.getType()) {
            case HOT_WATER:
                return context.getString(R.string.hot_water_short);
            case COLD_WATER:
                return context.getString(R.string.cold_water_short);
            case ELECTRICITY:
                switch (counter.getTariff()) {
                    case T1:
                        return context.getString(R.string.T1);
                    case T2:
                        return context.getString(R.string.T2);
                    case T3:
                        return context.getString(R.string.T3);
                }
            default:
                return "";
        }
    }

    private Drawable getIcon(CounterData counter) {
        switch (counter.getType()) {
            case ELECTRICITY:
                return ContextCompat.getDrawable(context, R.drawable.ic_electricity2);
            case COLD_WATER:
                return ContextCompat.getDrawable(context, R.drawable.ic_cold_water);
            case HOT_WATER:
                return ContextCompat.getDrawable(context, R.drawable.ic_hot_water);
            default:
                return ContextCompat.getDrawable(context, R.drawable.ic_counter);
        }
    }

    private int getBackgroundColorForBlock(CounterData counter) {
        switch (counter.getType()) {
            case HOT_WATER:
                return Material.Red._600.getAsColor();
            case COLD_WATER:
                return Material.Blue._600.getAsColor();
            case ELECTRICITY:
                switch (counter.getTariff()) {
                    case T1:
                        return Material.Teal._600.getAsColor();
                    case T2:
                        return Material.Green._600.getAsColor();
                    case T3:
                        return Material.BlueGrey._600.getAsColor();
                }
            default:
                return Material.Grey._600.getAsColor();
        }
    }

    @Override
    public int getItemCount() {
        return counters.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final LinearLayout block;
        private final TextView name;
        private final ImageView icon;
        private final TextView value;

        ViewHolder(View view) {
            super(view);
            block = view.findViewById(R.id.ll_CounterBlock);
            name = view.findViewById(R.id.tv_CounterBlockName);
            icon = view.findViewById(R.id.iv_CounterBlockIcon);
            value = view.findViewById(R.id.tv_CounterBlockValue);
        }
    }

}
