package bengezal.salim.ru.vostokunion.fragments.meterReading;

import io.reactivex.Observable;
import ru.bengezal.salim.vostok_union_api.dto.DeleteMeterReadingDTO;
import ru.bengezal.salim.vostok_union_api.dto.MeterReadingsDTO;
import ru.bengezal.salim.vostok_union_api.dto.NewMeterReadingsDTO;

public interface MeterReadingModel {

    Observable<MeterReadingsDTO> getMeterReadingsInfo();

    Observable<String> deleteMeterReadingsValue(DeleteMeterReadingDTO meterReadingsDeleteValueDAO);

    Observable<String> saveMeterReadings(NewMeterReadingsDTO meterReadingsDTO);
}
