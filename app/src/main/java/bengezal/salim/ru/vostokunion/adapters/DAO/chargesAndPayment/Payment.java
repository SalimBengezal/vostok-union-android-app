package bengezal.salim.ru.vostokunion.adapters.DAO.chargesAndPayment;

import java.util.Date;

public class Payment {

    private final Date paymentDate;
    private final Double paymentAmount;
    private final String paymentSource;

    public Payment(Date paymentDate, Double paymentAmount, String paymentSource) {
        this.paymentDate = paymentDate;
        this.paymentAmount = paymentAmount;
        this.paymentSource = paymentSource;
    }

    public Date getPaymentDate() {
        return paymentDate;
    }

    public Double getPaymentAmount() {
        return paymentAmount;
    }

    public String getPaymentSource() {
        return paymentSource;
    }
}
