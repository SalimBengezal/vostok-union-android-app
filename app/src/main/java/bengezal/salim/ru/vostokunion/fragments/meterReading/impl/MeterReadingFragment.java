package bengezal.salim.ru.vostokunion.fragments.meterReading.impl;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

import bengezal.salim.ru.vostokunion.R;
import bengezal.salim.ru.vostokunion.adapters.MeterReadingsAdapter;
import bengezal.salim.ru.vostokunion.components.BlockInformationView;
import bengezal.salim.ru.vostokunion.fragments.meterReading.MeterReadingPresenter;
import bengezal.salim.ru.vostokunion.fragments.meterReading.MeterReadingView;
import bengezal.salim.ru.vostokunion.fragments.meterReading.dao.DeleteMeterReading;
import bengezal.salim.ru.vostokunion.fragments.meterReading.dao.MeterReadings;
import bengezal.salim.ru.vostokunion.fragments.meterReading.dao.NewMeterReadings;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class MeterReadingFragment extends Fragment implements MeterReadingView {

    private MeterReadingPresenter presenter;
    private final DeleteValueListener deleteValueListener = new DeleteValueListener() {
        @Override
        public void onDeleteValue(MeterReadings.Record record) {
            MeterReadings.Record.DeleteLink deleteLink = record.getDeleteLink();
            DeleteMeterReading deleteMeterReading = new DeleteMeterReading(deleteLink.getRowIdSch(), deleteLink.getTariff(), deleteLink.getDate(), deleteLink.getPokazLs(), deleteLink.getAction());
            presenter.deleteMeterReadingsValue(deleteMeterReading);
        }
    };
    @BindView(R.id.meter_readings_progressBar)
    ProgressBar progressBar;
    @BindView(R.id.meter_readings_rvRecords)
    RecyclerView rvRecords;
    @BindView(R.id.meter_readings_content)
    LinearLayout content;
    Unbinder unbinder;
    @BindView(R.id.meter_readings_info_block)
    BlockInformationView meterReadingsBlockInformation;
    private MeterReadingsAdapter adapter;
    private MenuItem menuItemSaveMeterReadings;

    public static MeterReadingFragment getInstance() {
        return new MeterReadingFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_meter_readings, container, false);
        super.onCreateView(inflater, container, savedInstanceState);
        presenter = new MeterReadingPresenterImpl(this);
        unbinder = ButterKnife.bind(this, view);
        presenter.showMeterReadings();

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        rvRecords.setLayoutManager(mLayoutManager);
        rvRecords.setHasFixedSize(true);
        rvRecords.setNestedScrollingEnabled(false);
        MeterReadingsAdapter adapter = new MeterReadingsAdapter(new MeterReadings(null, null, null, null), getContext(), deleteValueListener);
        rvRecords.setAdapter(adapter);

        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void startLoading() {
        if (getActivity() != null) {
            progressBar.setVisibility(ProgressBar.VISIBLE);
            content.setVisibility(NestedScrollView.INVISIBLE);
        }
    }

    @Override
    public void stopLoading() {
        if (getActivity() != null) {
            progressBar.setVisibility(ProgressBar.INVISIBLE);
            content.setVisibility(NestedScrollView.VISIBLE);
        }
    }

    @Override
    public void showMeterReadings(MeterReadings meterReadings) {
        if (getActivity() != null) {
            adapter = new MeterReadingsAdapter(meterReadings, getContext(), deleteValueListener);
            rvRecords.setAdapter(adapter);
            showInfoBlock(meterReadings.getStartDate(), meterReadings.getEndDate());

            boolean isEnabled = true;
            boolean acceptedAllReadings = true;
            for (MeterReadings.Record rec : meterReadings.getReadings()) {
                if (!rec.getEnabledEditState()) {
                    isEnabled = false;
                }
                if (rec.getDeleteLink() == null) {
                    acceptedAllReadings = false;
                }
            }
            if (isEnabled && !acceptedAllReadings) {
                menuItemSaveMeterReadings.setVisible(true);
            } else {
                menuItemSaveMeterReadings.setVisible(false);
            }
        }
    }

    private void showInfoBlock(Integer startDay, Integer endDay) {
        DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT);
        String dateString;
        if (startDay != null && endDay != null) {
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.DAY_OF_MONTH, startDay);
            Date from = calendar.getTime();
            calendar.set(Calendar.DAY_OF_MONTH, endDay);
            Date to = calendar.getTime();
            dateString = String.format("%s %s %s %s", getString(R.string.entering_meter_reading_available_from), df.format(from), getString(R.string.until), df.format(to));

            meterReadingsBlockInformation.setMessage(dateString);
            meterReadingsBlockInformation.setIcon(ContextCompat.getDrawable(Objects.requireNonNull(getContext()), R.drawable.ic_info));
            meterReadingsBlockInformation.setVisibility(View.VISIBLE);
        } else {
            meterReadingsBlockInformation.setVisibility(View.GONE);
        }
    }

    @Override
    public void resultOfEnteringReadings(String message) {
        if (getView() != null) {
            Snackbar.make(getView(), message, Snackbar.LENGTH_LONG).show();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_meter_readings, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        menuItemSaveMeterReadings = menu.findItem(R.id.menu_save_meter_readings);
        menuItemSaveMeterReadings.setVisible(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_save_meter_readings:
                saveMeterReadings();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void saveMeterReadings() {
        hideKeyBoard();
        MeterReadings meterReadings = adapter.getMeterReadings();
        if (meterReadings != null) {
            boolean valid = false;
            try {
                valid = true;
            } catch (Exception e) {
                e.printStackTrace();
            }
            NewMeterReadings newMeterReadings = new NewMeterReadings(meterReadings.getAction(), meterReadings.getLs());
            for (MeterReadings.Record record : meterReadings.getReadings()) {
                if (!record.getEnabledEditState()) {
                    valid = false;
                    break;
                }
                newMeterReadings.addRecord(record.getNewReadingInputName(), record.getNewReading(), record.getNewReadingInputHiddenName(), record.getNewReadingInputHiddenVal());
            }
            if (valid) {
                presenter.enterNewMeterReadings(newMeterReadings);
            }
        }
    }

    private void hideKeyBoard() {
        Activity activity = getActivity();
        if (activity != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
            View view = activity.getCurrentFocus();
            if (view == null) {
                view = new View(activity);
            }
            if (imm != null) {
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public interface DeleteValueListener {
        void onDeleteValue(MeterReadings.Record record);
    }

    @Override
    public void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }
}
