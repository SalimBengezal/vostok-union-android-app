package bengezal.salim.ru.vostokunion.adapters.DAO.Counters;

import android.support.annotation.NonNull;

import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class DateRecord implements Comparable<DateRecord> {

    public static final Comparator<DateRecord> DESCENDING_DATE = (d, d1) -> d1.date.compareTo(d.date);
    public final Set<CounterRecord> records;
    public Date date;

    public DateRecord() {
        records = new HashSet<>();
    }

    public Set<Counter> getCountersNumbers() {
        Set<Counter> countersSet = new HashSet<>();
        for (CounterRecord record : records) {
            countersSet.add(new Counter(record.counterNumber, getName(record.counterType)));
        }
        return countersSet;
    }

    private String getName(Counters.TYPE counterType) {
        switch (counterType) {
            case HOT_WATER:
                return "Горячая вода";
            case COLD_WATER:
                return "Холодная вода";
            case ELECTRICITY_T2:
                return "Электричество";
            case ELECTRICITY_T3:
                return "Электричество";
            case ELECTRICITY_T1:
                return "Электричество";
        }
        return null;
    }

    @Override
    public int compareTo(@NonNull DateRecord dateRecord) {
        return this.date.compareTo(dateRecord.date);
    }

    public class Counter {

        public final String number;
        public final String name;

        Counter(String number, String name) {
            this.number = number;
            this.name = name;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Counter counter = (Counter) o;
            return (number != null ? number.equals(counter.number) : counter.number == null) && (name != null ? name.equals(counter.name) : counter.name == null);
        }

        @Override
        public int hashCode() {
            int result = number != null ? number.hashCode() : 0;
            result = 31 * result + (name != null ? name.hashCode() : 0);
            return result;
        }
    }


}
