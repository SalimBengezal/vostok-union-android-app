package bengezal.salim.ru.vostokunion.injection;

import android.app.Application;

import com.squareup.leakcanary.LeakCanary;

import bengezal.salim.ru.vostokunion.injection.modules.AppModule;
import bengezal.salim.ru.vostokunion.injection.modules.NetworkReceiverModule;
import bengezal.salim.ru.vostokunion.injection.modules.UtilsModule;

public class App extends Application {

    private static AppComponent component;

    public static AppComponent getComponent() {
        return component;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        if (LeakCanary.isInAnalyzerProcess(this)) {
            return;
        }
        LeakCanary.install(this);
        component = buildComponent();
    }

    private AppComponent buildComponent() {
        return DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .utilsModule(new UtilsModule())
                .networkReceiverModule(new NetworkReceiverModule())
                .build();
    }
}
