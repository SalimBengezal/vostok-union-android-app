package bengezal.salim.ru.vostokunion.injection.modules;

import android.content.Context;
import android.support.annotation.NonNull;

import java.util.concurrent.Executor;

import javax.inject.Singleton;

import bengezal.salim.ru.vostokunion.database.AppDatabase;
import bengezal.salim.ru.vostokunion.database.LocalRepository;
import bengezal.salim.ru.vostokunion.database.LocalRepositoryImpl;
import bengezal.salim.ru.vostokunion.managers.CookieManager;
import bengezal.salim.ru.vostokunion.managers.FileManager;
import bengezal.salim.ru.vostokunion.managers.SharedPreferencesManager;
import dagger.Module;
import dagger.Provides;

@Module
public class UtilsModule {

    @Provides
    @NonNull
    @Singleton
    SharedPreferencesManager provideSharedPreferencesManager(Context context) {
        return new SharedPreferencesManager(context);
    }

    @Provides
    @NonNull
    @Singleton
    CookieManager provideCookieManager(Context context) {
        return new CookieManager(context);
    }

    @Provides
    @NonNull
    @Singleton
    FileManager provideFileManager(Context context) {
        return new FileManager(context);
    }

    @Provides
    @NonNull
    @Singleton
    LocalRepository provideLocalRepository(AppDatabase database, Executor executor) {
        return new LocalRepositoryImpl(database, executor);
    }

}
