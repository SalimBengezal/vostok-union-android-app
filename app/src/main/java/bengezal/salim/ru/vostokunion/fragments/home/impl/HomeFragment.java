package bengezal.salim.ru.vostokunion.fragments.home.impl;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.mikepenz.materialize.color.Material;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import bengezal.salim.ru.vostokunion.R;
import bengezal.salim.ru.vostokunion.adapters.HomeFragmentCountersAdapter;
import bengezal.salim.ru.vostokunion.fragments.home.HomeFragmentPresenter;
import bengezal.salim.ru.vostokunion.fragments.home.HomeFragmentView;
import bengezal.salim.ru.vostokunion.fragments.home.dao.ChargesData;
import bengezal.salim.ru.vostokunion.fragments.home.dao.CounterData;
import bengezal.salim.ru.vostokunion.fragments.home.dao.HomeFragmentInfoAndBalanceData;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class HomeFragment extends Fragment implements HomeFragmentView {

    private static final String TAG = HomeFragment.class.getSimpleName();
    @BindView(R.id.home_progressBar)
    ProgressBar homeProgressBar;
    @BindView(R.id.home_container)
    LinearLayout homeContainer;
    @BindView(R.id.bc_consumptionDiagram)
    BarChart chart;
    @BindView(R.id.tv_accountNumber)
    TextView tvAccountNumber;
    @BindView(R.id.tv_accountOwner)
    TextView tvAccountOwner;
    @BindView(R.id.tv_accountAddress)
    TextView tvAccountAddress;
    @BindView(R.id.tv_BalanceLabel)
    TextView tvBalanceLabel;
    @BindView(R.id.tv_BalanceValue)
    TextView tvBalanceValue;
    @BindView(R.id.tv_BalanceDebtDate)
    TextView tvBalanceDebtDate;
    @BindView(R.id.tv_BalanceDebtValue)
    TextView tvBalanceDebtValue;
    @BindView(R.id.tv_BalanceAccruedDate)
    TextView tvBalanceAccruedDate;
    @BindView(R.id.tv_BalanceAccruedValue)
    TextView tvBalanceAccruedValue;
    @BindView(R.id.tv_BalanceLastDatePaymentLabel)
    TextView tvBalanceLastDatePaymentLabel;
    @BindView(R.id.tv_BalanceLastDatePaymentValue)
    TextView tvBalanceLastDatePaymentValue;
    @BindView(R.id.tv_BalanceIncludedPenaltiesMessage)
    TextView tvBalanceIncludedPenaltiesMessage;
    @BindView(R.id.tv_BalanceCurrentMonthPaidLabel)
    TextView tvBalanceCurrentMonthPaidLabel;
    @BindView(R.id.tv_BalanceCurrentMonthPaidValue)
    TextView tvBalanceCurrentMonthPaidValue;
    @BindView(R.id.btn_BalancePayReceipt)
    AppCompatButton btnBalancePayReceipt;
    @BindView(R.id.rv_countersBlocks)
    RecyclerView rvCountersBlocks;
    @BindView(R.id.tv_countersLastDateReading)
    TextView tvCountersLastDateReading;

    private Unbinder unbinder;
    private OnFragmentInteractionListener mListener;
    private HomeFragmentCountersAdapter countersAdapter;
    private HomeFragmentPresenter presenter;

    public static HomeFragment getInstance() {
        return new HomeFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        unbinder = ButterKnife.bind(this, view);
        presenter = new HomeFragmentPresenterImpl(this);
        presenter.loadInfoAndBalance();
        presenter.loadCounters();

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.add(Calendar.MONTH, -1);
        Date to = calendar.getTime();
        calendar.add(Calendar.MONTH, -11);
        Date from = calendar.getTime();
        presenter.loadCharges(from, to);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        rvCountersBlocks.setLayoutManager(linearLayoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rvCountersBlocks.getContext(), linearLayoutManager.getOrientation());
        dividerItemDecoration.setDrawable(getResources().getDrawable(R.drawable.divider));
        rvCountersBlocks.addItemDecoration(dividerItemDecoration);
        countersAdapter = new HomeFragmentCountersAdapter(new ArrayList<>(), getContext());
        rvCountersBlocks.setAdapter(countersAdapter);

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_exit:
                mListener.onLogoutRequest();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        mListener = null;
        super.onDetach();
    }

    private void showInfo(HomeFragmentInfoAndBalanceData data) {
        Log.i(TAG, "Showing data");
        if (getActivity() != null) {

            //region Block Information
            String shortCurrencyName = getString(R.string.short_currency_name);

            String accountNumber = String.format("%s: %s", getString(R.string.account_number), data.getAccountNumber());
            tvAccountNumber.setText(accountNumber);

            String accountOwner = data.getAccountName();
            tvAccountOwner.setText(accountOwner);

            String accountAddress = data.getAccountAddress();
            tvAccountAddress.setText(accountAddress);
            //endregion

            //region Block Balance

            double paidCurMonth = data.getReceiptPaidCurrentMonth();
            double curBalance = paidCurMonth + data.getReceiptCurrentBalance();
            if (curBalance > 0) {
                tvBalanceValue.setTextColor(Material.Green._300.getAsColor());
                tvBalanceLabel.setText(R.string.prepayment);
            } else {
                tvBalanceLabel.setText(R.string.debt);
                if (curBalance == 0) {
                    tvBalanceValue.setTextColor(Material.Green._300.getAsColor());
                } else {
                    tvBalanceValue.setTextColor(Material.Red._300.getAsColor());
                }
            }

            Double balance;
            if (data.getReceiptPenalties() > 0) {
                balance = Math.abs(paidCurMonth - data.getReceiptTotalPaymentWithPenalties());
                tvBalanceIncludedPenaltiesMessage.setText(String.format(Locale.getDefault(), "(%s: %.2f %s)", getString(R.string.include_penalties), data.getReceiptPenalties(), shortCurrencyName));
                tvBalanceIncludedPenaltiesMessage.setVisibility(View.VISIBLE);
            } else {
                balance = Math.abs(paidCurMonth + data.getReceiptCurrentBalance());
                tvBalanceIncludedPenaltiesMessage.setVisibility(View.GONE);
            }
            tvBalanceValue.setText(String.format(Locale.getDefault(), "%.2f %s", balance, shortCurrencyName));

            SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
            String debtDate = sdf.format(data.getReceiptMonth());
            String balanceDebtDate = String.format(Locale.getDefault(), "%s  %s", getString(R.string.debt_to_date), debtDate);
            tvBalanceDebtDate.setText(balanceDebtDate);

            String debtValue = String.format(Locale.getDefault(), "%.2f %s", data.getReceiptDebt(), shortCurrencyName);
            tvBalanceDebtValue.setText(debtValue);

            sdf = new SimpleDateFormat("MM/yy", Locale.getDefault());
            String month = sdf.format(data.getReceiptMonth());
            String lastPaymentDate = String.format(Locale.getDefault(), "%s %s", getString(R.string.paid_in), month);
            tvBalanceLastDatePaymentLabel.setText(lastPaymentDate);

            String lastPaidValue = String.format(Locale.getDefault(), "%.2f %s", data.getReceiptPaid(), shortCurrencyName);
            tvBalanceLastDatePaymentValue.setText(lastPaidValue);

            String accruedDate = String.format(Locale.getDefault(), "%s %s", getString(R.string.accrued_for), month);
            tvBalanceAccruedDate.setText(accruedDate);

            String accruedValue = String.format(Locale.getDefault(), "%.2f %s", data.getReceiptAccrued(), shortCurrencyName);
            tvBalanceAccruedValue.setText(accruedValue);

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(data.getReceiptMonth());
            calendar.add(Calendar.MONTH, 1);
            String currentMonth = sdf.format(calendar.getTime());
            String paidCurrentMonthLabel = String.format(Locale.getDefault(), "%s %s", getString(R.string.paid_in), currentMonth);
            tvBalanceCurrentMonthPaidLabel.setText(paidCurrentMonthLabel);

            String currentMonthPaid = String.format(Locale.getDefault(), "%.2f %s", data.getReceiptPaidCurrentMonth(), shortCurrencyName);
            tvBalanceCurrentMonthPaidValue.setText(currentMonthPaid);

            if (balance >= 0) {
                btnBalancePayReceipt.setText(R.string.add_funds);
            } else {
                btnBalancePayReceipt.setText(String.format(Locale.getDefault(), "%s %.2f %s", getString(R.string.pay), balance, shortCurrencyName));
            }

            //endregion

        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        chart = null;
    }

    @Override
    public void showInfoAndBalanceData(HomeFragmentInfoAndBalanceData data) {
        if (getActivity() != null) {
            showInfo(data);
        }
    }

    @Override
    public void startLoading() {
        if (getActivity() != null) {
            homeContainer.setVisibility(View.INVISIBLE);
            homeProgressBar.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void stopLoading() {
        if (getActivity() != null) {
            homeContainer.setVisibility(View.VISIBLE);
            homeProgressBar.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void showCountersData(List<CounterData> list) {
        if (getActivity() != null) {
            countersAdapter = new HomeFragmentCountersAdapter(list, getContext());
            rvCountersBlocks.setAdapter(countersAdapter);
            SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
            if (list.size() > 0) {
                Date lastDate = list.get(0).getLastReadingDate();
                for (int i = 0; i < list.size(); i++) {
                    Date newDate = list.get(i).getLastReadingDate();
                    if (lastDate.compareTo(newDate) < 0) {
                        lastDate = newDate;
                    }
                }
                tvCountersLastDateReading.setText(String.format(Locale.getDefault(), "%s %s", getString(R.string.last_reading_from), sdf.format(lastDate)));
            }
        }
    }

    @Override
    public void showChargesMonthly(List<ChargesData> list) {
        if (getActivity() != null) {
            List<BarEntry> barEntries = new ArrayList<>();
            List<String> months = new ArrayList<>();
            for (int i = 0; i < list.size(); i++) {
                barEntries.add(new BarEntry(i, list.get(i).getValue().floatValue()));
                months.add(list.get(i).getMonth());
            }

            BarDataSet dataSet = new MyBarDataSet(barEntries, "жкх");
            dataSet.setColors(Material.DeepOrange._200.getAsColor(), Material.Amber._200.getAsColor(), Material.LightGreen._200.getAsColor());
            BarData barData = new BarData(dataSet);
            barData.setDrawValues(true);
            barData.setValueTextColor(Material.BlueGrey._400.getAsColor());
            barData.setValueTextSize(11f);
            barData.setHighlightEnabled(false);
            chart.setData(barData);
            chart.setDrawGridBackground(false);
            chart.getDescription().setEnabled(false);
            chart.getLegend().setEnabled(false);
            chart.getAxisRight().setEnabled(false);
            chart.setScaleYEnabled(false);
            chart.setVisibleXRangeMinimum(4);
            chart.setDoubleTapToZoomEnabled(false);
            XAxis xAxis = chart.getXAxis();
            chart.setExtraBottomOffset(20);
            chart.zoom(2.4f, 1f, 1f, 1f);
            chart.moveViewTo(dataSet.getXMax(), 1f, YAxis.AxisDependency.RIGHT);
            xAxis.setDrawGridLines(false);
            xAxis.setGranularity(1f);
            xAxis.setDrawAxisLine(true);
            xAxis.setValueFormatter(new MonthValueFormatter(months));
            xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
            chart.invalidate();
        }
    }

    @OnClick(R.id.cl_blockInfo)
    public void onBlockInfoClicked() {
        mListener.onStartApartmentDetails();
    }

    @OnClick(R.id.btn_BalancePayReceipt)
    public void onPayReceiptClicked() {
        mListener.onPayBill();
    }

    @OnClick(R.id.btn_blockCountersAddNewReadings)
    public void onCountersAddNewReadingsClicked() {
        mListener.onAddNewReadings();
    }

    public interface OnFragmentInteractionListener {
        void onLogoutRequest();

        void onStartApartmentDetails();

        void onPayBill();

        void onAddNewReadings();
    }

    private class MonthValueFormatter implements IAxisValueFormatter {

        private final List<String> xAxisLabels;

        MonthValueFormatter(List<String> xAxisLabels) {
            this.xAxisLabels = xAxisLabels;
        }

        @Override
        public String getFormattedValue(float value, AxisBase axis) {
            return xAxisLabels.get((int) value);
        }
    }

    private class MyBarDataSet extends BarDataSet {

        double min;
        double max;
        double mediumLevel;
        double goodLevel;

        MyBarDataSet(List<BarEntry> barEntries, String label) {
            super(barEntries, label);
            if (barEntries.size() > 0) {
                min = barEntries.get(0).getY();
                max = barEntries.get(0).getY();
                for (BarEntry entry : barEntries) {
                    min = entry.getY() < min ? entry.getY() : min;
                    max = entry.getY() > max ? entry.getY() : max;
                }
                mediumLevel = (max - min) / 3 + min;
                goodLevel = 2 * (max - min) / 3 + min;
            }
        }

        @Override
        public int getColor(int index) {
            float val = getEntryForIndex(index).getY();
            if (val <= max && val >= min) {
                if (val >= goodLevel) {
                    return mColors.get(0);
                } else if (val >= mediumLevel) {
                    return mColors.get(1);
                } else {
                    return mColors.get(2);
                }
            }
            return super.getColor(index);
        }
    }

    @Override
    public void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }
}
