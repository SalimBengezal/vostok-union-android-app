package bengezal.salim.ru.vostokunion.activities.main;

import io.reactivex.Observable;
import ru.bengezal.salim.vostok_union_api.dto.MainInfoDTO;

public interface MainModel {

    Observable<MainInfoDTO> getMainInfo();

    Observable<Void> logout();

}
