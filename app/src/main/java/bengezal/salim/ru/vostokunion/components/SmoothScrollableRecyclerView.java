package bengezal.salim.ru.vostokunion.components;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

public class SmoothScrollableRecyclerView extends RecyclerView {


    public SmoothScrollableRecyclerView(Context context) {
        super(context);
    }

    public SmoothScrollableRecyclerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SmoothScrollableRecyclerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public boolean fling(int velocityX, int velocityY) {
        velocityY *= 0.8;
        return super.fling(velocityX, velocityY);
    }

}
