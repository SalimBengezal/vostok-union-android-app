package bengezal.salim.ru.vostokunion.fragments.home;

import java.util.List;

import bengezal.salim.ru.vostokunion.fragments.home.dao.ChargesData;
import bengezal.salim.ru.vostokunion.fragments.home.dao.CounterData;
import bengezal.salim.ru.vostokunion.fragments.home.dao.HomeFragmentInfoAndBalanceData;

public interface HomeFragmentView {

    void showInfoAndBalanceData(HomeFragmentInfoAndBalanceData parcel);

    void startLoading();

    void stopLoading();

    void showCountersData(List<CounterData> list);

    void showChargesMonthly(List<ChargesData> list);
}
