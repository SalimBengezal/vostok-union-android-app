package bengezal.salim.ru.vostokunion.adapters.DAO;

public class DetailsContentValues {

    private final String header;
    private final String value;
    private final int imageRes;

    public DetailsContentValues(String header, String value, int imageRes) {
        this.header = header;
        this.value = value;
        this.imageRes = imageRes;
    }

    public String getHeader() {
        return header;
    }

    public String getValue() {
        return value;
    }

    public int getImageRes() {
        return imageRes;
    }
}
