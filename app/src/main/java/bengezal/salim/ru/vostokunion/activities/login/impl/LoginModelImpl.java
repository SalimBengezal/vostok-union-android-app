package bengezal.salim.ru.vostokunion.activities.login.impl;

import android.util.Log;

import java.util.Map;

import javax.inject.Inject;

import bengezal.salim.ru.vostokunion.activities.login.LoginModel;
import bengezal.salim.ru.vostokunion.injection.App;
import io.reactivex.Observable;
import ru.bengezal.salim.vostok_union_api.VostokUnionAPI;
import ru.bengezal.salim.vostok_union_api.dto.AccountStatus;

public class LoginModelImpl implements LoginModel {

    private static final String TAG = LoginModelImpl.class.getName();

    @Inject
    VostokUnionAPI vostokUnion;

    LoginModelImpl() {
        App.getComponent().inject(this);
    }

    public Observable<AccountStatus> checkLS(String accountNumber) {
        return Observable.create(e -> {
            vostokUnion.setAccountNumber(accountNumber);
            Log.i(TAG, "Checking account number");
            e.onNext(vostokUnion.existAccountNumber());
            e.onComplete();
        });
    }

    @Override
    public Observable<Boolean> authenticate(String accountNumber, String password) {
        return Observable.create(e -> {
            vostokUnion.setAccountNumber(accountNumber);
            vostokUnion.setPassword(password);
            Log.i(TAG, "Authentication with accountNumber and password");
            e.onNext(vostokUnion.connect());
            e.onComplete();
        });
    }

    @Override
    public Observable<Boolean> authenticate(Map<String, String> cookies) {
        return Observable.create(e -> {
            vostokUnion.setCookies(cookies);
            Log.i(TAG, "Authentication with cookies");
            e.onNext(vostokUnion.connect());
            e.onComplete();
        });
    }

}
