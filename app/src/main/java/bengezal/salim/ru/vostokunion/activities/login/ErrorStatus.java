package bengezal.salim.ru.vostokunion.activities.login;

public enum ErrorStatus {

    NOT_EXIST, SERVICE_UNAVAILABLE, INCORRECT_PASSWORD, TIMEOUT

}
