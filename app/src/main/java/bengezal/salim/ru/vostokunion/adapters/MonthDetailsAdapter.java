package bengezal.salim.ru.vostokunion.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;
import java.util.Locale;

import bengezal.salim.ru.vostokunion.R;
import bengezal.salim.ru.vostokunion.parcels.MonthDetailsRecordParcel;

public class MonthDetailsAdapter extends RecyclerView.Adapter<MonthDetailsAdapter.ViewHolder> {


    private final List<MonthDetailsRecordParcel> records;

    public MonthDetailsAdapter(List<MonthDetailsRecordParcel> parcel) {
        this.records = parcel;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_month_details, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        MonthDetailsRecordParcel rec = records.get(position);
        if (rec.getName() != null) {
            holder.tvName.setText(rec.getName());
        } else {
            holder.tvName.setText("");
        }
        if (rec.getTariff() != null) {
            holder.tvTariff.setText(String.format(Locale.getDefault(), "%.2f", rec.getTariff()));
        } else {
            holder.tvTariff.setText("");
        }
        if (rec.getIncomingBalance() != null) {
            holder.tvIncomingBalance.setText(String.format(Locale.getDefault(), "%.2f", rec.getIncomingBalance()));
        } else {
            holder.tvIncomingBalance.setText("");
        }
        if (rec.getCharged() != null) {
            holder.tvCharged.setText(String.format(Locale.getDefault(), "%.2f", rec.getCharged()));
        } else {
            holder.tvCharged.setText("");
        }
        if (rec.getPayments() != null) {
            holder.tvPayments.setText(String.format(Locale.getDefault(), "%.2f", rec.getPayments()));
        } else {
            holder.tvPayments.setText("");
        }
        if (rec.getRecalculation() == null || rec.getRecalculation() == 0) {
            holder.tvLabelRecalculation.setVisibility(View.GONE);
            holder.tvRecalculation.setVisibility(View.GONE);
        } else {
            holder.tvRecalculation.setText(String.format(Locale.getDefault(), "%.2f", rec.getRecalculation()));
        }
        if (rec.getTakenForQuality() == null || rec.getTakenForQuality() == 0) {
            holder.tvTakenForQuality.setVisibility(View.GONE);
            holder.tvLabelTakenForQuality.setVisibility(View.GONE);
        } else {
            holder.tvTakenForQuality.setText(String.format(Locale.getDefault(), "%.2f", rec.getTakenForQuality()));
        }
        if (rec.getOutgoingBalance() != null) {
            holder.tvOutgoingBalance.setText(String.format(Locale.getDefault(), "%.2f", rec.getOutgoingBalance()));
        } else {
            holder.tvOutgoingBalance.setText("");
        }
        if (rec.getVolume() != null) {
            holder.tvVolume.setText(String.format(Locale.getDefault(), "%.2f", rec.getVolume()));
        } else {
            holder.tvVolume.setText("");
        }
    }

    @Override
    public int getItemCount() {
        return records.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView tvName;
        private final TextView tvIncomingBalance;
        private final TextView tvCharged;
        private final TextView tvPayments;
        private final TextView tvRecalculation;
        private final TextView tvTakenForQuality;
        private final TextView tvOutgoingBalance;
        private final TextView tvVolume;
        private final TextView tvTariff;
        private final TextView tvLabelRecalculation;
        private final TextView tvLabelTakenForQuality;

        ViewHolder(View view) {
            super(view);
            tvName = view.findViewById(R.id.item_month_details_nameValue);
            tvIncomingBalance = view.findViewById(R.id.item_month_details_incomingBalanceValue);
            tvCharged = view.findViewById(R.id.item_month_details_chargedValue);
            tvPayments = view.findViewById(R.id.item_month_details_paymentsValue);
            tvRecalculation = view.findViewById(R.id.item_month_details_recalculationValue);
            tvTakenForQuality = view.findViewById(R.id.item_month_details_takenForQualityValue);
            tvOutgoingBalance = view.findViewById(R.id.item_month_details_outgoingBalanceValue);
            tvVolume = view.findViewById(R.id.item_month_details_volumeValue);
            tvTariff = view.findViewById(R.id.item_month_details_tariffValue);
            tvLabelRecalculation = view.findViewById(R.id.item_month_details_recalculationLabel);
            tvLabelTakenForQuality = view.findViewById(R.id.item_month_details_takenForQualityLabel);
        }
    }
}
