package bengezal.salim.ru.vostokunion.fragments.documents;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jaredrummler.materialspinner.MaterialSpinner;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import bengezal.salim.ru.vostokunion.R;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class FragmentReceipt extends Fragment {

    Unbinder unbinder;
    @BindView(R.id.receipts_spinner)
    MaterialSpinner spinner;
    @BindView(R.id.receipts_btnSubmit)
    AppCompatButton receiptsBtnSubmit;
    private List<Date> months;
    private ReceiptListener listener;

    public FragmentReceipt() {
    }

    public static FragmentReceipt getInstance(ReceiptListener listener) {
        FragmentReceipt fragment = new FragmentReceipt();
        fragment.listener = listener;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.documents_tab_receipts, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    public void loadReceiptList(List<Date> dateList) {
        SimpleDateFormat sdf = new SimpleDateFormat("LLLL yyyy", new Locale("ru"));
        months = dateList;
        ArrayList<String> monthNames = new ArrayList<>();
        for (Date d : dateList) {
            monthNames.add(sdf.format(d));
        }
        spinner.setItems(monthNames);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.receipts_btnSubmit)
    public void onSubmitClicked() {
        int position = spinner.getSelectedIndex();
        Date period = months.get(position);
        listener.onSubmit(period);
    }

    public void startLoading() {
        if (receiptsBtnSubmit != null) {
            receiptsBtnSubmit.setEnabled(false);
            receiptsBtnSubmit.setText(R.string.downloading);
        }
    }

    public void stopLoading() {
        if (receiptsBtnSubmit != null) {
            receiptsBtnSubmit.setEnabled(true);
            receiptsBtnSubmit.setText(R.string.generate);
        }
    }

    public interface ReceiptListener {
        void onSubmit(Date period);
    }
}
