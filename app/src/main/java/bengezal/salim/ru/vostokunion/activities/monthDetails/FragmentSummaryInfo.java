package bengezal.salim.ru.vostokunion.activities.monthDetails;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.Group;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Locale;

import bengezal.salim.ru.vostokunion.R;
import bengezal.salim.ru.vostokunion.parcels.MonthDetailsParcel;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class FragmentSummaryInfo extends Fragment {

    private static final String PARCEL_KEY = "summary";

    @BindView(R.id.month_details_incomingBalance_value)
    TextView tvIncomingBalanceValue;
    @BindView(R.id.month_details_charged_value)
    TextView tvChargedValue;
    @BindView(R.id.month_details_payments_value)
    TextView tvPaymentsValue;
    @BindView(R.id.month_details_recalculation)
    TextView tvRecalculation;
    @BindView(R.id.month_details_recalculation_value)
    TextView tvRecalculationValue;
    @BindView(R.id.month_details_takenForQuality)
    TextView tvTakenForQuality;
    @BindView(R.id.month_details_takenForQuality_value)
    TextView tvTakenForQualityValue;
    @BindView(R.id.month_details_outgoingBalance_value)
    TextView tvOutgoingBalanceValue;
    Unbinder unbinder;
    @BindView(R.id.month_details_blockAdditionalInfo)
    Group blockAdditionalInfo;
    private MonthDetailsParcel parcel;

    public FragmentSummaryInfo() {
    }

    public static FragmentSummaryInfo getInstance(MonthDetailsParcel parcel) {
        FragmentSummaryInfo fragment = new FragmentSummaryInfo();
        Bundle args = new Bundle();
        args.putParcelable(PARCEL_KEY, parcel);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.month_details_tab_summary, container, false);
        unbinder = ButterKnife.bind(this, view);
        if (getArguments() != null) {
            parcel = getArguments().getParcelable(PARCEL_KEY);
        }
        if (parcel != null) {
            setSummaryInfo(parcel);
        }
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void setSummaryInfo(@NonNull MonthDetailsParcel parcel) {
        if (parcel.getIncomingBalance() != null) {
            tvIncomingBalanceValue.setText(String.format(Locale.getDefault(), "%.2f", parcel.getIncomingBalance()));
        }
        if (parcel.getCharged() != null) {
            tvChargedValue.setText(String.format(Locale.getDefault(), "%.2f", parcel.getCharged()));
        }
        if (parcel.getPayments() != null) {
            tvPaymentsValue.setText(String.format(Locale.getDefault(), "%.2f", parcel.getPayments()));
        }
        boolean isTakenForQualityZeroOrNull = parcel.getTakenForQuality() == null || parcel.getTakenForQuality() == 0;
        boolean isRecalculationZeroOrNull = parcel.getRecalculation() == null || parcel.getRecalculation() == 0;
        if (isTakenForQualityZeroOrNull && isRecalculationZeroOrNull) {
            blockAdditionalInfo.setVisibility(View.GONE);
        } else {
            if (!isRecalculationZeroOrNull) {
                tvRecalculationValue.setText(String.format(Locale.getDefault(), "%.2f", parcel.getRecalculation()));
            }
            if (!isTakenForQualityZeroOrNull) {
                tvTakenForQualityValue.setText(String.format(Locale.getDefault(), "%.2f", parcel.getTakenForQuality()));
            }
        }
        if (parcel.getOutgoingBalance() != null) {
            tvOutgoingBalanceValue.setText(String.format(Locale.getDefault(), "%.2f", parcel.getOutgoingBalance()));
        }
    }
}
