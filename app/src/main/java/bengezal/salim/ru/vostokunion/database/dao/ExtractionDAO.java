package bengezal.salim.ru.vostokunion.database.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import bengezal.salim.ru.vostokunion.database.entities.Extraction;
import io.reactivex.Flowable;
import io.reactivex.Maybe;

@Dao
public interface ExtractionDAO {

    @Query("SELECT * FROM extraction")
    Flowable<List<Extraction>> getAll();

    @Query("SELECT * FROM extraction WHERE uid=:id")
    Maybe<Extraction> getById(long id);

    @Insert
    void insert(Extraction extraction);

    @Delete
    void delete(Extraction extraction);

    @Query("DELETE FROM extraction")
    void deleteAll();

}
