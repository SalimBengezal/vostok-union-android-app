package bengezal.salim.ru.vostokunion.managers;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.LinkedHashMap;
import java.util.Map;

public class CookieManager {

    private static final String PREF_KEY_COOKIE = "COOKIE";

    private final Context context;

    public CookieManager(Context context) {
        this.context = context;
    }

    public void saveCookie(Map<String, String> cookies) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
        StringBuilder cookiesString = new StringBuilder();
        for (Map.Entry<String, String> entry : cookies.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            cookiesString.append(key).append("=").append(value).append(" ");
        }
        sp.edit().putString(PREF_KEY_COOKIE, cookiesString.toString().trim()).apply();

    }

    public Map<String, String> getCookie() {
        Map<String, String> cookie = new LinkedHashMap<>();
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
        String cookieString = sp.getString(PREF_KEY_COOKIE, "");
        if (cookieString.trim().isEmpty())
            return cookie;
        String[] keyValues = cookieString.split(" ");
        for (String keyValue : keyValues) {
            int ind = keyValue.indexOf("=");
            String key = keyValue.substring(0, ind);
            String value = keyValue.substring(ind + 1);
            cookie.put(key, value);
        }
        return cookie;
    }

    public void clearCookie() {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
        sp.edit().remove(PREF_KEY_COOKIE).apply();
    }
}
