package bengezal.salim.ru.vostokunion.database;

import java.util.List;

import bengezal.salim.ru.vostokunion.database.entities.Extraction;
import bengezal.salim.ru.vostokunion.database.entities.Receipt;
import io.reactivex.Flowable;
import io.reactivex.Maybe;

public interface LocalRepository {

    Flowable<List<Receipt>> getReceipts();

    Maybe<Receipt> getReceiptById(long id);

    void insertReceipt(Receipt receipt);

    void deleteReceipt(Receipt receipt);

    void deleteAllReceipts();

    Flowable<List<Extraction>> getExtractions();

    Maybe<Extraction> getExtractionById(long id);

    void insertExtraction(Extraction extraction);

    void deleteExtraction(Extraction extraction);

    void deleteAllExtractions();
}
