package bengezal.salim.ru.vostokunion.fragments.paymentHistory.impl;

import android.util.Log;

import java.util.Date;

import javax.inject.Inject;

import bengezal.salim.ru.vostokunion.fragments.paymentHistory.PaymentHistoryModel;
import bengezal.salim.ru.vostokunion.injection.App;
import io.reactivex.Observable;
import ru.bengezal.salim.vostok_union_api.VostokUnionAPI;
import ru.bengezal.salim.vostok_union_api.dto.PaymentHistoryDTO;

public class PaymentHistoryModelImpl implements PaymentHistoryModel {

    private static final String TAG = PaymentHistoryModelImpl.class.getName();

    @Inject
    VostokUnionAPI vostokUnion;

    public PaymentHistoryModelImpl() {
        App.getComponent().inject(this);
    }

    @Override
    public Observable<PaymentHistoryDTO> getPaymentsHistory(Date from, Date to) {
        return Observable.create(e -> {
            Log.i(TAG, "getting payment history");
            e.onNext(vostokUnion.getPaymentHistoryInfo(from, to));
            e.onComplete();
        });
    }

}
