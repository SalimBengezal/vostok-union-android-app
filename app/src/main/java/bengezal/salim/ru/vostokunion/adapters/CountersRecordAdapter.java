package bengezal.salim.ru.vostokunion.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import bengezal.salim.ru.vostokunion.R;
import bengezal.salim.ru.vostokunion.adapters.DAO.Counters.CounterRecord;

public class CountersRecordAdapter extends RecyclerView.Adapter<CountersRecordAdapter.ViewHolder> {

    private final List<CounterRecord> counters = new ArrayList<>();
    private final Context context;

    CountersRecordAdapter(Set<CounterRecord> records, Context context) {
        this.context = context;
        counters.addAll(records);
        Collections.sort(counters, CounterRecord.ALPHABETICAL_ORDER);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_counter_record, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        CounterRecord record = counters.get(position);
        holder.ivIcon.setImageResource(CounterRecord.getImageDrawable(record.counterType));
        holder.tvName.setText(CounterRecord.getCounterName(record.counterType));
        holder.tvCount.setText(String.format("%s", record.meterReading));
        holder.tvConsumption.setText(String.format("%s: %s", context.getString(R.string.consumption), record.consumption));
    }

    @Override
    public int getItemCount() {
        return counters.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final ImageView ivIcon;
        private final TextView tvName;
        private final TextView tvCount;
        private final TextView tvConsumption;

        ViewHolder(View view) {
            super(view);
            ivIcon = view.findViewById(R.id.item_counter_record_icon);
            tvName = view.findViewById(R.id.item_counter_record_name);
            tvCount = view.findViewById(R.id.item_counter_record_count);
            tvConsumption = view.findViewById(R.id.item_counter_record_consumption);

        }
    }

}
