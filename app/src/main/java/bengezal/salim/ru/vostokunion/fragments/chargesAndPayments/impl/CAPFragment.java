package bengezal.salim.ru.vostokunion.fragments.chargesAndPayments.impl;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

import bengezal.salim.ru.vostokunion.R;
import bengezal.salim.ru.vostokunion.activities.monthDetails.MonthDetailsActivity;
import bengezal.salim.ru.vostokunion.adapters.ChargesAndPaymentAdapter;
import bengezal.salim.ru.vostokunion.adapters.DAO.chargesAndPayment.ChargesAndPayments;
import bengezal.salim.ru.vostokunion.components.BlockInformationView;
import bengezal.salim.ru.vostokunion.dialogs.DateFilterDialogFragment;
import bengezal.salim.ru.vostokunion.fragments.chargesAndPayments.CAPPresenter;
import bengezal.salim.ru.vostokunion.fragments.chargesAndPayments.CAPView;
import bengezal.salim.ru.vostokunion.parcels.MonthDetailsParcel;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class CAPFragment extends Fragment implements CAPView, DateFilterDialogFragment.DateFilterListener {

    private static final String FILTER_DIALOG_TAG = "FILTER_DIALOG_TAG";
    private static final String TAG = CAPFragment.class.getName();

    @BindView(R.id.charges_and_payments_progressBar)
    ProgressBar progressBar;
    @BindView(R.id.charges_and_payments_rvRecords)
    RecyclerView chargesAndPaymentsRvRecords;
    @BindView(R.id.charges_and_payments_content)
    LinearLayout content;
    @BindView(R.id.charges_and_payments_info_block)
    BlockInformationView chargesAndPaymentsBlockInfo;
    private Unbinder unbinder;
    private CAPPresenter presenter;
    private Date to;
    private Date from;
    private ChargesAndPaymentAdapter adapter;

    public static CAPFragment getInstance() {
        return new CAPFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        Log.i(TAG, "Creating fragment");
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_charges_and_payments, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_charges_and_payments, container, false);
        unbinder = ButterKnife.bind(this, view);
        presenter = new CAPPresenterImpl(this);

        Calendar calendar = Calendar.getInstance();
        to = calendar.getTime();
        calendar.add(Calendar.MONTH, -6);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        onDateChosen(from = calendar.getTime(), to);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        chargesAndPaymentsRvRecords.setLayoutManager(mLayoutManager);
        adapter = new ChargesAndPaymentAdapter(new ArrayList<>(), getContext(), monthId -> presenter.showMonthDetails(monthId));
        chargesAndPaymentsRvRecords.setAdapter(adapter);
        chargesAndPaymentsRvRecords.setHasFixedSize(true);
        chargesAndPaymentsRvRecords.setNestedScrollingEnabled(false);

        Log.i(TAG, "oncreateview");
        return view;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_filter:
                showDialog();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showInfoBlock(Date from, Date end) {
        DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT);
        String dateString;
        if (from != null && end != null) {
            dateString = String.format("%s %s %s %s", getString(R.string.showing_results_from), df.format(from), getString(R.string.until), df.format(end));
        } else {
            dateString = getString(R.string.no_info_in_this_date_range);
        }
        chargesAndPaymentsBlockInfo.setMessage(dateString);
        chargesAndPaymentsBlockInfo.setIcon(ContextCompat.getDrawable(Objects.requireNonNull(getContext()), R.drawable.ic_info));
    }

    @SuppressLint("CommitTransaction")
    private void showDialog() {
        if (getActivity() != null) {
            FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
            Fragment prev = getActivity().getSupportFragmentManager().findFragmentByTag(FILTER_DIALOG_TAG);
            if (prev != null) {
                ft.remove(prev);
            }
            ft.addToBackStack(null);

            DateFilterDialogFragment dialogFragment = DateFilterDialogFragment.newInstance(from, to);
            dialogFragment.setTargetFragment(this, 0);
            assert getFragmentManager() != null;
            dialogFragment.show(getFragmentManager().beginTransaction(), FILTER_DIALOG_TAG);
        }
    }

    @Override
    public void onDateChosen(Date from, Date to) {
        if (getActivity() != null) {
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            if (fragmentManager.findFragmentById(R.id.charges_and_payments_info_block) != null) {
                fragmentManager.beginTransaction()
                        .remove(Objects.requireNonNull(fragmentManager.findFragmentById(R.id.counters_history_info_block)))
                        .commit();
            }
        }
        this.from = from;
        this.to = to;
        presenter.showChargesAndPayments(from, to);
    }

    @Override
    public void startLoading() {
        Log.i(TAG, "startLoading");
        if (getActivity() != null) {
            progressBar.setVisibility(ProgressBar.VISIBLE);
            content.setVisibility(NestedScrollView.INVISIBLE);
        }
    }

    @Override
    public void stopLoading() {
        Log.i(TAG, "stopLoading");
        if (getActivity() != null) {
            progressBar.setVisibility(ProgressBar.INVISIBLE);
            content.setVisibility(NestedScrollView.VISIBLE);
        }
    }

    @Override
    public void showChargesAndPayments(ChargesAndPayments chargesAndPayments) {
        Log.i(TAG, "showChargesANdPayments");
        if (getActivity() != null) {
            adapter = new ChargesAndPaymentAdapter(chargesAndPayments.getRecords(), getContext(), monthId -> presenter.showMonthDetails(monthId));
            chargesAndPaymentsRvRecords.setAdapter(adapter);
            showInfoBlock(chargesAndPayments.getStartDate(), chargesAndPayments.getEndDate());
        }
        Log.i(TAG, "end showChargesAndPayments");
    }

    @Override
    public void showMonthDetails(MonthDetailsParcel monthDetails) {
        Intent monthDetailsIntent = new Intent(getContext(), MonthDetailsActivity.class);
        monthDetailsIntent.putExtra(MonthDetailsParcel.class.getCanonicalName(), monthDetails);
        startActivity(monthDetailsIntent);
    }

    @Override
    public void notifyNoDataNow() {
        if (getView() != null) {
            Snackbar.make(getView(), "Данные будут доступны в следующем месяце", Snackbar.LENGTH_LONG).show();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }
}
