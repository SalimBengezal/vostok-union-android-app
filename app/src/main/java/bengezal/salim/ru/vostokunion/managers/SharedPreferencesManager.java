package bengezal.salim.ru.vostokunion.managers;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class SharedPreferencesManager {

    private static final String PREF_KEY_ACCOUNT_NUMBER = "ACCOUNT_NUMBER";
    private static final String PREF_KEY_PASSWORD = "PASSWORD";

    private final SharedPreferences sp;

    public SharedPreferencesManager(Context ctx) {
        sp = PreferenceManager.getDefaultSharedPreferences(ctx.getApplicationContext());
    }

    public String getAccountNumber() {
        return sp.getString(PREF_KEY_ACCOUNT_NUMBER, null);
    }

    public void setAccountNumber(String accountNumber) {
        sp.edit().putString(PREF_KEY_ACCOUNT_NUMBER, accountNumber).apply();
    }

    public String getPassword() {
        return sp.getString(PREF_KEY_PASSWORD, null);
    }

    public void setPassword(String password) {
        sp.edit().putString(PREF_KEY_PASSWORD, password).apply();
    }

    public void clearAccountNumberAndPassword() {
        sp.edit().remove(PREF_KEY_ACCOUNT_NUMBER).remove(PREF_KEY_PASSWORD).apply();
    }
}
