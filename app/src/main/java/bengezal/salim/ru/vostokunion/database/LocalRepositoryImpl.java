package bengezal.salim.ru.vostokunion.database;

import java.util.List;
import java.util.concurrent.Executor;

import bengezal.salim.ru.vostokunion.database.dao.ExtractionDAO;
import bengezal.salim.ru.vostokunion.database.dao.ReceiptDAO;
import bengezal.salim.ru.vostokunion.database.entities.Extraction;
import bengezal.salim.ru.vostokunion.database.entities.Receipt;
import io.reactivex.Flowable;
import io.reactivex.Maybe;

public class LocalRepositoryImpl implements LocalRepository {

    private final ReceiptDAO receiptDAO;
    private final ExtractionDAO extractionDAO;
    private final Executor executor;

    public LocalRepositoryImpl(AppDatabase appDatabase, Executor executor) {
        this.receiptDAO = appDatabase.receiptDAO();
        this.extractionDAO = appDatabase.extractionDAO();
        this.executor = executor;
    }

    @Override
    public Flowable<List<Receipt>> getReceipts() {
        return receiptDAO.getAll();
    }

    @Override
    public Maybe<Receipt> getReceiptById(long id) {
        return receiptDAO.getById(id);
    }

    @Override
    public void insertReceipt(Receipt receipt) {
        executor.execute(() -> receiptDAO.insert(receipt));
    }

    @Override
    public void deleteReceipt(Receipt receipt) {
        executor.execute(() -> receiptDAO.delete(receipt));
    }

    @Override
    public void deleteAllReceipts() {
        executor.execute(receiptDAO::deleteAll);
    }

    @Override
    public Flowable<List<Extraction>> getExtractions() {
        return extractionDAO.getAll();
    }

    @Override
    public Maybe<Extraction> getExtractionById(long id) {
        return extractionDAO.getById(id);
    }

    @Override
    public void insertExtraction(Extraction extraction) {
        executor.execute(() -> extractionDAO.insert(extraction));
    }

    @Override
    public void deleteExtraction(Extraction extraction) {
        executor.execute(() -> extractionDAO.delete(extraction));
    }

    @Override
    public void deleteAllExtractions() {
        executor.execute(extractionDAO::deleteAll);
    }

}
