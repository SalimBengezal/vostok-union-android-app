package bengezal.salim.ru.vostokunion.fragments.meterReading.dao;

public class DeleteMeterReading {

    private final String rowIdSch;
    private final String tariff;
    private final String date;
    private final String pokazLS;
    private final String action;

    public DeleteMeterReading(String rowIdSch, String tariff, String date, String pokazLS, String action) {
        this.rowIdSch = rowIdSch;
        this.tariff = tariff;
        this.date = date;
        this.action = action;
        this.pokazLS = pokazLS;
    }

    public String getRowIdSch() {
        return rowIdSch;
    }

    public String getTariff() {
        return tariff;
    }

    public String getDate() {
        return date;
    }

    public String getPokazLS() {
        return pokazLS;
    }

    public String getAction() {
        return action;
    }
}
