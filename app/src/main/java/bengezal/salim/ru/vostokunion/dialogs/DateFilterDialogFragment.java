package bengezal.salim.ru.vostokunion.dialogs;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.AppCompatButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Date;

import bengezal.salim.ru.vostokunion.R;
import bengezal.salim.ru.vostokunion.components.EditTextDate;

public class DateFilterDialogFragment extends DialogFragment {

    private static final String FROM = "FROM_KEY";
    private static final String TO = "TO_KEY";
    private DateFilterListener mListener;

    public static DateFilterDialogFragment newInstance(Date from, Date to) {
        DateFilterDialogFragment f = new DateFilterDialogFragment();
        Bundle args = new Bundle();
        args.putLong(FROM, from.getTime());
        args.putLong(TO, to.getTime());
        f.setArguments(args);
        return f;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_filter, container, false);
        EditTextDate etFrom = view.findViewById(R.id.dialog_filter_etFrom);
        EditTextDate etTo = view.findViewById(R.id.dialog_filter_etTo);
        AppCompatButton btnEdit = view.findViewById(R.id.dialog_filter_button);
        if (getArguments() != null) {
            etFrom.setDate(new Date(getArguments().getLong(FROM)));
            etTo.setDate(new Date(getArguments().getLong(TO)));
        }
        etFrom.setOnDateChangeListener(date -> {
            Date to = etTo.getDate();
            btnEdit.setEnabled(date.compareTo(to) < 0);
        });
        etTo.setOnDateChangeListener(date -> {
            Date from = etFrom.getDate();
            btnEdit.setEnabled(from.compareTo(date) < 0);
        });
        btnEdit.setOnClickListener(v -> {
            Date from = etFrom.getDate();
            Date to = etTo.getDate();
            if (from.compareTo(to) < 0) {
                mListener.onDateChosen(from, to);
                dismiss();
            }
        });
        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mListener = (DateFilterListener) getTargetFragment();
        } catch (Exception e) {
            throw new RuntimeException(context.toString()
                    + " must implement DateFilterListener");
        }
    }

    public interface DateFilterListener {
        void onDateChosen(Date from, Date to);
    }
}
