package bengezal.salim.ru.vostokunion.fragments.documents.impl;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.tbuonomo.viewpagerdotsindicator.SpringDotsIndicator;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import bengezal.salim.ru.vostokunion.R;
import bengezal.salim.ru.vostokunion.adapters.DocumentsAdapter;
import bengezal.salim.ru.vostokunion.adapters.ViewPagerAdapter;
import bengezal.salim.ru.vostokunion.components.MyViewPager;
import bengezal.salim.ru.vostokunion.fragments.documents.DocumentsPresenter;
import bengezal.salim.ru.vostokunion.fragments.documents.DocumentsView;
import bengezal.salim.ru.vostokunion.fragments.documents.FragmentExtraction;
import bengezal.salim.ru.vostokunion.fragments.documents.FragmentReceipt;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class DocumentsFragment extends Fragment implements DocumentsView {

    @BindView(R.id.documents_progressBar)
    ProgressBar progressBar;
    @BindView(R.id.documents_viewPager)
    MyViewPager viewPager;
    @BindView(R.id.documents_tabDots)
    SpringDotsIndicator tabDots;
    @BindView(R.id.documents_tvDownloadedDocuments)
    TextView tvDownloadedDocuments;
    @BindView(R.id.documents_rvDocuments)
    RecyclerView rvDocuments;
    Unbinder unbinder;
    private DocumentsPresenter presenter;

    private final FragmentReceipt.ReceiptListener receiptListener = new FragmentReceipt.ReceiptListener() {
        @Override
        public void onSubmit(Date period) {
            presenter.downloadReceipt(period);
        }
    };
    private final FragmentExtraction.ExtractionListener extractionListener = new FragmentExtraction.ExtractionListener() {
        @Override
        public void onSubmit(Date startPeriod, Date endPeriod) {
            presenter.downloadExtraction(startPeriod, endPeriod);
        }
    };
    private final DocumentsAdapter.ItemClickListener itemDocumentListListener = new DocumentsAdapter.ItemClickListener() {
        @Override
        public void onItemClicked(long id, DocumentsAdapter.Document.TYPE type) {
            switch (type) {
                case RECEIPT:
                    presenter.openStoredReceipt(id);
                    break;
                case EXTRACTION:
                    presenter.openStoredExtraction(id);
                    break;
            }
        }

        @Override
        public void onItemDelete(long id, DocumentsAdapter.Document.TYPE type) {
            switch (type) {
                case RECEIPT:
                    presenter.deleteReceipt(id);
                    break;
                case EXTRACTION:
                    presenter.deleteExtraction(id);
                    break;
            }
        }
    };

    private final ViewPager.OnPageChangeListener pageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        }

        @Override
        public void onPageSelected(int position) {
            switch (position) {
                case 0:
                    new Handler().postDelayed(() -> presenter.loadCachedReceipts(), 300);
                    break;
                case 1:
                    new Handler().postDelayed(() -> presenter.loadCachedExtractions(), 300);
                    break;
            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {
        }
    };

    private FragmentReceipt receiptFragment;
    private FragmentExtraction extractionFragment;
    private DocumentsAdapter documentsAdapter = new DocumentsAdapter(new ArrayList<>(), itemDocumentListListener, getContext());

    public static DocumentsFragment getInstance() {
        return new DocumentsFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_documents, container, false);
        unbinder = ButterKnife.bind(this, view);
        presenter = new DocumentsPresenterImpl(this);

        progressBar.bringToFront();

        receiptFragment = FragmentReceipt.getInstance(receiptListener);
        extractionFragment = FragmentExtraction.getInstance(extractionListener);

        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(Objects.requireNonNull(getActivity()).getSupportFragmentManager());
        viewPagerAdapter.addFragment(receiptFragment, "Квитанции ЖКХ");
        viewPagerAdapter.addFragment(extractionFragment, "Выписки из ЛС");
        viewPager.setAdapter(viewPagerAdapter);
        viewPager.addOnPageChangeListener(pageChangeListener);
        tabDots.setViewPager(viewPager);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        rvDocuments.setLayoutManager(mLayoutManager);
        rvDocuments.setAdapter(documentsAdapter);
        rvDocuments.setHasFixedSize(true);
        rvDocuments.setNestedScrollingEnabled(false);
        presenter.loadCachedReceipts();
        presenter.getMonthsInReceiptList();
        return view;
    }

    @Override
    public void startLoading() {
        receiptFragment.startLoading();
        extractionFragment.startLoading();
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void stopLoading() {
        receiptFragment.stopLoading();
        extractionFragment.stopLoading();
        progressBar.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showMonthsInReceiptList(List<Date> dateList) {
        if (receiptFragment != null) {
            receiptFragment.loadReceiptList(dateList);
        }
    }

    @Override
    public void openReceiptFile(String path) {
        File pdf = new File(path);
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.fromFile(pdf), "application/pdf");
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(intent);
    }

    @Override
    public void fileNotFound(String fileName) {
        String message = getString(R.string.file_not_found) + ": " + fileName;
        Snackbar.make(Objects.requireNonNull(getView()), message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showCachedDocumentsList(List<DocumentsAdapter.Document> documents, String header) {
        documentsAdapter = new DocumentsAdapter(documents, itemDocumentListListener, getContext());
        rvDocuments.setAdapter(documentsAdapter);
        tvDownloadedDocuments.setText(header);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }
}
