package bengezal.salim.ru.vostokunion.injection;

import javax.inject.Singleton;

import bengezal.salim.ru.vostokunion.activities.login.impl.LoginModelImpl;
import bengezal.salim.ru.vostokunion.activities.login.impl.LoginPresenterImpl;
import bengezal.salim.ru.vostokunion.activities.main.impl.MainModelImpl;
import bengezal.salim.ru.vostokunion.activities.main.impl.MainPresenterImpl;
import bengezal.salim.ru.vostokunion.fragments.chargesAndPayments.impl.CAPModelImpl;
import bengezal.salim.ru.vostokunion.fragments.countersHistory.impl.CountersHistoryModelImpl;
import bengezal.salim.ru.vostokunion.fragments.documents.impl.DocumentsModelImpl;
import bengezal.salim.ru.vostokunion.fragments.documents.impl.DocumentsPresenterImpl;
import bengezal.salim.ru.vostokunion.fragments.home.impl.HomeFragmentModelImpl;
import bengezal.salim.ru.vostokunion.fragments.meterReading.impl.MeterReadingModelImpl;
import bengezal.salim.ru.vostokunion.fragments.paymentHistory.impl.PaymentHistoryModelImpl;
import bengezal.salim.ru.vostokunion.injection.modules.AppModule;
import bengezal.salim.ru.vostokunion.injection.modules.NetworkReceiverModule;
import bengezal.salim.ru.vostokunion.injection.modules.UtilsModule;
import dagger.Component;

@Component(modules = {AppModule.class, UtilsModule.class, NetworkReceiverModule.class})
@Singleton
public interface AppComponent {

    void inject(LoginPresenterImpl loginPresenter);

    void inject(MainPresenterImpl mainPresenter);

    void inject(LoginModelImpl loginModel);

    void inject(MainModelImpl mainModel);

    void inject(HomeFragmentModelImpl homeFragmentModel);

    void inject(CountersHistoryModelImpl countersHistoryModel);

    void inject(PaymentHistoryModelImpl paymentHistoryModel);

    void inject(MeterReadingModelImpl meterReadingModel);

    void inject(CAPModelImpl capModel);

    void inject(DocumentsModelImpl receiptsModel);

    void inject(DocumentsPresenterImpl receiptsPresenter);

}
