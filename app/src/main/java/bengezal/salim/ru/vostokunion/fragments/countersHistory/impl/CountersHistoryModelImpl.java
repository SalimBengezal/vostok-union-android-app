package bengezal.salim.ru.vostokunion.fragments.countersHistory.impl;

import android.util.Log;

import java.util.Date;

import javax.inject.Inject;

import bengezal.salim.ru.vostokunion.fragments.countersHistory.CountersHistoryModel;
import bengezal.salim.ru.vostokunion.injection.App;
import io.reactivex.Observable;
import ru.bengezal.salim.vostok_union_api.VostokUnionAPI;
import ru.bengezal.salim.vostok_union_api.dto.CountersHistoryDTO;

public class CountersHistoryModelImpl implements CountersHistoryModel {

    private static final String TAG = CountersHistoryModelImpl.class.getName();

    @Inject
    VostokUnionAPI vostokUnion;

    public CountersHistoryModelImpl() {
        App.getComponent().inject(this);
    }

    @Override
    public Observable<CountersHistoryDTO> getHistoryCounters(Date from, Date to) {
        return Observable.create(e -> {
            Log.i(TAG, "getting counters");
            e.onNext(vostokUnion.getCountersHistoryInfo(from, to));
            e.onComplete();
        });
    }

}
