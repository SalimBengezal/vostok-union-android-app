package bengezal.salim.ru.vostokunion.database.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import bengezal.salim.ru.vostokunion.database.entities.Receipt;
import io.reactivex.Flowable;
import io.reactivex.Maybe;

@Dao
public interface ReceiptDAO {

    @Query("SELECT * FROM receipt")
    Flowable<List<Receipt>> getAll();

    @Query("SELECT * FROM receipt WHERE uid=:id")
    Maybe<Receipt> getById(long id);

    @Insert
    void insert(Receipt receipt);

    @Delete
    void delete(Receipt receipt);

    @Query("DELETE FROM receipt")
    void deleteAll();
}
