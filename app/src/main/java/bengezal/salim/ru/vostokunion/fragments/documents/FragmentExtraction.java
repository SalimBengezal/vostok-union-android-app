package bengezal.salim.ru.vostokunion.fragments.documents;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Calendar;
import java.util.Date;

import bengezal.salim.ru.vostokunion.R;
import bengezal.salim.ru.vostokunion.components.EditTextDate;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class FragmentExtraction extends Fragment {

    @BindView(R.id.extraction_startDate)
    EditTextDate etStartDate;
    @BindView(R.id.extraction_endDate)
    EditTextDate etEndDate;
    Unbinder unbinder;
    @BindView(R.id.extraction_btnSubmit)
    AppCompatButton extractionBtnSubmit;
    private ExtractionListener listener;

    public FragmentExtraction() {
    }

    public static FragmentExtraction getInstance(ExtractionListener listener) {
        FragmentExtraction fragment = new FragmentExtraction();
        fragment.listener = listener;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.documents_tab_extraction, container, false);
        unbinder = ButterKnife.bind(this, view);
        Date now = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.YEAR, -1);
        etStartDate.setDate(calendar.getTime());
        etEndDate.setDate(now);
        etStartDate.setOnDateChangeListener(date -> {
            Date end = etEndDate.getDate();
            extractionBtnSubmit.setEnabled(date.compareTo(end) < 0);
        });
        etEndDate.setOnDateChangeListener(date -> {
            Date start = etStartDate.getDate();
            extractionBtnSubmit.setEnabled(start.compareTo(date) < 0);
        });
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.extraction_btnSubmit)
    public void onViewClicked() {
        Date from = etStartDate.getDate();
        Date to = etEndDate.getDate();
        if (from.compareTo(to) < 0)
            listener.onSubmit(from, to);
    }

    public void startLoading() {
        if (extractionBtnSubmit != null) {
            extractionBtnSubmit.setEnabled(false);
            extractionBtnSubmit.setText(R.string.downloading);
        }
    }

    public void stopLoading() {
        if (extractionBtnSubmit != null) {
            extractionBtnSubmit.setEnabled(true);
            extractionBtnSubmit.setText(R.string.generate);
        }
    }

    public interface ExtractionListener {
        void onSubmit(Date startPeriod, Date endPeriod);
    }
}
