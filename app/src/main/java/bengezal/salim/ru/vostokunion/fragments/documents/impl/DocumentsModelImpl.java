package bengezal.salim.ru.vostokunion.fragments.documents.impl;

import android.util.Log;

import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import bengezal.salim.ru.vostokunion.database.LocalRepository;
import bengezal.salim.ru.vostokunion.database.entities.Extraction;
import bengezal.salim.ru.vostokunion.database.entities.Receipt;
import bengezal.salim.ru.vostokunion.fragments.documents.DocumentsModel;
import bengezal.salim.ru.vostokunion.injection.App;
import io.reactivex.Flowable;
import io.reactivex.Maybe;
import io.reactivex.Observable;
import ru.bengezal.salim.vostok_union_api.VostokUnionAPI;

public class DocumentsModelImpl implements DocumentsModel {

    private static final String TAG = DocumentsModelImpl.class.getName();

    @Inject
    VostokUnionAPI vostokUnion;

    @Inject
    LocalRepository dbRepository;

    DocumentsModelImpl() {
        App.getComponent().inject(this);
    }

    @Override
    public Observable<List<Date>> getReceiptList() {
        return Observable.create(e -> {
            Log.i(TAG, "getting receipts date list");
            e.onNext(vostokUnion.getReceiptList());
            e.onComplete();
        });
    }

    @Override
    public Observable<byte[]> downloadReceipt(Date period) {
        return Observable.fromCallable(() -> vostokUnion.generateAndDownloadReceiptFile(period));
    }

    @Override
    public Observable<byte[]> downloadExtraction(Date startPeriod, Date endPeriod) {
        return Observable.fromCallable(() -> vostokUnion.generateAndDownloadExtractionFile(startPeriod, endPeriod));
    }

    @Override
    public Flowable<List<Receipt>> getCachedReceipts() {
        return dbRepository.getReceipts();
    }

    @Override
    public Flowable<List<Extraction>> getCachedExtractions() {
        return dbRepository.getExtractions();
    }

    @Override
    public Maybe<Receipt> getReceiptById(long id) {
        return dbRepository.getReceiptById(id);
    }

    @Override
    public Maybe<Extraction> getExtractionById(long id) {
        return dbRepository.getExtractionById(id);
    }

    @Override
    public void saveReceiptToDB(Receipt receipt) {
        dbRepository.insertReceipt(receipt);
    }

    @Override
    public void saveExtractionToDB(Extraction extraction) {
        dbRepository.insertExtraction(extraction);
    }

    @Override
    public void deleteReceiptFromDB(Receipt receipt) {
        dbRepository.deleteReceipt(receipt);
    }

    @Override
    public void deleteExtractionFromDB(Extraction extraction) {
        dbRepository.deleteExtraction(extraction);
    }


}
