package bengezal.salim.ru.vostokunion.activities.monthDetails;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.tbuonomo.viewpagerdotsindicator.SpringDotsIndicator;

import bengezal.salim.ru.vostokunion.R;
import bengezal.salim.ru.vostokunion.adapters.MonthDetailsAdapter;
import bengezal.salim.ru.vostokunion.adapters.ViewPagerAdapter;
import bengezal.salim.ru.vostokunion.parcels.MonthDetailsParcel;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MonthDetailsActivity extends AppCompatActivity {


    @BindView(R.id.month_details_list)
    RecyclerView lvRecords;
    @BindView(R.id.month_details_viewPager)
    ViewPager viewPager;
    @BindView(R.id.month_details_tabDots)
    SpringDotsIndicator tabDots;
    @BindView(R.id.month_details_toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_month_details);
        ButterKnife.bind(this);
        MonthDetailsParcel parcel = getIntent().getParcelableExtra(MonthDetailsParcel.class.getCanonicalName());
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        lvRecords.setLayoutManager(mLayoutManager);
        RecyclerView.Adapter adapter = new MonthDetailsAdapter(parcel.getRecords());
        lvRecords.setAdapter(adapter);
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        FragmentSummaryInfo fragmentSummary = FragmentSummaryInfo.getInstance(parcel);
        FragmentPieChart fragmentPieChart = FragmentPieChart.getInstance(parcel);
        if (parcel.getMonthName() != null) {
            toolbar.setTitle(parcel.getMonthName());
        }
        viewPagerAdapter.addFragment(fragmentSummary, "Итоговая информация");
        viewPagerAdapter.addFragment(fragmentPieChart, "Круговая диаграмма");
        tabDots.setViewPager(viewPager);
        viewPager.setAdapter(viewPagerAdapter);
    }


}
