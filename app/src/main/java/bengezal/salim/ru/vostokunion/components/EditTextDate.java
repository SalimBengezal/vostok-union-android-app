package bengezal.salim.ru.vostokunion.components;

import android.app.DatePickerDialog;
import android.content.Context;
import android.text.InputType;
import android.util.AttributeSet;
import android.widget.DatePicker;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class EditTextDate extends android.support.v7.widget.AppCompatEditText {

    private static final DateFormat df = new SimpleDateFormat("d MMM yyyy", new Locale("ru"));
    private static final Calendar calendar = Calendar.getInstance();
    private Date date;
    private DateChangeListener dateChangeListener;
    private final DatePickerDialog.OnDateSetListener mListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker datePicker, int year, int month, int day) {
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, month);
            calendar.set(Calendar.DAY_OF_MONTH, day);
            date = calendar.getTime();
            setText(df.format(calendar.getTime()));
            if (dateChangeListener != null) {
                dateChangeListener.onDateChosen(date);
            }
        }
    };

    public EditTextDate(Context context) {
        super(context);
        setInputType(InputType.TYPE_NULL);
        setClickable(true);
        setFocusableInTouchMode(false);
    }

    public EditTextDate(Context context, AttributeSet attrs) {
        super(context, attrs);
        setInputType(InputType.TYPE_NULL);
        setClickable(true);
        setFocusableInTouchMode(false);
    }

    public EditTextDate(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setInputType(InputType.TYPE_NULL);
        setClickable(true);
        setFocusableInTouchMode(false);
    }

    public void setOnDateChangeListener(DateChangeListener dateChangeListener) {
        this.dateChangeListener = dateChangeListener;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        setText(df.format(date));
        this.date = date;
        setOnClickListener(v -> {
            calendar.setTime(date);
            int fromYear = calendar.get(Calendar.YEAR);
            int fromMonth = calendar.get(Calendar.MONTH);
            int fromDay = calendar.get(Calendar.DAY_OF_MONTH);
            assert getContext() != null;
            new DatePickerDialog(getContext(), mListener, fromYear, fromMonth, fromDay).show();
        });
    }

    public interface DateChangeListener {
        void onDateChosen(Date date);
    }

}