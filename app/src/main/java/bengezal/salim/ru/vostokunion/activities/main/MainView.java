package bengezal.salim.ru.vostokunion.activities.main;

import bengezal.salim.ru.vostokunion.parcels.ApartmentDetailsParcel;

public interface MainView {

    void showPersonalInfoInNavigationDrawer(String address, String fio);

    void startApartmentDetailsActivity(ApartmentDetailsParcel parcel);

    void startLoginActivity();

    void startLoading();

    void stopLoading();

}
