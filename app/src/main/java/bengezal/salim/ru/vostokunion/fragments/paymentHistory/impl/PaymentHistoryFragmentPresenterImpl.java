package bengezal.salim.ru.vostokunion.fragments.paymentHistory.impl;

import java.util.Date;

import bengezal.salim.ru.vostokunion.adapters.DAO.PaymentHistory.PaymentHistory;
import bengezal.salim.ru.vostokunion.fragments.paymentHistory.PaymentHistoryModel;
import bengezal.salim.ru.vostokunion.fragments.paymentHistory.PaymentHistoryPresenter;
import bengezal.salim.ru.vostokunion.fragments.paymentHistory.PaymentHistoryView;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import ru.bengezal.salim.vostok_union_api.dto.PaymentHistoryDTO;

public class PaymentHistoryFragmentPresenterImpl implements PaymentHistoryPresenter {

    private final PaymentHistoryView view;
    private final PaymentHistoryModel model;

    private final CompositeDisposable disposables = new CompositeDisposable();

    PaymentHistoryFragmentPresenterImpl(PaymentHistoryView view) {
        this.view = view;
        this.model = new PaymentHistoryModelImpl();
    }

    @Override
    public void showPaymentHistory(Date from, Date to) {
        view.startLoading();
        Disposable disposable = model.getPaymentsHistory(from, to)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(this::convert)
                .subscribe(next -> {
                    view.showPaymentHistory(next);
                    view.stopLoading();
                });
        disposables.add(disposable);
    }

    private PaymentHistory convert(PaymentHistoryDTO paymentHistoryDTO) {
        PaymentHistory paymentHistory = new PaymentHistory();
        paymentHistory.setFrom(paymentHistoryDTO.getFrom());
        paymentHistory.setTo(paymentHistoryDTO.getTo());
        for (PaymentHistoryDTO.Record rec : paymentHistoryDTO.getRecords()) {
            paymentHistory.addRecord(rec.getDate(), rec.getSum(), rec.getSource());
        }
        return paymentHistory;
    }

    @Override
    public void onDestroy() {
        disposables.dispose();
    }
}
