package bengezal.salim.ru.vostokunion.components;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import bengezal.salim.ru.vostokunion.R;

public class BlockInformationView extends RelativeLayout {

    private TextView tvMessage;
    private ImageView ivIcon;

    public BlockInformationView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.block_information, this);
        this.ivIcon = findViewById(R.id.block_information_ivIcon);
        this.tvMessage = findViewById(R.id.block_information_tvMessage);
    }

    public String getMessage() {
        return String.valueOf(tvMessage.getText());
    }

    public void setMessage(String text) {
        tvMessage.setText(text);
    }

    public void setIcon(Drawable drawable) {
        ivIcon.setImageDrawable(drawable);
    }


}
