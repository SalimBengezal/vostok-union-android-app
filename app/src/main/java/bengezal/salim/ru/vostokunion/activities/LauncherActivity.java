package bengezal.salim.ru.vostokunion.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import bengezal.salim.ru.vostokunion.activities.login.impl.LoginActivity;
import bengezal.salim.ru.vostokunion.activities.main.impl.MainActivity;

public class LauncherActivity extends AppCompatActivity {

    private static final int AUTHORIZATION_REQUEST = 1;
    private static final int LOGOUT_REQUEST = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent activityIntent = new Intent(this, LoginActivity.class);
        startActivityForResult(activityIntent, AUTHORIZATION_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AUTHORIZATION_REQUEST) {
            if (resultCode == RESULT_OK) {
                Intent activityIntent = new Intent(this, MainActivity.class);
                startActivityForResult(activityIntent, LOGOUT_REQUEST);
            } else {
                finish();
            }
        } else if (requestCode == LOGOUT_REQUEST) {
            if (resultCode == RESULT_OK) {
                Intent activityIntent = new Intent(this, LoginActivity.class);
                startActivityForResult(activityIntent, AUTHORIZATION_REQUEST);
            } else {
                finish();
            }
        }
    }
}
