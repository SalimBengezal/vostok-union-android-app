package bengezal.salim.ru.vostokunion.fragments.meterReading.dao;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class MeterReadings {

    public static final Comparator<Record> DESCENDING_ACCEPTED_VALUE = (first, second) -> {
        if (first.getDeleteLink() != null && second.getDeleteLink() != null) {
            return first.compareTo(second);
        } else if (first.getDeleteLink() != null && second.getDeleteLink() == null) {
            return 1;
        } else if (first.getDeleteLink() == null && second.getDeleteLink() != null) {
            return -1;
        } else {
            return first.compareTo(second);
        }
    };


    private final List<Record> readings;
    private final String action;
    private final String ls;
    private final Integer startDate;
    private final Integer endDate;

    public MeterReadings(String action, String ls, Integer startDate, Integer endDate) {
        this.action = action;
        this.ls = ls;
        this.startDate = startDate;
        this.endDate = endDate;
        readings = new ArrayList<>();
    }

    public Integer getStartDate() {
        return startDate;
    }

    public Integer getEndDate() {
        return endDate;
    }

    public String getAction() {
        return action;
    }

    public String getLs() {
        return ls;
    }

    public void addReading(String name, String tariff, String counterNumber, Date lastReadingDate, Double lastReading, Double newReading, String newReadingInputName, String newReadingInputHiddenName, String newReadingInputHiddenVal, Boolean enabledEditing, String delRowIdSch, String delTariff, String delDate, String delPokazLS, String delAction) {
        Record rec = new Record(name, tariff, counterNumber, lastReadingDate, lastReading, newReading, newReadingInputName, newReadingInputHiddenName, newReadingInputHiddenVal, enabledEditing);
        rec.setDeleteLink(delRowIdSch, delTariff, delDate, delPokazLS, delAction);
        readings.add(rec);
    }

    public void addReading(String name, String tariff, String counterNumber, Date lastReadingDate, Double lastReading, Double newReading, String newReadingInputName, String newReadingInputHiddenName, String newReadingInputHiddenVal, Boolean enabledEditing) {
        Record rec = new Record(name, tariff, counterNumber, lastReadingDate, lastReading, newReading, newReadingInputName, newReadingInputHiddenName, newReadingInputHiddenVal, enabledEditing);
        readings.add(rec);
    }

    public List<Record> getReadings() {
        return readings;
    }

    public class Record implements Comparable<Record> {

        private final String name;
        private final String tariff;
        private final String counterNumber;
        private final Date lastReadingDate;
        private final Double lastReading;
        private final String newReadingInputName;
        private final String newReadingInputHiddenName;
        private final String newReadingInputHiddenVal;
        private final Boolean enabledEditState;
        private Double newReading;
        private DeleteLink deleteLink;

        Record(String name, String tariff, String counterNumber, Date lastReadingDate, Double lastReading, Double newReading, String newReadingInputName, String newReadingInputHiddenName, String newReadingInputHiddenVal, Boolean enabledEditState) {
            this.name = name;
            this.tariff = tariff;
            this.counterNumber = counterNumber;
            this.lastReadingDate = lastReadingDate;
            this.lastReading = lastReading;
            this.newReading = newReading;
            this.newReadingInputName = newReadingInputName;
            this.newReadingInputHiddenName = newReadingInputHiddenName;
            this.newReadingInputHiddenVal = newReadingInputHiddenVal;
            this.enabledEditState = enabledEditState;
            this.deleteLink = null;
        }

        public Boolean getEnabledEditState() {
            return enabledEditState;
        }

        public String getName() {
            return name;
        }

        public String getTariff() {
            return tariff;
        }

        public String getCounterNumber() {
            return counterNumber;
        }

        public Date getLastReadingDate() {
            return lastReadingDate;
        }

        public Double getLastReading() {
            return lastReading;
        }

        public Double getNewReading() {
            return newReading;
        }

        public void setNewReading(Double newReading) {
            this.newReading = newReading;
        }

        public String getNewReadingInputName() {
            return newReadingInputName;
        }

        public String getNewReadingInputHiddenName() {
            return newReadingInputHiddenName;
        }

        public String getNewReadingInputHiddenVal() {
            return newReadingInputHiddenVal;
        }

        @Override
        public int compareTo(@NonNull Record record) {
            String r1 = name;
            String r2 = record.name;
            if (r1 != null && r2 != null) {
                int firstMatch = r1.compareToIgnoreCase(r2);
                if (firstMatch == 0) {
                    r1 = tariff;
                    r2 = record.tariff;
                    if (r1 != null && r2 != null) {
                        return r1.compareToIgnoreCase(r2);
                    }
                } else {
                    return firstMatch;
                }
            }
            return 0;
        }

        private void setDeleteLink(String rowIdSch, String tariff, String date, String pokazLs, String action) {
            this.deleteLink = new DeleteLink(rowIdSch, tariff, date, pokazLs, action);
        }

        public DeleteLink getDeleteLink() {
            return deleteLink;
        }

        public class DeleteLink {
            private final String rowIdSch;
            private final String tariff;
            private final String date;
            private final String pokazLs;
            private final String action;


            DeleteLink(String rowIdSch, String tariff, String date, String pokazLs, String action) {
                this.rowIdSch = rowIdSch;
                this.tariff = tariff;
                this.date = date;
                this.pokazLs = pokazLs;
                this.action = action;
            }

            public String getRowIdSch() {
                return rowIdSch;
            }

            public String getTariff() {
                return tariff;
            }

            public String getDate() {
                return date;
            }

            public String getPokazLs() {
                return pokazLs;
            }

            public String getAction() {
                return action;
            }
        }

    }

}
