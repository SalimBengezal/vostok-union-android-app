package bengezal.salim.ru.vostokunion.activities.main.impl;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;
import com.mikepenz.materialize.color.Material;

import bengezal.salim.ru.vostokunion.R;
import bengezal.salim.ru.vostokunion.activities.ApartmentDetailsActivity;
import bengezal.salim.ru.vostokunion.activities.main.MainPresenter;
import bengezal.salim.ru.vostokunion.activities.main.MainView;
import bengezal.salim.ru.vostokunion.fragments.chargesAndPayments.impl.CAPFragment;
import bengezal.salim.ru.vostokunion.fragments.countersHistory.impl.CountersHistoryFragment;
import bengezal.salim.ru.vostokunion.fragments.documents.impl.DocumentsFragment;
import bengezal.salim.ru.vostokunion.fragments.home.impl.HomeFragment;
import bengezal.salim.ru.vostokunion.fragments.meterReading.impl.MeterReadingFragment;
import bengezal.salim.ru.vostokunion.fragments.paymentHistory.impl.PaymentHistoryFragment;
import bengezal.salim.ru.vostokunion.parcels.ApartmentDetailsParcel;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements HomeFragment.OnFragmentInteractionListener, MainView {

    private static final String TAG = MainActivity.class.getName();

    private static final int DRAWER_FRAGMENT_HOME_ID = 1;
    private static final int DRAWER_FRAGMENT_HISTORY_COUNTERS_ID = 2;
    private static final int DRAWER_FRAGMENT_SETTINGS_ID = 3;
    private static final int DRAWER_FRAGMENT_PAYMENT_HISTORY_ID = 4;
    private static final int DRAWER_FRAGMENT_METER_READINGS_ID = 5;
    private static final int DRAWER_FRAGMENT_CHARGES_AND_PAYMENT_ID = 6;
    private static final int DRAWER_FRAGMENT_RECEIPTS = 7;

    @BindView(R.id.main_container)
    RelativeLayout container;
    @BindView(R.id.main_toolbar)
    Toolbar toolbar;
    @BindView(R.id.main_progressBar)
    ProgressBar progressBar;
    private Drawer drawer;
    private AccountHeader accountHeader;
    private MainPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        initializeDrawer();
        presenter = new MainPresenterImpl(this);
        presenter.showAccountNameAndAddressInNavigationDrawer();
        drawer.setSelection(DRAWER_FRAGMENT_HOME_ID);
    }

    private void initializeDrawer() {
        drawer = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(toolbar)
                .withActionBarDrawerToggle(true)
                .withActionBarDrawerToggleAnimated(true)
                .withAccountHeader(accountHeader = getAccountHeader())
                .withCloseOnClick(true)
                .addDrawerItems(
                        new PrimaryDrawerItem()
                                .withName(R.string.drawer_item_home)
                                .withIcon(FontAwesome.Icon.faw_home)
                                .withIdentifier(DRAWER_FRAGMENT_HOME_ID),
                        new PrimaryDrawerItem()
                                .withName(R.string.drawer_history_counters)
                                .withIcon(FontAwesome.Icon.faw_history)
                                .withIdentifier(DRAWER_FRAGMENT_HISTORY_COUNTERS_ID),
                        new PrimaryDrawerItem()
                                .withName(R.string.payment_history)
                                .withIcon(FontAwesome.Icon.faw_money)
                                .withIdentifier(DRAWER_FRAGMENT_PAYMENT_HISTORY_ID),
                        new PrimaryDrawerItem()
                                .withName(R.string.entering_meter_reading)
                                .withIcon(FontAwesome.Icon.faw_pencil)
                                .withIdentifier(DRAWER_FRAGMENT_METER_READINGS_ID),
                        new PrimaryDrawerItem()
                                .withName(R.string.charges_and_payments)
                                .withIcon(FontAwesome.Icon.faw_file_text_o)
                                .withIdentifier(DRAWER_FRAGMENT_CHARGES_AND_PAYMENT_ID),
                        new PrimaryDrawerItem()
                                .withName(R.string.receipts)
                                .withIcon(FontAwesome.Icon.faw_rub)
                                .withIdentifier(DRAWER_FRAGMENT_RECEIPTS),
                        new DividerDrawerItem(),
                        new SecondaryDrawerItem()
                                .withName(R.string.drawer_settings)
                                .withIcon(FontAwesome.Icon.faw_cog)
                                .withIdentifier(DRAWER_FRAGMENT_SETTINGS_ID)
                )
                .withOnDrawerListener(new Drawer.OnDrawerListener() {
                    @Override
                    public void onDrawerOpened(View drawerView) {
                        InputMethodManager inputMethodManager = (InputMethodManager) MainActivity.this.getSystemService(Activity.INPUT_METHOD_SERVICE);
                        if (MainActivity.this.getCurrentFocus() != null) {
                            assert inputMethodManager != null;
                            inputMethodManager.hideSoftInputFromWindow(MainActivity.this.getCurrentFocus().getWindowToken(), 0);
                        }
                    }

                    @Override
                    public void onDrawerClosed(View drawerView) {
                    }

                    @Override
                    public void onDrawerSlide(View drawerView, float slideOffset) {
                    }
                })
                .withOnDrawerItemClickListener((view, position, drawerItem) -> {
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    Fragment fragment = null;
                    switch ((int) drawerItem.getIdentifier()) {
                        case DRAWER_FRAGMENT_HOME_ID:
                            Log.i(TAG, "Showing FRAGMENT_HOME");
                            toolbar.setTitle(R.string.my_profile);
                            fragment = HomeFragment.getInstance();
                            break;
                        case DRAWER_FRAGMENT_HISTORY_COUNTERS_ID:
                            Log.i(TAG, "Showing FRAGMENT_COUNTERS_HISTORY");
                            toolbar.setTitle(R.string.history_of_counters);
                            fragment = CountersHistoryFragment.getInstance();
                            break;
                        case DRAWER_FRAGMENT_PAYMENT_HISTORY_ID:
                            Log.i(TAG, "Showing FRAGMENT_PAYMENT_HISTORY");
                            toolbar.setTitle(getString(R.string.payment_history));
                            fragment = PaymentHistoryFragment.getInstance();
                            break;
                        case DRAWER_FRAGMENT_METER_READINGS_ID:
                            Log.i(TAG, "Showing FRAGMENT_METER_READINGS");
                            toolbar.setTitle(getString(R.string.entering_meter_reading));
                            fragment = MeterReadingFragment.getInstance();
                            break;
                        case DRAWER_FRAGMENT_CHARGES_AND_PAYMENT_ID:
                            Log.i(TAG, "Showing FRAGMENT_CHARGES_AND_PAYMENT_ID");
                            toolbar.setTitle(R.string.charges_and_payments);
                            fragment = CAPFragment.getInstance();
                            break;
                        case DRAWER_FRAGMENT_RECEIPTS:
                            Log.i(TAG, "Showing FRAGMENT_RECEIPTS");
                            toolbar.setTitle(R.string.receipts);
                            fragment = DocumentsFragment.getInstance();
                    }
                    if (fragment != null) {
                        fragmentManager.beginTransaction().replace(R.id.main_container, fragment).commit();
                    }
                    return false;
                })
                .build();
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen())
            drawer.closeDrawer();
        else
            super.onBackPressed();
    }

    @Override
    public void onLogoutRequest() {
        presenter.logOut();
    }

    @Override
    public void onStartApartmentDetails() {
        presenter.startActivityForApartmentDetails();
    }

    @Override
    public void onPayBill() {
        //TODO
    }

    @Override
    public void onAddNewReadings() {
        drawer.setSelection(DRAWER_FRAGMENT_METER_READINGS_ID);
    }

    @Override
    public void showPersonalInfoInNavigationDrawer(String address, String fio) {
        IProfile profile = accountHeader.getActiveProfile();
        profile.withName(fio);
        profile.withEmail(address);
        accountHeader.updateProfile(profile);
    }

    private AccountHeader getAccountHeader() {
        return new AccountHeaderBuilder()
                .withActivity(this)
                .withCompactStyle(true)
                .withAccountHeader(R.layout.drawer_custom_header)
                .withProfileImagesVisible(false)
                .withHeaderBackground(new ColorDrawable(Material.Green._300.getAsColor()))
                .addProfiles(new ProfileDrawerItem().withIdentifier(1))
                .withOnAccountHeaderListener((view, profile, current) -> false)
                .withSelectionListEnabledForSingleProfile(false)
                .withOnAccountHeaderSelectionViewClickListener((view, profile) -> {
                    presenter.startActivityForApartmentDetails();
                    return false;
                })
                .build();
    }

    @Override
    public void startApartmentDetailsActivity(ApartmentDetailsParcel parcel) {
        Intent detailsIntent = new Intent(this, ApartmentDetailsActivity.class);
        detailsIntent.putExtra(ApartmentDetailsParcel.class.getCanonicalName(), parcel);
        startActivity(detailsIntent);
    }

    @Override
    public void startLoginActivity() {
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void startLoading() {
        progressBar.setVisibility(ProgressBar.VISIBLE);
        container.setVisibility(RelativeLayout.INVISIBLE);
    }

    @Override
    public void stopLoading() {
        progressBar.setVisibility(ProgressBar.INVISIBLE);
        container.setVisibility(RelativeLayout.VISIBLE);
    }

    @Override
    protected void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }
}
