package bengezal.salim.ru.vostokunion.activities.main;

public interface MainPresenter {

    void showAccountNameAndAddressInNavigationDrawer();

    void startActivityForApartmentDetails();

    void logOut();

    void onDestroy();

}
