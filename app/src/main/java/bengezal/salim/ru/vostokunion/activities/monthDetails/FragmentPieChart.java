package bengezal.salim.ru.vostokunion.activities.monthDetails;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.mikepenz.materialize.color.Material;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import bengezal.salim.ru.vostokunion.R;
import bengezal.salim.ru.vostokunion.parcels.MonthDetailsParcel;
import bengezal.salim.ru.vostokunion.parcels.MonthDetailsRecordParcel;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class FragmentPieChart extends Fragment {

    private static final String PARCEL_KEY = "piechart";
    private static final Integer[] COLORS = new Integer[]{
            Material.Yellow._300.getAsColor(),
            Material.Green._300.getAsColor(),
            Material.Teal._300.getAsColor(),
            Material.Red._300.getAsColor(),
            Material.BlueGrey._300.getAsColor(),
            Material.DeepOrange._300.getAsColor(),
            Material.LightGreen._300.getAsColor(),
            Material.Brown._300.getAsColor(),
            Material.LightBlue._300.getAsColor()
    };
    @BindView(R.id.month_details_pieChart)
    PieChart pieChart;
    Unbinder unbinder;
    private MonthDetailsParcel parcel;

    public FragmentPieChart() {
    }

    public static FragmentPieChart getInstance(MonthDetailsParcel records) {
        FragmentPieChart fragment = new FragmentPieChart();
        Bundle args = new Bundle();
        args.putParcelable(PARCEL_KEY, records);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.month_details_tab_pie_chart, container, false);
        unbinder = ButterKnife.bind(this, view);
        if (getArguments() != null) {
            parcel = getArguments().getParcelable(PARCEL_KEY);
        }
        if (parcel != null) {
            customizePieChart();
            setPieChart(parcel);
        }
        return view;
    }

    private void customizePieChart() {
        pieChart.setUsePercentValues(true);
        pieChart.getDescription().setEnabled(false);
        //pieChart.setExtraOffsets(5, 10, 5, 5);
        pieChart.setDragDecelerationFrictionCoef(0.95f);
        pieChart.setDrawHoleEnabled(true);
        pieChart.setHoleColor(Color.TRANSPARENT);
        pieChart.setHoleRadius(58f);
        pieChart.setTransparentCircleRadius(61f);
        pieChart.setDrawCenterText(false);
        pieChart.setRotationEnabled(false);
        pieChart.setHighlightPerTapEnabled(true);
        pieChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {

            }

            @Override
            public void onNothingSelected() {

            }
        });

        pieChart.getLegend().setEnabled(false);

        pieChart.setDrawEntryLabels(false);
        //pieChart.setEntryLabelColor(Color.WHITE);
        //pieChart.setEntryLabelTextSize(12f);
    }

    private void setPieChart(MonthDetailsParcel details) {
        //TODO:DO
        List<PieEntry> entries = new ArrayList<>();
        for (MonthDetailsRecordParcel record : details.getRecords()) {
            Double val = record.getCharged();
            if (val != null && val > 0) {
                entries.add(new PieEntry(val.floatValue(), record.getName()));
            }
        }
        PieDataSet dataSet = new PieDataSet(entries, "Процентное потребление");
        dataSet.setDrawIcons(false);
        dataSet.setColors(Arrays.asList(COLORS));
        dataSet.setSliceSpace(2f);
        //dataSet.setIconsOffset(new MPPointF(0, 40));
        dataSet.setSelectionShift(5f);

        //TODO:set color by list

        PieData data = new PieData(dataSet);
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(11f);
        data.setValueTextColor(Color.WHITE);
        data.setDrawValues(true);
        data.setValueTextColor(Color.WHITE);
        pieChart.setData(data);
        pieChart.highlightValues(null);
        pieChart.invalidate();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

}
