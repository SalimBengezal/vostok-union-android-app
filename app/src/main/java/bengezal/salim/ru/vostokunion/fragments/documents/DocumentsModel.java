package bengezal.salim.ru.vostokunion.fragments.documents;

import java.util.Date;
import java.util.List;

import bengezal.salim.ru.vostokunion.database.entities.Extraction;
import bengezal.salim.ru.vostokunion.database.entities.Receipt;
import io.reactivex.Flowable;
import io.reactivex.Maybe;
import io.reactivex.Observable;

public interface DocumentsModel {

    Observable<List<Date>> getReceiptList();

    Observable<byte[]> downloadReceipt(Date period);

    Observable<byte[]> downloadExtraction(Date startPeriod, Date endPeriod);

    Flowable<List<Receipt>> getCachedReceipts();

    Flowable<List<Extraction>> getCachedExtractions();

    Maybe<Receipt> getReceiptById(long id);

    Maybe<Extraction> getExtractionById(long id);

    void saveReceiptToDB(Receipt receipt);

    void saveExtractionToDB(Extraction extraction);

    void deleteReceiptFromDB(Receipt receipt);

    void deleteExtractionFromDB(Extraction extraction);

}
