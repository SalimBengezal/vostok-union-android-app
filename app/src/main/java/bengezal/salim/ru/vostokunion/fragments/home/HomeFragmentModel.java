package bengezal.salim.ru.vostokunion.fragments.home;


import java.util.Date;

import io.reactivex.Observable;
import ru.bengezal.salim.vostok_union_api.dto.ChargesAndPaymentsDTO;
import ru.bengezal.salim.vostok_union_api.dto.MainInfoDTO;
import ru.bengezal.salim.vostok_union_api.dto.MeterReadingsDTO;

public interface HomeFragmentModel {

    Observable<MainInfoDTO> getMainInfo();

    Observable<MeterReadingsDTO> getCountersInfo();

    Observable<ChargesAndPaymentsDTO> getCharges(Date from, Date to);
}
