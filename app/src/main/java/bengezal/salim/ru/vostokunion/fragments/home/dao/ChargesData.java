package bengezal.salim.ru.vostokunion.fragments.home.dao;

public class ChargesData {

    private final String month;
    private final Double value;

    public ChargesData(String month, Double value) {
        this.month = month;
        this.value = value;
    }

    public String getMonth() {
        return month;
    }

    public Double getValue() {
        return value;
    }
}
