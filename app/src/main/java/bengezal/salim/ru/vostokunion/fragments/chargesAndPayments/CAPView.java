package bengezal.salim.ru.vostokunion.fragments.chargesAndPayments;

import bengezal.salim.ru.vostokunion.adapters.DAO.chargesAndPayment.ChargesAndPayments;
import bengezal.salim.ru.vostokunion.parcels.MonthDetailsParcel;

public interface CAPView {

    void startLoading();

    void stopLoading();

    void showChargesAndPayments(ChargesAndPayments chargesAndPayments);

    void showMonthDetails(MonthDetailsParcel monthDetails);

    void notifyNoDataNow();
}
