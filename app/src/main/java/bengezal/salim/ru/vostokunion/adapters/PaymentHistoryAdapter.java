package bengezal.salim.ru.vostokunion.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import bengezal.salim.ru.vostokunion.R;
import bengezal.salim.ru.vostokunion.adapters.DAO.PaymentHistory.PaymentHistoryRecord;

public class PaymentHistoryAdapter extends RecyclerView.Adapter<PaymentHistoryAdapter.ViewHolder> {

    private final List<PaymentHistoryRecord> records;
    private final Context context;

    public PaymentHistoryAdapter(List<PaymentHistoryRecord> list, Context context) {
        this.records = list;
        this.context = context;
        Collections.sort(records, PaymentHistoryRecord.DATE_DESC);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_payment_history, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        PaymentHistoryRecord record = records.get(position);
        holder.tvSum.setText(String.format(Locale.getDefault(), "%.2f руб.", record.getSum()));
        holder.tvSource.setText(record.getSource());
        holder.ivIcon.setImageDrawable(ContextCompat.getDrawable(context, getDrawable(record.getSource())));
        String dateString = new SimpleDateFormat("dd MMMM yyyy", Locale.getDefault()).format(record.getDate());
        holder.tvDate.setText(dateString);
    }

    private int getDrawable(String source) {
        if (source.toLowerCase().contains("сбербанк")) {
            return R.drawable.ic_sberbank;
        } else if (source.toLowerCase().contains("псб")) {
            return R.drawable.ic_psb;
        } else if (source.toLowerCase().contains("эврика")) {
            return R.drawable.ic_vostok_union;
        } else
            return android.R.color.transparent;
    }


    @Override
    public int getItemCount() {
        return records.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView tvDate;
        private final ImageView ivIcon;
        private final TextView tvSource;
        private final TextView tvSum;

        ViewHolder(View view) {
            super(view);
            tvDate = view.findViewById(R.id.item_payment_history_date);
            ivIcon = view.findViewById(R.id.item_payment_history_icon);
            tvSource = view.findViewById(R.id.item_payment_history_source);
            tvSum = view.findViewById(R.id.item_payment_history_sum);
        }
    }
}
