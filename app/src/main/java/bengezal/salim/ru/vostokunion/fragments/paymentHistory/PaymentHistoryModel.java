package bengezal.salim.ru.vostokunion.fragments.paymentHistory;

import java.util.Date;

import io.reactivex.Observable;
import ru.bengezal.salim.vostok_union_api.dto.PaymentHistoryDTO;

public interface PaymentHistoryModel {

    Observable<PaymentHistoryDTO> getPaymentsHistory(Date from, Date to);

}
