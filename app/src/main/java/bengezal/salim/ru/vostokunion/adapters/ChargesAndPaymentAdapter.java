package bengezal.salim.ru.vostokunion.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.aakira.expandablelayout.ExpandableLayoutListenerAdapter;
import com.github.aakira.expandablelayout.ExpandableRelativeLayout;
import com.mikepenz.materialize.color.Material;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import bengezal.salim.ru.vostokunion.R;
import bengezal.salim.ru.vostokunion.adapters.DAO.chargesAndPayment.ChargesAndPayments;
import bengezal.salim.ru.vostokunion.adapters.DAO.chargesAndPayment.Payment;
import bengezal.salim.ru.vostokunion.components.ClickableRecyclerView;

public class ChargesAndPaymentAdapter extends RecyclerView.Adapter<ChargesAndPaymentAdapter.ViewHolder> {

    private static final String TAG = ChargesAndPaymentAdapter.class.getName();
    private final List<ChargesAndPayments.Record> records;
    private final Context context;
    private final MonthClickListener listener;

    public ChargesAndPaymentAdapter(List<ChargesAndPayments.Record> records, Context context, @NonNull MonthClickListener listener) {
        this.records = new ArrayList<>(records);
        this.context = context;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Log.i(TAG, "oncreateViewHolder");
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_charges_and_payment, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Log.i(TAG, "onBindViewHolder" + position);
        ChargesAndPayments.Record record = records.get(position);
        if (record.getMonthId() != null) {
            holder.block.setOnClickListener(e -> listener.onMonthBlockClick(record.getMonthId()));
        }
        if (record.getMonthName() != null) {
            holder.tvMonthName.setText(record.getMonthName());
        }
        if (record.getIncomingBalance() != null) {
            holder.tvIncomingBalance.setText(String.format(Locale.getDefault(), "%.2f руб.", record.getIncomingBalance()));
        }
        if (record.getCharged() != null) {
            holder.tvCharged.setText(String.format(Locale.getDefault(), "%.2f руб.", record.getCharged()));
        }
        if (record.getOutgoingBalance() != null) {
            holder.tvOutgoingBalance.setText(String.format(Locale.getDefault(), "%.2f руб.", record.getOutgoingBalance()));
        }
        Double sum = 0.0;
        if (!record.getPayments().isEmpty()) {
            for (Payment payment : record.getPayments()) {
                sum += payment.getPaymentAmount();
            }
        }
        holder.tvPaid.setText(String.format(Locale.getDefault(), "%.2f %s.", sum, context.getString(R.string.short_currency_name)));

        holder.blockPaid.setOnClickListener(v -> {

            if (!holder.blockPayments.isExpanded()) {
                holder.blockPayments.setListener(new ExpandableLayoutListenerAdapter() {
                    @Override
                    public void onAnimationStart() {
                        holder.ivExpandPaid.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_expand_less));
                        holder.blockPaid.setBackgroundColor(Material.Grey._50.getAsColor());
                    }
                });
                holder.blockPayments.toggle();
            } else {
                holder.blockPayments.setListener(new ExpandableLayoutListenerAdapter() {
                    @Override
                    public void onAnimationStart() {
                        holder.ivExpandPaid.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_expand_more));
                    }

                    @Override
                    public void onAnimationEnd() {
                        holder.blockPaid.setBackgroundColor(Material.White._1000.getAsColor());
                    }
                });
                holder.blockPayments.toggle();
            }
        });

        RecyclerView.Adapter adapter = new PaymentsAdapter(record.getPayments());
        holder.rvPayments.setAdapter(adapter);
        if (record.getPayments().isEmpty()) {
            holder.tvNoPayments.setVisibility(View.VISIBLE);
        }


        LinearLayoutManager mLayoutManager = new LinearLayoutManager(context);
        holder.rvPayments.setLayoutManager(mLayoutManager);
        holder.rvPayments.setHasFixedSize(true);
        holder.blockPayments.setDuration(500);
        holder.blockPayments.setInterpolator(new FastOutSlowInInterpolator());
        holder.blockPayments.toggle();
        holder.blockPayments.setOnClickListener(v -> holder.block.callOnClick());
        holder.rvPayments.setOnClickListener(v -> holder.block.callOnClick());
        Log.i(TAG, "onBindViewHolder end" + position);
    }

    @Override
    public int getItemCount() {
        return records.size();
    }

    public interface MonthClickListener {
        void onMonthBlockClick(String monthId);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView tvMonthName;
        private final TextView tvIncomingBalance;
        private final TextView tvOutgoingBalance;
        private final TextView tvCharged;
        private final TextView tvNoPayments;
        private final ClickableRecyclerView rvPayments;
        private final LinearLayout block;
        private final TextView tvPaid;
        private final ConstraintLayout blockPaid;
        private final ImageView ivExpandPaid;
        private final ExpandableRelativeLayout blockPayments;

        ViewHolder(View view) {
            super(view);
            tvMonthName = view.findViewById(R.id.item_charges_and_payment_monthName);
            tvIncomingBalance = view.findViewById(R.id.item_charges_and_payment_incomingBalance);
            tvCharged = view.findViewById(R.id.item_charges_and_payment_charged);
            tvOutgoingBalance = view.findViewById(R.id.item_charges_and_payment_outgoingBalance);
            tvNoPayments = view.findViewById(R.id.item_charges_and_payment_noPaymentsInfo);
            rvPayments = view.findViewById(R.id.item_charges_and_payment_paymentsList);
            block = view.findViewById(R.id.item_charges_and_payment_block);
            tvPaid = view.findViewById(R.id.item_charges_and_payment_paid);
            blockPaid = view.findViewById(R.id.item_charges_and_payment_blockPaid);
            ivExpandPaid = view.findViewById(R.id.item_charges_and_payment_ivExpand);
            blockPayments = view.findViewById(R.id.item_charges_and_payment_blockPayments);
        }
    }

}
