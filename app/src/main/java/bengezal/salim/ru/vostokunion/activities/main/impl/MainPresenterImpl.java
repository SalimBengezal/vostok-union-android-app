package bengezal.salim.ru.vostokunion.activities.main.impl;

import android.util.Log;

import javax.inject.Inject;

import bengezal.salim.ru.vostokunion.activities.main.MainModel;
import bengezal.salim.ru.vostokunion.activities.main.MainPresenter;
import bengezal.salim.ru.vostokunion.activities.main.MainView;
import bengezal.salim.ru.vostokunion.injection.App;
import bengezal.salim.ru.vostokunion.managers.CookieManager;
import bengezal.salim.ru.vostokunion.managers.SharedPreferencesManager;
import bengezal.salim.ru.vostokunion.parcels.ApartmentDetailsParcel;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import ru.bengezal.salim.vostok_union_api.dto.MainInfoDTO;

public class MainPresenterImpl implements MainPresenter {

    private static final String TAG = MainPresenterImpl.class.getName();
    private final MainModel model;
    private final MainView view;
    @Inject
    SharedPreferencesManager sharedPreferencesManager;
    @Inject
    CookieManager cookieManager;
    private MainInfoDTO mainInfoDTO;

    private final CompositeDisposable disposables = new CompositeDisposable();

    MainPresenterImpl(MainView view) {
        App.getComponent().inject(this);
        model = new MainModelImpl();
        this.view = view;
    }

    @Override
    public void showAccountNameAndAddressInNavigationDrawer() {
        Disposable disposable = model.getMainInfo()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .share()
                .cache()
                .subscribe(data -> {
                    Log.i(TAG, "showing data");
                    this.mainInfoDTO = data;
                    view.showPersonalInfoInNavigationDrawer(data.address, data.accountName);
                    Log.i(TAG, "Completed showing in nav drawer");
                });
        disposables.add(disposable);
    }

    @Override
    public void startActivityForApartmentDetails() {
        if (mainInfoDTO != null)
            view.startApartmentDetailsActivity(
                    new ApartmentDetailsParcel(
                            mainInfoDTO.accountName,
                            mainInfoDTO.accountNumber,
                            mainInfoDTO.address,
                            mainInfoDTO.phoneNumber,
                            mainInfoDTO.registeredCount,
                            mainInfoDTO.livingArea,
                            mainInfoDTO.totalArea,
                            mainInfoDTO.ownershipDocument
                    )
            );
    }

    @Override
    public void logOut() {
        Log.i(TAG, "Attempt to logOut");
        view.startLoading();
        model.logout()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnComplete(() -> {
                    sharedPreferencesManager.clearAccountNumberAndPassword();
                    cookieManager.clearCookie();
                    view.stopLoading();
                    view.startLoginActivity();
                    Log.i(TAG, "Logged out");
                })
                .subscribe();
    }

    @Override
    public void onDestroy() {
        disposables.dispose();
    }
}
