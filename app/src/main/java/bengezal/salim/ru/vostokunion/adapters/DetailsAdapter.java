package bengezal.salim.ru.vostokunion.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import bengezal.salim.ru.vostokunion.R;
import bengezal.salim.ru.vostokunion.adapters.DAO.DetailsContentValues;

public class DetailsAdapter extends RecyclerView.Adapter<DetailsAdapter.ViewHolder> {

    private final List<DetailsContentValues> contentValues;

    public DetailsAdapter(List<DetailsContentValues> contentValues) {
        this.contentValues = contentValues;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_details, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.header.setText(contentValues.get(position).getHeader());
        holder.value.setText(contentValues.get(position).getValue());
        holder.iconView.setImageResource(contentValues.get(position).getImageRes());
    }

    @Override
    public int getItemCount() {
        return contentValues.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        final TextView header;
        final TextView value;
        final ImageView iconView;

        ViewHolder(View view) {
            super(view);
            header = view.findViewById(R.id.item_details_header);
            value = view.findViewById(R.id.item_details_value);
            iconView = view.findViewById(R.id.item_details_icon);
        }
    }
}
