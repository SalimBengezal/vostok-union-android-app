package bengezal.salim.ru.vostokunion.adapters.DAO.PaymentHistory;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PaymentHistory {

    private final List<PaymentHistoryRecord> records;
    private Date from;
    private Date to;

    public PaymentHistory() {
        records = new ArrayList<>();
    }

    public List<PaymentHistoryRecord> getRecords() {
        return records;
    }

    public void addRecord(Date date, Double sum, String source) {
        PaymentHistoryRecord record = new PaymentHistoryRecord(date, sum, source);
        records.add(record);
    }

    public Date getFrom() {
        return from;
    }

    public void setFrom(Date from) {
        this.from = from;
    }

    public Date getTo() {
        return to;
    }

    public void setTo(Date to) {
        this.to = to;
    }


}
