package ru.bengezal.salim.vostok_union_api;

class Urls {

    static final String HOME_PAGE = "https://lk.vostok-union.ru";
    static final String CHECK_LS = HOME_PAGE + "/ajax.php";
    static final String MAIN_PAGE = HOME_PAGE + "/stack-ls-info.php";
    static final String PAYMENT_HISTORY_PAGE = HOME_PAGE + "/stack-hist-payments.php";
    static final String METER_READINGS_PAGE = HOME_PAGE + "/stack-counter.php";
    static final String CHARGES_AND_PAYMENTS_PAGE = HOME_PAGE + "/stack-period-pay.php";
    static final String EXIT = HOME_PAGE + "/exit.php";
    static final String COUNTER_HISTORY_PAGE = HOME_PAGE + "/stack-hist-counter.php";
    static final String MONTH_DETAILS_PAGE = HOME_PAGE + "/modal.php";
    static final String RECEIPT_PAGE = HOME_PAGE + "/lk-print-kvt.php";
    static final String GENERATE_DOCUMENT = HOME_PAGE + "/generate.php";
    static final String DOWNLOAD_DOCUMENT = HOME_PAGE + "/download.php";
}
