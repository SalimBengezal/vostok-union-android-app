package ru.bengezal.salim.vostok_union_api.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ChargesAndPaymentsDTO {

    private final List<Record> records = new ArrayList<>();
    private Date from;
    private Date to;

    public void addRecord(String recordId, String monthName, Double incomingBalance, Double charged, Date paymentDate, Double paymentAmount, Double outgoingBalance, String paymentSource) {
        records.add(new Record(recordId, monthName, incomingBalance, charged, paymentDate, paymentAmount, outgoingBalance, paymentSource));
    }

    public List<Record> getRecords() {
        return records;
    }

    public Date getFrom() {
        return from;
    }

    public void setFrom(Date from) {
        this.from = from;
    }

    public Date getTo() {
        return to;
    }

    public void setTo(Date to) {
        this.to = to;
    }

    public class Record {
        private final String recordId;
        private final String monthName;
        private final Double incomingBalance;
        private final Double charged;
        private final Date paymentDate;
        private final Double paymentAmount;
        private final Double outgoingBalance;
        private final String paymentSource;

        Record(String recordId, String monthName, Double incomingBalance, Double charged, Date paymentDate, Double paymentAmount, Double outgoingBalance, String paymentSource) {
            this.recordId = recordId;
            this.monthName = monthName;
            this.incomingBalance = incomingBalance;
            this.charged = charged;
            this.paymentDate = paymentDate;
            this.paymentAmount = paymentAmount;
            this.outgoingBalance = outgoingBalance;
            this.paymentSource = paymentSource;
        }

        public String getRecordId() {
            return recordId;
        }

        public String getMonthName() {
            return monthName;
        }

        public Double getIncomingBalance() {
            return incomingBalance;
        }

        public Double getCharged() {
            return charged;
        }

        public Date getPaymentDate() {
            return paymentDate;
        }

        public Double getPaymentAmount() {
            return paymentAmount;
        }

        public Double getOutgoingBalance() {
            return outgoingBalance;
        }

        public String getPaymentSource() {
            return paymentSource;
        }
    }
}
