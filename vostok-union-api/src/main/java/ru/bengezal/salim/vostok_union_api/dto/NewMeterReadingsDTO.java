package ru.bengezal.salim.vostok_union_api.dto;

import java.util.ArrayList;
import java.util.List;

public class NewMeterReadingsDTO {

    public static final String ACTION = "action";
    public static final String POKAZ_LS = "pokaz_LS";

    private final String action;
    private final String pokazLS;
    private final List<Record> records;

    public NewMeterReadingsDTO(String action, String pokazLS) {
        this.action = action;
        this.pokazLS = pokazLS;
        records = new ArrayList<>();
    }

    public String getAction() {
        return action;
    }

    public String getPokazLS() {
        return pokazLS;
    }

    public List<Record> getRecords() {
        return records;
    }

    public void addRecord(String inputName, Double newValue, String hiddenName, String hiddenVal) {
        records.add(new Record(inputName, hiddenName, hiddenVal, newValue));
    }

    public class Record {
        private final String inputName;
        private final String hiddenName;
        private final String hiddenVal;
        private final Double newValue;

        Record(String inputName, String hiddenName, String hiddenVal, Double newValue) {
            this.inputName = inputName;
            this.hiddenName = hiddenName;
            this.hiddenVal = hiddenVal;
            this.newValue = newValue;
        }

        public String getInputName() {
            return inputName;
        }

        public String getHiddenName() {
            return hiddenName;
        }

        public String getHiddenVal() {
            return hiddenVal;
        }

        public Double getNewValue() {
            return newValue;
        }
    }
}
