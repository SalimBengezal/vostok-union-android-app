package ru.bengezal.salim.vostok_union_api.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MeterReadingsDTO {

    private final List<Record> readings;
    private String action;
    private String ls;
    private Integer startPossibleEnteringDate;
    private Integer endPossibleEnteringDate;

    public MeterReadingsDTO() {
        readings = new ArrayList<>();
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getLs() {
        return ls;
    }

    public void setLs(String ls) {
        this.ls = ls;
    }

    public void addReading(String name, String tariff, String counterNumber, Date lastReadingDate, Double lastReading, Double newReading, String newReadingInputName, String newReadingInputHiddenName, String newReadingInputHiddenVal, Boolean enabled, String delRowIdSch, String delTariff, String delDate, String delPokazLS, String delAction) {
        Record rec = new Record(name, tariff, counterNumber, lastReadingDate, lastReading, newReading, newReadingInputName, newReadingInputHiddenName, newReadingInputHiddenVal, enabled);
        if (delRowIdSch != null && delTariff != null && delDate != null && delPokazLS != null && delAction != null) {
            rec.setDeleteLink(delRowIdSch, delTariff, delDate, delPokazLS, delAction);
        }
        readings.add(rec);
    }

    public List<Record> getReadings() {
        return readings;
    }

    public Integer getStartPossibleEnteringDate() {
        return startPossibleEnteringDate;
    }

    public void setStartPossibleEnteringDate(Integer startPossibleEnteringDate) {
        this.startPossibleEnteringDate = startPossibleEnteringDate;
    }

    public Integer getEndPossibleEnteringDate() {
        return endPossibleEnteringDate;
    }

    public void setEndPossibleEnteringDate(Integer endPossibleEnteringDate) {
        this.endPossibleEnteringDate = endPossibleEnteringDate;
    }

    public class Record {

        private final String name;
        private final String tariff;
        private final String counterNumber;
        private final Date lastReadingDate;
        private final Double lastReading;
        private final Double newReading;
        private final String newReadingInputName;
        private final String newReadingInputHiddenName;
        private final String newReadingInputHiddenVal;
        private final Boolean editingStatus;
        private DeleteLink deleteLink;

        Record(String name, String tariff, String counterNumber, Date lastReadingDate, Double lastReading, Double newReading, String newReadingInputName, String newReadingInputHiddenName, String newReadingInputHiddenVal, Boolean editingStatus) {
            this.name = name;
            this.tariff = tariff;
            this.counterNumber = counterNumber;
            this.lastReadingDate = lastReadingDate;
            this.lastReading = lastReading;
            this.newReading = newReading;
            this.newReadingInputName = newReadingInputName;
            this.newReadingInputHiddenName = newReadingInputHiddenName;
            this.newReadingInputHiddenVal = newReadingInputHiddenVal;
            this.editingStatus = editingStatus;
        }

        public String getName() {
            return name;
        }

        public String getTariff() {
            return tariff;
        }

        public String getCounterNumber() {
            return counterNumber;
        }

        public Date getLastReadingDate() {
            return lastReadingDate;
        }

        public Double getLastReading() {
            return lastReading;
        }

        public Double getNewReading() {
            return newReading;
        }

        public String getNewReadingInputName() {
            return newReadingInputName;
        }

        public String getNewReadingInputHiddenName() {
            return newReadingInputHiddenName;
        }

        public String getNewReadingInputHiddenVal() {
            return newReadingInputHiddenVal;
        }

        public Boolean getEditingStatus() {
            return editingStatus;
        }

        void setDeleteLink(String rowIdSch, String tariff, String date, String pokazLs, String action) {
            this.deleteLink = new DeleteLink(rowIdSch, tariff, date, pokazLs, action);
        }

        public DeleteLink getDeleteLink() {
            return deleteLink;
        }

        public class DeleteLink {
            private final String rowIdSch;
            private final String tariff;
            private final String date;
            private final String pokazLs;
            private final String action;


            DeleteLink(String rowIdSch, String tariff, String date, String pokazLs, String action) {
                this.rowIdSch = rowIdSch;
                this.tariff = tariff;
                this.date = date;
                this.pokazLs = pokazLs;
                this.action = action;
            }

            public String getRowIdSch() {
                return rowIdSch;
            }

            public String getTariff() {
                return tariff;
            }

            public String getDate() {
                return date;
            }

            public String getPokazLs() {
                return pokazLs;
            }

            public String getAction() {
                return action;
            }
        }
    }

}
