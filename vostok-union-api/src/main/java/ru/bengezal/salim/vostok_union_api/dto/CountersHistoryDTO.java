package ru.bengezal.salim.vostok_union_api.dto;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class CountersHistoryDTO {

    private final Set<Record> historyList = new HashSet<>();
    private Date fromDate;
    private Date toDate;

    public Set<Record> getHistoryList() {
        return historyList;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public void add(CounterType counterType, String counterNumber, Date date, double meterReading, double consumption) {
        historyList.add(new Record(counterType, counterNumber, date, meterReading, consumption));
    }

    public class Record {
        private final CounterType counterType;
        private final String counterNumber;
        private final Date date;
        private final double meterReading;
        private final double consumption;

        Record(CounterType counterType, String counterNumber, Date date, double meterReading, double consumption) {
            this.counterType = counterType;
            this.counterNumber = counterNumber;
            this.date = date;
            this.meterReading = meterReading;
            this.consumption = consumption;
        }

        public CounterType getCounterType() {
            return counterType;
        }

        public String getCounterNumber() {
            return counterNumber;
        }

        public Date getDate() {
            return date;
        }

        public double getMeterReading() {
            return meterReading;
        }

        public double getConsumption() {
            return consumption;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Record that = (Record) o;

            return Double.compare(that.meterReading, meterReading) == 0 && Double.compare(that.consumption, consumption) == 0 && counterType == that.counterType && counterNumber.equals(that.counterNumber) && date.equals(that.date);
        }

        @Override
        public int hashCode() {
            int result;
            long temp;
            result = counterType.hashCode();
            result = 31 * result + counterNumber.hashCode();
            result = 31 * result + date.hashCode();
            temp = Double.doubleToLongBits(meterReading);
            result = 31 * result + (int) (temp ^ (temp >>> 32));
            temp = Double.doubleToLongBits(consumption);
            result = 31 * result + (int) (temp ^ (temp >>> 32));
            return result;
        }
    }
}
