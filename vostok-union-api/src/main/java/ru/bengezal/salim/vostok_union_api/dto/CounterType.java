package ru.bengezal.salim.vostok_union_api.dto;

public enum CounterType {
    HOT_WATER, COLD_WATER, ELECTRICITY_T1, ELECTRICITY_T2, ELECTRICITY_T3
}
