package ru.bengezal.salim.vostok_union_api.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PaymentHistoryDTO {

    private final List<Record> records;
    private Date from;
    private Date to;

    public PaymentHistoryDTO() {
        records = new ArrayList<>();
    }

    public List<Record> getRecords() {
        return records;
    }

    public void addRecord(Date date, Double sum, String source) {
        Record record = new Record(date, sum, source);
        records.add(record);
    }

    public Date getFrom() {
        return from;
    }

    public void setFrom(Date from) {
        this.from = from;
    }

    public Date getTo() {
        return to;
    }

    public void setTo(Date to) {
        this.to = to;
    }

    public class Record {

        private final Date date;
        private final Double sum;
        private final String source;

        Record(Date date, Double sum, String source) {
            this.date = date;
            this.sum = sum;
            this.source = source;
        }

        public Date getDate() {
            return date;
        }

        public Double getSum() {
            return sum;
        }

        public String getSource() {
            return source;
        }
    }

}
