package ru.bengezal.salim.vostok_union_api;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import ru.bengezal.salim.vostok_union_api.dto.AccountStatus;
import ru.bengezal.salim.vostok_union_api.dto.ChargesAndPaymentsDTO;
import ru.bengezal.salim.vostok_union_api.dto.CountersHistoryDTO;
import ru.bengezal.salim.vostok_union_api.dto.DeleteMeterReadingDTO;
import ru.bengezal.salim.vostok_union_api.dto.MainInfoDTO;
import ru.bengezal.salim.vostok_union_api.dto.MeterReadingsDTO;
import ru.bengezal.salim.vostok_union_api.dto.MonthDetailsDTO;
import ru.bengezal.salim.vostok_union_api.dto.NewMeterReadingsDTO;
import ru.bengezal.salim.vostok_union_api.dto.PaymentHistoryDTO;

public interface VostokUnionAPI {

    boolean connect();

    void disconnect();

    void setAccountNumber(String accountNumber);

    void setPassword(String password);

    Map<String, String> getCookies();

    void setCookies(Map<String, String> cookies);

    AccountStatus existAccountNumber() throws IOException;

    MainInfoDTO getMainInfo() throws IOException;

    CountersHistoryDTO getCountersHistoryInfo(Date from, Date to) throws IOException;

    MeterReadingsDTO getMeterReadingsInfo() throws IOException;

    MonthDetailsDTO getMonthDetailsInfo(String monthId) throws IOException;

    PaymentHistoryDTO getPaymentHistoryInfo(Date from, Date to) throws IOException;

    ChargesAndPaymentsDTO getChargesAndPaymentsInfo(Date from, Date to) throws IOException;

    String saveNewReadings(NewMeterReadingsDTO meterReadings) throws IOException;

    String deleteMeterReading(DeleteMeterReadingDTO deleteMeterReading) throws IOException;

    List<Date> getReceiptList() throws IOException;

    byte[] generateAndDownloadReceiptFile(Date period) throws IOException;

    byte[] generateAndDownloadExtractionFile(Date startPeriod, Date endPeriod) throws IOException;

}
