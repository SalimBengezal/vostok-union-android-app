package ru.bengezal.salim.vostok_union_api.dto;


import java.util.Date;

public class MainInfoDTO {

    public String address;
    public String accountName;
    public String phoneNumber;
    public String accountNumber;
    public double totalArea;
    public double livingArea;
    public int registeredCount;
    public String ownershipDocument;
    public Date receiptMonth;
    public double receiptCurrentBalance;
    public double receiptDebt;
    public double receiptAccrued;
    public double receiptPaid;
    public double receiptPenalties;
    public double receiptTotalPayment;
    public double receiptTotalPaymentWithPenalties;
    public double receiptPaidCurrentMonth;

}
