package ru.bengezal.salim.vostok_union_api.dto;

import java.util.ArrayList;
import java.util.List;

public class MonthDetailsDTO {

    private final List<Record> records;

    private Record summary;

    public MonthDetailsDTO() {
        records = new ArrayList<>();
    }

    public void addRecord(String name, Double tariff, Double incomingBalance, Double charged, Double recalculation, Double takenForQuality, Double payments, Double outgoingBalance, Double volume) {
        records.add(new Record(name, tariff, incomingBalance, charged, recalculation, takenForQuality, payments, outgoingBalance, volume));
    }

    public Record getSummary() {
        return summary;
    }

    public void setSummary(String name, Double tariff, Double incomingBalance, Double charged, Double recalculation, Double takenForQuality, Double payments, Double outgoingBalance, Double volume) {
        this.summary = new Record(name, tariff, incomingBalance, charged, recalculation, takenForQuality, payments, outgoingBalance, volume);
    }

    public List<Record> getRecords() {
        return records;
    }

    public class Record {

        private final String name;
        private final Double tariff;
        private final Double incomingBalance;
        private final Double charged;
        private final Double recalculation;
        private final Double takenForQuality;
        private final Double payments;
        private final Double outgoingBalance;
        private final Double volume;

        Record(String name, Double tariff, Double incomingBalance, Double charged, Double recalculation, Double takenForQuality, Double payments, Double outgoingBalance, Double volume) {
            this.name = name;
            this.tariff = tariff;
            this.incomingBalance = incomingBalance;
            this.charged = charged;
            this.recalculation = recalculation;
            this.takenForQuality = takenForQuality;
            this.payments = payments;
            this.outgoingBalance = outgoingBalance;
            this.volume = volume;
        }

        public String getName() {
            return name;
        }

        public Double getTariff() {
            return tariff;
        }

        public Double getIncomingBalance() {
            return incomingBalance;
        }

        public Double getCharged() {
            return charged;
        }

        public Double getRecalculation() {
            return recalculation;
        }

        public Double getTakenForQuality() {
            return takenForQuality;
        }

        public Double getPayments() {
            return payments;
        }

        public Double getOutgoingBalance() {
            return outgoingBalance;
        }

        public Double getVolume() {
            return volume;
        }
    }

}
