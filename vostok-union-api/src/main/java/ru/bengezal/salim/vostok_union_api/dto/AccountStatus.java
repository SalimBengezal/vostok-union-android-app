package ru.bengezal.salim.vostok_union_api.dto;

public enum AccountStatus {

    NOT_EXIST, EXIST, SERVICE_UNAVAILABLE

}
