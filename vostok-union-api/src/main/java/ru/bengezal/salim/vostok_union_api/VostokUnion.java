package ru.bengezal.salim.vostok_union_api;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Logger;

import ru.bengezal.salim.vostok_union_api.dto.AccountStatus;
import ru.bengezal.salim.vostok_union_api.dto.ChargesAndPaymentsDTO;
import ru.bengezal.salim.vostok_union_api.dto.CounterType;
import ru.bengezal.salim.vostok_union_api.dto.CountersHistoryDTO;
import ru.bengezal.salim.vostok_union_api.dto.DeleteMeterReadingDTO;
import ru.bengezal.salim.vostok_union_api.dto.MainInfoDTO;
import ru.bengezal.salim.vostok_union_api.dto.MeterReadingsDTO;
import ru.bengezal.salim.vostok_union_api.dto.MonthDetailsDTO;
import ru.bengezal.salim.vostok_union_api.dto.NewMeterReadingsDTO;
import ru.bengezal.salim.vostok_union_api.dto.PaymentHistoryDTO;


public class VostokUnion implements VostokUnionAPI {

    private static final Logger LOG = Logger.getLogger(VostokUnion.class.getName());
    private static final String USER_AGENT = "Mozilla/5.0";
    private static final int TIMEOUT = 60000;

    private String accountNumber;
    private String password;
    private Map<String, String> cookies;

    @Override
    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    @Override
    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public Map<String, String> getCookies() {
        return cookies;
    }

    @Override
    public void setCookies(Map<String, String> cookies) {
        this.cookies = cookies;
    }

    @Override
    public AccountStatus existAccountNumber() throws IOException {
        LOG.info("Checking existing account number");
        Map<String, String> keyValues = new HashMap<>();
        keyValues.put("ls", accountNumber);
        keyValues.put("action", "checkLS");
        Connection.Response response = Jsoup.connect(Urls.CHECK_LS)
                .userAgent(USER_AGENT)
                .method(Connection.Method.POST)
                .data(keyValues)
                .execute();
        try {
            String rowIdString = response.body();
            int rowId = Integer.parseInt(rowIdString);
            if (rowId >= 0)
                return AccountStatus.EXIST;
            else
                return AccountStatus.NOT_EXIST;
        } catch (NumberFormatException e) {
            return AccountStatus.SERVICE_UNAVAILABLE;
        }
    }


    private boolean connectWithCredentials() {
        LOG.info("Connecting with credentials");
        try {
            Connection.Response loginForm = Jsoup.connect(Urls.HOME_PAGE)
                    .method(Connection.Method.GET)
                    .userAgent(USER_AGENT)
                    .execute();
            this.cookies = loginForm.cookies();

            LOG.info("Got cookies: " + cookies.toString());

            Map<String, String> keyValues = new HashMap<>();
            keyValues.put("ls", accountNumber);
            keyValues.put("pwd", password);
            keyValues.put("vpb_captcha_code", "");
            keyValues.put("saction", "lspwd");

            Document doc = Jsoup.connect(Urls.MAIN_PAGE)
                    .cookies(cookies)
                    .data(keyValues)
                    .userAgent(USER_AGENT)
                    .timeout(TIMEOUT)
                    .post();
            return isAuthorized(doc);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    private boolean connectWithCookies() {
        LOG.info("Connecting with cookies");
        try {
            Document doc = Jsoup.connect(Urls.MAIN_PAGE)
                    .cookies(cookies)
                    .userAgent(USER_AGENT)
                    .timeout(TIMEOUT)
                    .get();
            return isAuthorized(doc);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    private boolean isAuthorized(Document doc) {
        return doc.select("a.menu-right").attr("href").equals("exit.php");
    }

    @Override
    public boolean connect() {
        LOG.info("Attempt to connect");
        boolean isConnected = false;
        if (!cookies.isEmpty()) {
            isConnected = connectWithCookies();
        }
        if (!isConnected) {
            cookies.clear();
            isConnected = connectWithCredentials();
        }
        return isConnected;
    }

    @Override
    public void disconnect() {
        LOG.info("Attempt to disconnect");
        try {
            Jsoup.connect(Urls.EXIT)
                    .cookies(cookies)
                    .userAgent(USER_AGENT)
                    .get();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public MainInfoDTO getMainInfo() throws IOException {
        LOG.info("getting main info");
        Document doc = Jsoup.connect(Urls.MAIN_PAGE)
                .cookies(cookies)
                .userAgent(USER_AGENT)
                .timeout(TIMEOUT)
                .method(Connection.Method.GET)
                .get();
        return parseMainPage(doc);
    }

    private MainInfoDTO parseMainPage(Document document) {
        LOG.info("Extracting information from mainPage");
        MainInfoDTO mainInfoDTO = new MainInfoDTO();

        Elements elWidgets = document.select("div.vidjet");
        if (elWidgets.size() > 1) {
            //region ������: ����������
            Element elWidgetInformation = elWidgets.get(0);
            Elements elLeftBlockInformation = elWidgetInformation.select("div.info-text");
            // ���������� - ����� ����
            if (elLeftBlockInformation.size() == 4) {
                mainInfoDTO.address = elLeftBlockInformation.get(0).text();
                String owner = elLeftBlockInformation.get(1).text();
                String accountNumber = elLeftBlockInformation.get(2).text();
                String phone = elLeftBlockInformation.get(3).text();
                if (owner.contains("�������:")) {
                    owner = owner.replace("�������:", "").trim();
                    mainInfoDTO.accountName = owner;
                }
                if (accountNumber.contains("����� �����:")) {
                    accountNumber = accountNumber.replace("����� �����:", "").trim();
                    mainInfoDTO.accountNumber = accountNumber;
                }
                if (phone.contains("�������:")) {
                    phone = phone.replace("�������:", "").trim();
                    mainInfoDTO.phoneNumber = phone;
                }
            }

            //���������� - ������ ����
            Elements elRightInfoLabels = elWidgetInformation.select("div.info-right-text");
            Elements elRightInfoValues = elRightInfoLabels.next();
            if (elRightInfoLabels.size() == elRightInfoValues.size() && elRightInfoLabels.size() == 4) {
                if (elRightInfoLabels.get(0).text().contains("����� �����������:")) {
                    String registeredCountString = elRightInfoValues.get(0).text();
                    mainInfoDTO.registeredCount = Integer.parseInt(registeredCountString.replace("���.", "").trim());
                }
                if (elRightInfoLabels.get(1).text().contains("����� �������:")) {
                    String totalAreaString = elRightInfoValues.get(1).text();
                    mainInfoDTO.totalArea = Double.parseDouble(totalAreaString.replace("�2", "").trim());
                }
                if (elRightInfoLabels.get(2).text().contains("����� �������:")) {
                    String livingAreaString = elRightInfoValues.get(2).text();
                    mainInfoDTO.livingArea = Double.parseDouble(livingAreaString.replace("�2", "").trim());
                }
                if (elRightInfoLabels.get(3).text().contains("�������� �� �������������:")) {
                    mainInfoDTO.ownershipDocument = elRightInfoValues.get(3).text();
                }
            }
            //endregion

            //region ������: ������ ����� �� ������ ���������
            Elements elBlockBalance = elWidgets.get(1).select("div.info");
            Element elCurrentBalance = elBlockBalance.get(0).select("div[style*=font-size: 18px]").first();
            if (elCurrentBalance != null) {
                double balance = parseMoneyString(elCurrentBalance.text());
                balance = -balance;
                mainInfoDTO.receiptCurrentBalance = balance;
            }
            Element elDebtLabel = elBlockBalance.select("div:containsOwn(����)").first();
            if (elDebtLabel != null && elDebtLabel.nextElementSibling() != null) {
                String debtDateString = elDebtLabel.text().replace("���� ��", "").replace(":", "").trim();
                DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT, new Locale("ru"));
                try {
                    mainInfoDTO.receiptMonth = df.parse(debtDateString);
                } catch (ParseException e) {
                    LOG.info(String.format("Can't parse date: %s from %s", debtDateString, elDebtLabel));
                }
                mainInfoDTO.receiptDebt = parseMoneyString(elDebtLabel.nextElementSibling().text());
            }
            Element elAccruedLabel = elBlockBalance.select("div:containsOwn(���������)").first();
            if (elAccruedLabel != null && elAccruedLabel.nextElementSibling() != null) {
                mainInfoDTO.receiptAccrued = parseMoneyString(elAccruedLabel.nextElementSibling().text());
            }
            Element elPaidLabel = elBlockBalance.select("div:containsOwn(��������)").first();
            if (elPaidLabel != null && elPaidLabel.nextElementSibling() != null) {
                mainInfoDTO.receiptPaid = parseMoneyString(elPaidLabel.nextElementSibling().text());
            }
            Element elPenaltiesLabel = elBlockBalance.select("div:containsOwn(����:)").first();
            if (elPenaltiesLabel != null && elPenaltiesLabel.nextElementSibling() != null) {
                mainInfoDTO.receiptPenalties = parseMoneyString(elPenaltiesLabel.nextElementSibling().text());
            }
            Element elTotalPaymentLabel = elBlockBalance.select("div:containsOwn(����� � ������:)").first();
            if (elTotalPaymentLabel != null && elTotalPaymentLabel.nextElementSibling() != null) {
                mainInfoDTO.receiptTotalPayment = parseMoneyString(elTotalPaymentLabel.nextElementSibling().text());
            }
            Element elTotalPaymentWithPenaltiesLabel = elBlockBalance.select("div:containsOwn(����� � ������ � ������ ����:)").first();
            if (elTotalPaymentWithPenaltiesLabel != null && elTotalPaymentWithPenaltiesLabel.nextElementSibling() != null) {
                mainInfoDTO.receiptTotalPaymentWithPenalties = parseMoneyString(elTotalPaymentWithPenaltiesLabel.nextElementSibling().text());
            }
            Element elPaidCurMonthValue = elBlockBalance.select("div.balans-money > i").first();
            if (elPaidCurMonthValue != null) {
                mainInfoDTO.receiptPaidCurrentMonth = parseMoneyString(elPaidCurMonthValue.text());
            }
            //endregion
        }
        return mainInfoDTO;

    }

    private double parseMoneyString(String money) {
        String moneyString = money.replace("���.", "").replace(" ", "").trim();
        return Double.parseDouble(moneyString);
    }

    @Override
    public CountersHistoryDTO getCountersHistoryInfo(Date from, Date to) throws IOException {
        LOG.info("getting counters history info");
        DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT, new Locale("ru"));
        Map<String, String> keyValues = new HashMap<>();
        keyValues.put("appendedInput", df.format(from));
        keyValues.put("appendedInput2", df.format(to));
        keyValues.put("show", "go");
        Document document = Jsoup.connect(Urls.COUNTER_HISTORY_PAGE)
                .cookies(cookies)
                .userAgent(USER_AGENT)
                .timeout(TIMEOUT)
                .data(keyValues)
                .post();
        return parseCountersHistoryInfo(document);
    }

    private CountersHistoryDTO parseCountersHistoryInfo(Document document) {
        CountersHistoryDTO countersHistoryDTO = new CountersHistoryDTO();
        DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT, new Locale("ru"));
        Element elDatesRange = document.select("table#counters > caption").first();
        if (elDatesRange != null) {
            String datesString = elDatesRange.text();
            String from = datesString.substring(datesString.indexOf("����� �") + 7, datesString.indexOf(" �� ")).trim();
            String to = datesString.substring(datesString.indexOf(" �� ") + 4).trim();
            try {
                countersHistoryDTO.setFromDate(df.parse(from));
                countersHistoryDTO.setToDate(df.parse(to));
            } catch (ParseException e) {
                LOG.info("Can't parse date:" + e.getMessage());
            }
        }
        Elements rows = document.select("table#counters > tbody > tr");
        for (Element row : rows) {
            CounterType counterType = null;
            String counterNumber = null;
            Date date = null;
            Double meterReading;
            double consumption;
            Elements record = row.children();
            if (record.size() == 5) {
                Element elTypeWithNumber = record.get(1);
                if (elTypeWithNumber == null) {
                    continue;
                } else {
                    String typeString = elTypeWithNumber.text();
                    if (typeString.contains("�1"))
                        counterType = CounterType.ELECTRICITY_T1;
                    else if (typeString.contains("�2"))
                        counterType = CounterType.ELECTRICITY_T2;
                    else if (typeString.contains("�3"))
                        counterType = CounterType.ELECTRICITY_T3;
                    else if (typeString.contains("���"))
                        counterType = CounterType.COLD_WATER;
                    else if (typeString.contains("���"))
                        counterType = CounterType.HOT_WATER;
                    if (typeString.contains("��������� �����"))
                        counterNumber = typeString.substring(typeString.indexOf("��������� �����") + 16).trim();
                }
                Element elDate = elTypeWithNumber.nextElementSibling();
                if (elDate == null) {
                    continue;
                } else {
                    try {
                        date = df.parse(elDate.text());
                    } catch (ParseException e) {
                        LOG.info("Can't parse:" + e.getMessage());
                    }
                }
                Element elMeterReading = elDate.nextElementSibling();
                if (elMeterReading == null) {
                    continue;
                } else {
                    meterReading = Double.parseDouble(elMeterReading.text());
                }
                Element elConsumption = elMeterReading.nextElementSibling();
                if (elConsumption == null) {
                    continue;
                } else {
                    consumption = Double.parseDouble(elConsumption.text());
                }
                countersHistoryDTO.add(counterType, counterNumber, date, meterReading, consumption);
            }
        }
        return countersHistoryDTO;
    }

    @Override
    public PaymentHistoryDTO getPaymentHistoryInfo(Date from, Date to) throws IOException {
        LOG.info("getting payment history info");
        DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT, new Locale("ru"));
        Map<String, String> keyValues = new HashMap<>();
        keyValues.put("appendedInput", df.format(from));
        keyValues.put("appendedInput2", df.format(to));
        keyValues.put("show", "go");
        Document document = Jsoup.connect(Urls.PAYMENT_HISTORY_PAGE)
                .cookies(cookies)
                .userAgent(USER_AGENT)
                .timeout(TIMEOUT)
                .data(keyValues)
                .post();
        return parsePaymentHistoryInfo(document);

    }

    private PaymentHistoryDTO parsePaymentHistoryInfo(Document document) {
        PaymentHistoryDTO paymentHistoryDTO = new PaymentHistoryDTO();
        String datesString = document.select("table > caption").text();
        DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT, new Locale("ru"));
        if (datesString != null) {
            String from = datesString.substring(datesString.indexOf(" � ") + 3, datesString.indexOf(" �� ")).trim();
            String to = datesString.substring(datesString.indexOf(" �� ") + 4).trim();
            try {
                paymentHistoryDTO.setFrom(df.parse(from));
                paymentHistoryDTO.setTo(df.parse(to));
            } catch (ParseException e) {
                LOG.info("Can't parse:" + e.getMessage());
            }
        }

        Elements rows = document.select("table > tbody > tr");
        for (Element row : rows) {
            Date date = null;
            Double sum;
            String source;
            Elements children = row.children();
            if (children.size() == 3) {
                Element elDate = children.get(0);
                if (elDate == null) {
                    continue;
                } else {
                    try {
                        date = df.parse(elDate.text());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
                Element elSum = children.get(1);
                if (elSum == null) {
                    continue;
                } else {
                    sum = parseMoneyString(elSum.text());
                }
                Element elSource = children.get(2);
                if (elSource == null) {
                    continue;
                } else {
                    source = elSource.text();
                }
                paymentHistoryDTO.addRecord(date, sum, source);
            }
        }
        return paymentHistoryDTO;
    }

    @Override
    public MeterReadingsDTO getMeterReadingsInfo() throws IOException {
        LOG.info("getting meter readings");
        Document document = Jsoup.connect(Urls.METER_READINGS_PAGE)
                .cookies(cookies)
                .userAgent(USER_AGENT)
                .timeout(TIMEOUT)
                .get();
        return parseMeterReadingsInfo(document);
    }

    private MeterReadingsDTO parseMeterReadingsInfo(Document document) {
        MeterReadingsDTO meterReadingsDTO = new MeterReadingsDTO();
        String message = document.select("div.alert.alert-error>strong").text();
        message = message.replaceAll("[�-��-�]", "").replaceAll("\\s+", " ").trim();
        String[] dates = message.split(" ");
        Integer startDate = null;
        Integer endDate = null;
        try {
            if (dates.length == 2) {
                startDate = Integer.parseInt(dates[0]);
                endDate = Integer.parseInt(dates[1]);
            }
        } catch (NumberFormatException e) {
            LOG.info("Can't parse:" + e.getMessage());
        }
        meterReadingsDTO.setStartPossibleEnteringDate(startDate);
        meterReadingsDTO.setEndPossibleEnteringDate(endDate);
        Element elPokazLS = document.select("p>input[name='pokaz_LS']").first();
        String pokazLS = null;
        if (elPokazLS != null) {
            pokazLS = elPokazLS.val();
        }
        meterReadingsDTO.setLs(pokazLS);
        Element elAction = document.select("p>input[name='action']").first();
        String action = null;
        if (elAction != null) {
            action = elAction.val();
        }
        meterReadingsDTO.setAction(action);
        Elements elements = document.select("table > thead > tr > td");
        if (elements.size() == 6 && elements.get(0).text().contains("�������� ������")
                && elements.get(1).text().contains("�������� ������")
                && elements.get(2).text().contains("��������� �����")
                && elements.get(3).text().contains("���� ���������")
                && elements.get(4).text().contains("���������� ���������")
                && elements.get(5).text().contains("����� ���������")) {

            Elements trs = document.select("table > tbody > tr");
            for (Element tr : trs) {
                String name = null;
                Element elName = tr.child(0);
                if (elName != null) {
                    name = elName.text();
                }
                String tariff = null;
                Element elTariff = tr.child(1);
                if (elTariff != null && !elTariff.text().trim().equals("")) {
                    tariff = elTariff.text();
                }
                String counterNumber = null;
                Element elCounterNumber = tr.child(2);
                if (elCounterNumber != null) {
                    counterNumber = elCounterNumber.text();
                }
                DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT, new Locale("ru"));
                Date date = null;
                Element elDate = tr.child(3);
                if (elDate != null) {
                    try {
                        date = df.parse(elDate.text());
                    } catch (ParseException e) {
                        LOG.info("Can't parse:" + e.getMessage());
                    }
                }

                String delRowIdSch = null;
                String delTariff = null;
                String delDate = null;
                String delPokazLS = null;
                String delAction = null;

                Double lastReading = null;
                Element elLastReading = tr.child(4);
                if (elLastReading != null) {
                    Elements includedLink = elLastReading.select("a");
                    if (includedLink != null && !includedLink.isEmpty()) {
                        String attrVal = includedLink.attr("onclick");
                        if (!attrVal.isEmpty()) {
                            attrVal = attrVal.substring(attrVal.indexOf("(") + 1, attrVal.indexOf(")")).replace("\"", "");
                            String[] attrVals = attrVal.split(",");
                            if (attrVals.length == 3) {
                                delRowIdSch = attrVals[0].trim();
                                delTariff = attrVals[1].trim();
                                delDate = attrVals[2].trim();

                                Elements formDelete = document.select("form#deleteValue");
                                Elements inputPokazLS = formDelete.select("input[name=pokaz_LS]");
                                Elements inputAction = formDelete.select("input[name=action]");
                                delPokazLS = inputPokazLS.val();
                                delAction = inputAction.val();
                            }
                            includedLink.remove();
                        }
                    }
                    lastReading = Double.parseDouble(elLastReading.text());
                }

                Boolean enabledEditing = null;
                String newReadingInputName = null;
                Double newReading = null;
                String newReadingInputHiddenName = null;
                String newReadingInputHiddenVal = null;

                Element elNewReading = tr.child(5);
                if (elNewReading != null && elNewReading.children().size() == 2) {
                    Element elNewReadingInput = elNewReading.child(0);
                    if (elNewReadingInput != null) {
                        String attrDisabled = elNewReadingInput.attr("disabled");
                        enabledEditing = !attrDisabled.equals("disabled");
                        newReadingInputName = elNewReadingInput.attr("name");
                        try {
                            newReading = Double.parseDouble(elNewReadingInput.val().trim());
                        } catch (NumberFormatException e) {
                            LOG.info("Can't parse:" + e.getMessage());
                        }
                    }
                    Element elNewReadingInputHidden = elNewReading.child(1);
                    if (elNewReadingInputHidden != null) {
                        newReadingInputHiddenName = elNewReadingInputHidden.attr("name");
                        newReadingInputHiddenVal = elNewReadingInputHidden.val();
                    }
                }
                meterReadingsDTO.addReading(
                        name,
                        tariff,
                        counterNumber,
                        date,
                        lastReading,
                        newReading,
                        newReadingInputName,
                        newReadingInputHiddenName,
                        newReadingInputHiddenVal,
                        enabledEditing,
                        delRowIdSch,
                        delTariff,
                        delDate,
                        delPokazLS,
                        delAction
                );
            }
        }
        return meterReadingsDTO;
    }

    @Override
    public String saveNewReadings(NewMeterReadingsDTO meterReadings) throws IOException {
        LOG.info("save new meter readings");
        Map<String, String> keyValues = new HashMap<>();
        keyValues.put(NewMeterReadingsDTO.ACTION, meterReadings.getAction());
        keyValues.put(NewMeterReadingsDTO.POKAZ_LS, meterReadings.getPokazLS());
        for (NewMeterReadingsDTO.Record rec : meterReadings.getRecords()) {
            String newValue = "";
            if (rec.getNewValue() != null) {
                newValue = String.format("%s", rec.getNewValue());
            }
            keyValues.put(rec.getInputName(), newValue);
            keyValues.put(rec.getHiddenName(), rec.getHiddenVal());
        }
        Document document = Jsoup.connect(Urls.METER_READINGS_PAGE)
                .cookies(cookies)
                .userAgent(USER_AGENT)
                .timeout(TIMEOUT)
                .data(keyValues)
                .post();
        return parseAnswerAfterPostInMeterReadings(document);
    }

    private String parseAnswerAfterPostInMeterReadings(Document document) {
        Elements alertDivs = document.select("div.alert");
        Elements errorDivs = alertDivs.select("div.alert-error");
        if (errorDivs != null && errorDivs.size() == 1) {
            Element errorDiv = errorDivs.first();
            errorDiv.select("button").remove();
            errorDiv.select("strong").remove();
            return errorDiv.text();
        }
        Elements successAlertDivs = alertDivs.select("div.alert-success");
        if (successAlertDivs != null && successAlertDivs.size() == 1) {
            Element successAlertDiv = successAlertDivs.first();
            successAlertDiv.select("button").remove();
            successAlertDiv.select("strong").remove();
            return successAlertDiv.text();
        }
        return "";
    }

    public String deleteMeterReading(DeleteMeterReadingDTO meterReadingDTO) throws IOException {
        LOG.info("deleting meter reading");
        Map<String, String> keyValues = new HashMap<>();
        keyValues.put(DeleteMeterReadingDTO.ROW_ID_SCH, meterReadingDTO.getRowIdSch());
        keyValues.put(DeleteMeterReadingDTO.TARIFF, meterReadingDTO.getTariff());
        keyValues.put(DeleteMeterReadingDTO.DATE, meterReadingDTO.getDate());
        keyValues.put(DeleteMeterReadingDTO.ACTION, meterReadingDTO.getAction());
        keyValues.put(DeleteMeterReadingDTO.POKAZ_LS, meterReadingDTO.getPokazLS());
        Document document = Jsoup.connect(Urls.METER_READINGS_PAGE)
                .cookies(cookies)
                .userAgent(USER_AGENT)
                .timeout(TIMEOUT)
                .data(keyValues)
                .post();
        String message = parseAnswerAfterPostInMeterReadings(document);
        if (message.trim().isEmpty()) {
            return "��������� ������� �������";
        }
        return message;
    }

    @Override
    public ChargesAndPaymentsDTO getChargesAndPaymentsInfo(Date from, Date to) throws IOException {
        LOG.info("getting charges and payments info");
        DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT, new Locale("ru"));
        Map<String, String> keyValues = new HashMap<>();
        keyValues.put("appendedInput", df.format(from));
        keyValues.put("appendedInput2", df.format(to));
        keyValues.put("show", "go");
        Document document = Jsoup.connect(Urls.CHARGES_AND_PAYMENTS_PAGE)
                .cookies(cookies)
                .userAgent(USER_AGENT)
                .timeout(TIMEOUT)
                .data(keyValues)
                .post();
        return parseChargesAndPaymentsInfo(document);
    }

    private ChargesAndPaymentsDTO parseChargesAndPaymentsInfo(Document document) {
        ChargesAndPaymentsDTO chargesAndPaymentsDTO = new ChargesAndPaymentsDTO();
        Element table = document.select("table").first();
        String datesString = table.select("caption").text();
        DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT, new Locale("ru"));
        if (datesString != null) {
            String from = datesString.substring(datesString.indexOf(" � ") + 3, datesString.indexOf(" �� ")).trim();
            String to = datesString.substring(datesString.indexOf(" �� ") + 4).trim();
            try {
                chargesAndPaymentsDTO.setFrom(df.parse(from));
                chargesAndPaymentsDTO.setTo(df.parse(to));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        Elements rows = table.select("tbody > tr");
        for (Element row : rows) {
            Elements tds = row.select("td");
            if (tds.size() == 7 && !tds.text().toLowerCase().contains("�����")) {

                String id = row.id();
                String monthNameStr = tds.get(0).text().trim();
                String monthName = null;
                if (!monthNameStr.equals("")) {
                    monthName = monthNameStr;
                }
                String incomingBalanceStr = tds.get(1).text().trim();
                Double incomingBalance = null;
                if (!incomingBalanceStr.equals("")) {
                    incomingBalance = parseMoneyString(incomingBalanceStr);
                }
                String chargedStr = tds.get(2).text().trim();
                Double charged = null;
                if (!chargedStr.equals("")) {
                    charged = parseMoneyString(chargedStr);
                }
                String paymentDateStr = tds.get(3).text().trim();
                Date paymentDate = null;
                if (!paymentDateStr.equals("")) {
                    try {
                        paymentDate = df.parse(paymentDateStr);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
                String paymentAmountStr = tds.get(4).text().trim();
                Double paymentAmount = null;
                if (!paymentAmountStr.equals("")) {
                    paymentAmount = parseMoneyString(paymentAmountStr);
                }
                String outgoingBalanceStr = tds.get(5).text().trim();
                Double outgoingBalance = null;
                if (!outgoingBalanceStr.equals("")) {
                    outgoingBalance = parseMoneyString(outgoingBalanceStr);
                }
                String paymentSourceStr = tds.get(6).text().trim();
                String paymentSource = null;
                if (!paymentSourceStr.equals("")) {
                    paymentSource = paymentSourceStr;
                }
                chargesAndPaymentsDTO.addRecord(id, monthName, incomingBalance, charged, paymentDate, paymentAmount, outgoingBalance, paymentSource);
            }
        }
        return chargesAndPaymentsDTO;
    }

    @Override
    public MonthDetailsDTO getMonthDetailsInfo(String monthId) throws IOException {
        LOG.info("getting month details");
        Document document = Jsoup.connect(Urls.MONTH_DETAILS_PAGE)
                .cookies(cookies)
                .userAgent(USER_AGENT)
                .timeout(TIMEOUT)
                .data("month", monthId)
                .get();
        return parseMonthDetailsInfo(document);
    }

    private MonthDetailsDTO parseMonthDetailsInfo(Document document) {
        MonthDetailsDTO monthDetails = new MonthDetailsDTO();
        Elements rows = document.select("tbody > tr");
        if (!rows.isEmpty()) {
            for (Element row : rows) {
                Elements columns = row.select("td:not(:has(b))");
                Elements summary = row.select("td:has(b)");
                if (!summary.isEmpty()) {
                    columns = row.select("td");
                }
                if (columns.size() == 10) {
                    String name = null;
                    String nameStr = columns.get(1).text();
                    if (nameStr != null && !nameStr.equals("")) {
                        name = nameStr;
                    }
                    Double tariff = null;
                    String tariffStr = columns.get(2).text();
                    if (tariffStr != null && !tariffStr.equals("")) {
                        tariff = parseMoneyString(tariffStr);
                    }
                    Double incomingBalance = null;
                    String incomingBalanceStr = columns.get(3).text();
                    if (incomingBalanceStr != null && !incomingBalanceStr.equals("")) {
                        incomingBalance = parseMoneyString(incomingBalanceStr);
                    }
                    Double charged = null;
                    String chargedStr = columns.get(4).text();
                    if (chargedStr != null && !chargedStr.equals("")) {
                        charged = parseMoneyString(chargedStr);
                    }
                    Double recalculation = null;
                    String recalculationStr = columns.get(5).text();
                    if (recalculationStr != null && !recalculationStr.equals("")) {
                        recalculation = parseMoneyString(recalculationStr);
                    }
                    Double takenForQuality = null;
                    String takenForQualityStr = columns.get(6).text();
                    if (takenForQualityStr != null && !takenForQualityStr.equals("")) {
                        takenForQuality = parseMoneyString(takenForQualityStr);
                    }
                    Double payments = null;
                    String paymentsStr = columns.get(7).text();
                    if (paymentsStr != null && !paymentsStr.equals("")) {
                        payments = parseMoneyString(paymentsStr);
                    }
                    Double outgoingBalance = null;
                    String outgoingBalanceStr = columns.get(8).text();
                    if (outgoingBalanceStr != null && !outgoingBalanceStr.equals("")) {
                        outgoingBalance = parseMoneyString(outgoingBalanceStr);
                    }
                    Double volume = null;
                    String volumeStr = columns.get(9).text();
                    if (volumeStr != null && !volumeStr.equals("")) {
                        volume = parseMoneyString(volumeStr);
                    }
                    if (!summary.isEmpty()) {
                        monthDetails.setSummary(name, tariff, incomingBalance, charged, recalculation, takenForQuality, payments, outgoingBalance, volume);
                    } else {
                        monthDetails.addRecord(name, tariff, incomingBalance, charged, recalculation, takenForQuality, payments, outgoingBalance, volume);
                    }
                }
            }
        }
        return monthDetails;
    }

    public List<Date> getReceiptList() throws IOException {
        LOG.info("getting receipt list");
        Document document = Jsoup.connect(Urls.RECEIPT_PAGE)
                .cookies(cookies)
                .userAgent(USER_AGENT)
                .timeout(TIMEOUT)
                .get();
        return parseReceiptList(document);

    }

    private List<Date> parseReceiptList(Document document) {
        List<Date> dateList = new ArrayList<>();
        Elements elOptions = document.select("select#period>option");
        if (!elOptions.isEmpty()) {
            String dateString;
            DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT, new Locale("ru"));
            for (Element option : elOptions) {
                dateString = option.attr("value");
                if (!dateString.trim().isEmpty()) {
                    try {
                        dateList.add(df.parse(dateString));
                    } catch (ParseException e) {
                        LOG.info("Can't parse date string:" + dateString);
                    }
                }
            }
        }
        return dateList;
    }

    private String generateUrlToReceiptFile(Date period) throws IOException {
        DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT, new Locale("ru"));
        Connection.Response response = Jsoup.connect(Urls.GENERATE_DOCUMENT)
                .cookies(cookies)
                .userAgent(USER_AGENT)
                .timeout(TIMEOUT)
                .data("period", df.format(period))
                .ignoreContentType(true)
                .execute();

        return response.body();
    }

    private byte[] downloadReceiptFile(Date period, String filename) throws IOException {
        DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT, new Locale("ru"));
        Map<String, String> keyValues = new HashMap<>();
        keyValues.put("period", df.format(period));
        keyValues.put("url", filename);
        Connection.Response response = Jsoup.connect(Urls.DOWNLOAD_DOCUMENT)
                .cookies(cookies)
                .userAgent(USER_AGENT)
                .timeout(TIMEOUT)
                .data(keyValues)
                .ignoreContentType(true)
                .execute();
        return response.bodyAsBytes();
    }

    private String generateUrlToExtractionFile(Date startPeriod, Date endPeriod) throws IOException {
        DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT, new Locale("ru"));
        Map<String, String> keyValues = new HashMap<>();
        keyValues.put("from", df.format(startPeriod));
        keyValues.put("to", df.format(endPeriod));
        Connection.Response response = Jsoup.connect(Urls.GENERATE_DOCUMENT)
                .cookies(cookies)
                .userAgent(USER_AGENT)
                .timeout(TIMEOUT)
                .data(keyValues)
                .ignoreContentType(true)
                .execute();

        return response.body();
    }

    private byte[] downloadExtractionFile(Date startPeriod, Date endPeriod, String filename) throws IOException {
        DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT, new Locale("ru"));
        Map<String, String> keyValues = new HashMap<>();
        keyValues.put("from", df.format(startPeriod));
        keyValues.put("to", df.format(endPeriod));
        keyValues.put("show", "go");
        keyValues.put("url", filename);
        Connection.Response response = Jsoup.connect(Urls.DOWNLOAD_DOCUMENT)
                .cookies(cookies)
                .userAgent(USER_AGENT)
                .timeout(TIMEOUT)
                .data(keyValues)
                .ignoreContentType(true)
                .execute();
        return response.bodyAsBytes();
    }

    public byte[] generateAndDownloadReceiptFile(Date period) throws IOException {
        String url = generateUrlToReceiptFile(period);
        return downloadReceiptFile(period, url);
    }

    public byte[] generateAndDownloadExtractionFile(Date startPeriod, Date endPeriod) throws IOException {
        String url = generateUrlToExtractionFile(startPeriod, endPeriod);
        return downloadExtractionFile(startPeriod, endPeriod, url);
    }

}
