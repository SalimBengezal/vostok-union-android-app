package ru.bengezal.salim.vostok_union_api.dto;

public class DeleteMeterReadingDTO {

    public static final String ROW_ID_SCH = "row_id_sch";
    public static final String DATE = "date";
    public static final String TARIFF = "tarif";
    public static final String POKAZ_LS = "pokaz_LS";
    public static final String ACTION = "action";

    private final String rowIdSch;
    private final String tariff;
    private final String date;
    private final String pokazLS;
    private final String action;

    public DeleteMeterReadingDTO(String rowIdSch, String tariff, String date, String pokazLS, String action) {
        this.rowIdSch = rowIdSch;
        this.tariff = tariff;
        this.date = date;
        this.action = action;
        this.pokazLS = pokazLS;
    }

    public String getRowIdSch() {
        return rowIdSch;
    }

    public String getTariff() {
        return tariff;
    }

    public String getDate() {
        return date;
    }

    public String getPokazLS() {
        return pokazLS;
    }

    public String getAction() {
        return action;
    }
}
